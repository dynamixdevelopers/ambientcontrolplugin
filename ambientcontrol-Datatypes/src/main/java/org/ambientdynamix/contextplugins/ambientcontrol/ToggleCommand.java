package org.ambientdynamix.contextplugins.ambientcontrol;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by workshop on 3/12/14.
 */
public class ToggleCommand extends IControlMessage {

    /**
     * Required CREATOR field that generates instances of this Parcelable class from a Parcel.
     *
     * @see http://developer.android.com/reference/android/os/Parcelable.Creator.html
     */
    public static Parcelable.Creator<ToggleCommand> CREATOR = new Parcelable.Creator<ToggleCommand>() {
        /**
         * Create a new instance of the Parcelable class, instantiating it from the given Parcel whose data had
         * previously been written by Parcelable.writeToParcel().
         */
        public ToggleCommand createFromParcel(Parcel in) {
            return new ToggleCommand(in);
        }

        /**
         * Create a new array of the Parcelable class.
         */
        public ToggleCommand[] newArray(int size) {
            return new ToggleCommand[size];
        }
    };


    private double velocity = 0;

    private boolean down = true;

    public double getVelocity() {
        return velocity;
    }

    public void setVelocity(int velocity) {
        this.velocity = velocity;
    }

    public boolean isDown() {
        return down;
    }

    public void setDown(boolean down) {
        this.down = down;
    }

    /**
     * Returns the type of the context information represented by the IContextInfo. This string must match one of the
     * supported context information type strings described by the source ContextPlugin.
     */
    @Override
    public String getContextType() {
        return CONTEXT_TYPE;
    }

    /**
     * Returns the fully qualified class-name of the class implementing the IContextInfo interface. This allows Dynamix
     * applications to dynamically cast IContextInfo objects to their original type using reflection. A Java
     * "instanceof" compare can also be used for this purpose.
     */
    @Override
    public String getImplementingClassname() {
        return this.getClass().getName();
    }

    /**
     * Returns a Set of supported string-based context representation format types or null if no representation formats
     * are supported. Examples formats could include MIME, Dublin Core, RDF, etc. See the plug-in documentation for
     * supported representation types.
     */
    @Override
    public Set<String> getStringRepresentationFormats() {
        Set<String> formats = new HashSet<String>();
        formats.add("text/plain");
        return formats;
    }

    /**
     * Returns a string-based representation of the IContextInfo for the specified format string (e.g.
     * "application/json") or null if the requested format is not supported.
     */
    @Override
    public String getStringRepresentation(String format) {
        if (format.equalsIgnoreCase("text/plain"))
            return "command: " + command + " velocity: " + velocity;
        else
            // Format not supported, so return an empty string
            return "";
    }


    /**
     * Create a ToggleCommand
     */
    public ToggleCommand(String command, String name) {
        super(command,name);
    }

    public ToggleCommand(String command, double velocity, String name) {
        super(command,name);
        this.velocity = velocity;
    }

    public ToggleCommand(String command, double velocity, boolean down, String name) {
        super(command,name);
        this.velocity = velocity;
        this.down = down;
    }

    public ToggleCommand(String command, boolean down, String name) {
        super(command,name);
        this.down = down;
    }

    /**
     * {@inheritDoc}
     */


    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }

    /**
     * Used by Parcelable when sending (serializing) data over IPC.
     */
    public void writeParcel(Parcel out, int flags) {
        out.writeDouble(velocity);

    }

    /**
     * Used by the Parcelable.Creator when reconstructing (deserializing) data sent over IPC.
     */
    private ToggleCommand(final Parcel in) {
        super(in);
        velocity = in.readDouble();
    }

    /**
     * Default implementation that returns 0.
     *
     * @return 0
     */
    @Override
    public int describeContents() {
        return 0;
    }
}
