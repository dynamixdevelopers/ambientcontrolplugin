package org.ambientdynamix.contextplugins.ambientcontrol;

import android.util.Log;
import org.jibx.runtime.*;

import java.io.*;
import java.util.*;

/**
 * Created by workshop on 07-Jul-14.
 */
public class ControlGraphConfig {

        private final String TAG = this.getClass().getSimpleName();
    private String clientPlugin; // The plugin receiving contextEvents
    private Set<String> serverPlugins; // The plugin serving contextEvents

    private Map<String, List<Edge>> controlEdges; //Command -> edge list
    private int hashID;

    public int getHashID() {
        return hashID;
    }

    public ControlGraphConfig(String clientPlugin, Set<String> serverPlugins, Collection<Edge> controlEdges) {
        this.clientPlugin = clientPlugin;
        this.serverPlugins = serverPlugins;
        this.controlEdges = new HashMap<String, List<Edge>>();
        for (Edge controlEdge : controlEdges) {
            if(!this.controlEdges.containsKey(controlEdge.getCommandType()))
                this.controlEdges.put(controlEdge.getCommandType(),new ArrayList<Edge>());
            this.controlEdges.get(controlEdge.getCommandType()).add(controlEdge);
        }
        try {
            hashID = this.marshal().hashCode();
        } catch (JiBXException e) {
            e.printStackTrace();
        }
    }

    private Set<String> commandsFromStrings(List<String> strings) {
        Set<String> commands = new HashSet<String>();
        for (String string : strings) {
            commands.add(string);
        }
        return commands;
    }

    public ControlGraphConfig(String xml) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        try {
            Log.i(TAG,"initializing control graph: " + xml);
            IBindingFactory bfact = BindingDirectory.getFactory(ControlConfig.class);
            IUnmarshallingContext uctx = bfact.createUnmarshallingContext();
            StringReader stringReader = new StringReader(xml);
//            Object obj = uctx.unmarshalDocument(new FileInputStream("SpheroDrone.xml"), null);
            Object obj = uctx.unmarshalDocument(stringReader, null);
            ControlConfig config = (ControlConfig) obj;
//            Log.i(TAG,config.getController() + ", " + config.getReceiverList());
            IMarshallingContext mctx = bfact.createMarshallingContext();
            mctx.setIndent(4);
            StringWriter stringWriter = new StringWriter();
            mctx.marshalDocument(config, "UTF-8", null, stringWriter);
            Log.i(TAG, "config read: " + stringWriter.toString());
            clientPlugin = config.getReceiver();
            serverPlugins = new HashSet<String>();
            serverPlugins.addAll(config.getControllerList());

            controlEdges = new HashMap<String, List<Edge>>();
            readEdges(controlEdges, config.getControlEdgeList());

            hashID = xml.hashCode();
        } catch (JiBXException e) {
            e.printStackTrace();
        }
    }

    private Translator instantiateTranslator(String className) throws IllegalAccessException, InstantiationException, ClassNotFoundException {
        try{
            return (Translator) Class.forName(className).newInstance();
        }catch(ClassNotFoundException e){
            for (List<Class> classes : TranslatingProfileMatcher.getTranslators().values()) {
                for (Class translatorClass : classes) {
                    if(translatorClass.getCanonicalName().equals(className))
                        return (Translator) translatorClass.newInstance();
                }
            }
            throw new ClassNotFoundException("Translator: " + className + " not registered");
        }
    }

    /**
     *
     * @param target A map, that maps command types to a list of edges over which these commands are exchanged
     * @param configs edge configs to read from the xml objects
     * @throws ClassNotFoundException
     * @throws IllegalAccessException
     * @throws InstantiationException
     */
    private void readEdges(Map<String, List<Edge>> target, List<EdgeConfig> configs) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        for (EdgeConfig edgeConfig : configs) {
            TranslatorConfig translatorConfig = edgeConfig.getTranslator();
            Edge edge;
            if(!target.containsKey(edgeConfig.getCommandType()))
                target.put(edgeConfig.getCommandType(),new ArrayList<Edge>());
            if (translatorConfig == null)
                edge = new Edge(edgeConfig.getCommandType(), edgeConfig.getName(), edgeConfig.getSourcePlugin(), (ArrayList<String>)edgeConfig.getSourceDeviceIdList(), edgeConfig.getTargetPlugin(), (ArrayList<String>)edgeConfig.getTargetDeviceIdList());
            else {
                Map<String, String> parameters = new HashMap<String, String>();
                if (translatorConfig.getParameterList() != null) {
                    for (Parameter parameter : translatorConfig.getParameterList()) {
                        parameters.put(parameter.getName(), parameter.getValue());
                    }
                }
                for (Edge existingEdge : target.get(edgeConfig.getCommandType())) {
                    Log.i(TAG,"targetDevices: " + edgeConfig.getTargetDeviceIdList() + "  sourceDevices: " + edgeConfig.getSourceDeviceIdList());
                    if(existingEdge.getCommandType().equals(edgeConfig.getCommandType()) &&
                            edgeConfig.getSourceDeviceIdList() != null && existingEdge.getSourceDeviceIds().containsAll(edgeConfig.getSourceDeviceIdList()) &&
                            edgeConfig.getTargetDeviceIdList() != null && existingEdge.getTargetDeviceIds().containsAll(edgeConfig.getTargetDeviceIdList()) &&
                            existingEdge.getTranslator() != null &&
                            existingEdge.getTranslator().getClass().equals(translatorConfig.get_Class())) {
                        existingEdge.getTranslator().getTranslateTo().addAll(translatorConfig.getTranslateToList());
                        existingEdge.getTranslator().getConfig().putAll(parameters);
                        Log.i(TAG, "adding translation to existing translator: " + existingEdge.getTranslator().getTranslateFrom() + " to " + translatorConfig.getTranslateToList() + " from " + edgeConfig.getSourcePlugin());
                        return;
                    }
                }
                Translator translator = instantiateTranslator(translatorConfig.get_Class());
                translator.setTranslateTo(commandsFromStrings(translatorConfig.getTranslateToList()));
                translator.setConfig(parameters);
                edge = new Edge(edgeConfig.getCommandType(), edgeConfig.getName(), edgeConfig.getSourcePlugin(), (ArrayList<String>)edgeConfig.getSourceDeviceIdList(), edgeConfig.getTargetPlugin(), (ArrayList<String>)edgeConfig.getTargetDeviceIdList(), translator);
            }
            target.get(edgeConfig.getCommandType()).add(edge);
        }
    }

    public static void main(String[] args) {
        IBindingFactory bfact = null;
        try {
            ControlConfig config = new ControlConfig();

            config.setReceiver("org.ambientdynamix.contextplugins.ardrone");
            ArrayList<String> controlers = new ArrayList<String>();
            controlers.add("org.ambientdynamix.contextplugins.spheronative");
            controlers.add("org.ambientdynamix.contextplugins.pitchtracker");
            config.setControllerList(controlers);
            ArrayList<EdgeConfig> controlEdges = new ArrayList<EdgeConfig>();
            ArrayList<String> targetDeviceIds = new ArrayList<String>();
            targetDeviceIds.add("max bulb");
            targetDeviceIds.add("Hue Lamp 2");
            EdgeConfig edgeConfig1 = new EdgeConfig();
            edgeConfig1.setName("Pitch Yaw Roll");
            edgeConfig1.setCommandType(Commands.SENSOR_PYR.toString());
            edgeConfig1.setSourcePlugin("org.ambientdynamix.contextplugins.spheronative");
            edgeConfig1.setTargetPlugin("org.ambientdynamix.contextplugins.ardrone");
            edgeConfig1.setTargetDeviceIdList(targetDeviceIds);
            controlEdges.add(edgeConfig1);

            EdgeConfig edgeConfig2 = new EdgeConfig();
            edgeConfig2.setName("Collision");
            edgeConfig2.setCommandType(Commands.SENSOR_TOGGLE.toString());
            edgeConfig2.setSourcePlugin("org.ambientdynamix.contextplugins.spheronative");
            edgeConfig2.setTargetPlugin("org.ambientdynamix.contextplugins.ardrone");
            TranslatorConfig translatorConfig = new TranslatorConfig();
            translatorConfig.set_Class(ToggleSensorToStartStopTranslator.class.getCanonicalName());
            translatorConfig.setParameterList(null);
            List<String> translateTo1 = new Vector<String>();
            translateTo1.add(Commands.MOVEMENT_START_STOP.toString());
            translatorConfig.setTranslateToList(translateTo1);
            edgeConfig2.setTranslator(translatorConfig);
            edgeConfig2.setTargetDeviceIdList(targetDeviceIds);
            controlEdges.add(edgeConfig2);

            EdgeConfig edgeConfig3 = new EdgeConfig();
            edgeConfig3.setName("First Derivative");
            edgeConfig3.setCommandType(Commands.SENSOR_SINGLE_VALUE.toString());
            edgeConfig3.setSourcePlugin("org.ambientdynamix.contextplugins.pitchtracker");
            edgeConfig3.setTargetPlugin("org.ambientdynamix.contextplugins.ardrone");
            TranslatorConfig translatorConfig2 = new TranslatorConfig();
            translatorConfig2.set_Class(SingleValueToUpDownTranslator.class.getCanonicalName());
            translatorConfig2.setParameterList(null);
            List<String> translateTo2 = new Vector<String>();
            translateTo2.add(Commands.MOVEMENT_UP.toString());
            translateTo2.add(Commands.MOVEMENT_DOWN.toString());
            translatorConfig2.setTranslateToList(translateTo2);
            edgeConfig3.setTranslator(translatorConfig2);
            controlEdges.add(edgeConfig3);

            config.setControlEdgeList(controlEdges);


            bfact = BindingDirectory.getFactory(ControlConfig.class);
            IMarshallingContext mctx = bfact.createMarshallingContext();
            mctx.setIndent(4);
            mctx.marshalDocument(config, "UTF-8", null,
                    new FileOutputStream("SpheroDrone.xml"));
            StringWriter stringWriter = new StringWriter();
            mctx.marshalDocument(config, "UTF-8", null, stringWriter);
            System.out.println(stringWriter.toString());
//            try {
//                ControlGraphConfig configuration = new ControlGraphConfig(stringWriter.toString());
//                System.out.println(configuration.marshal());
//
//            } catch (ClassNotFoundException e) {
//                e.printStackTrace();
//            } catch (IllegalAccessException e) {
//                e.printStackTrace();
//            } catch (InstantiationException e) {
//                e.printStackTrace();
//            }
        } catch (JiBXException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }


    public String getClientPlugin() {
        return clientPlugin;
    }

    public void setClientPlugin(String clientPlugin) {
        this.clientPlugin = clientPlugin;
    }

    public Set<String> getServerPlugins() {
        return serverPlugins;
    }

    public void setServerPlugins(Set<String> serverPlugins) {
        this.serverPlugins = serverPlugins;
    }

    public Map<String, List<Edge>> getControlEdges() {
        return controlEdges;
    }

    public String marshal() throws JiBXException {
        ControlConfig config = new ControlConfig();
        config.setReceiver(clientPlugin);
        ArrayList<String> controllers = new ArrayList<String>();
        for (String serverPlugin : serverPlugins) {
            controllers.add(serverPlugin);
        }
        config.setControllerList(controllers);

        ArrayList<EdgeConfig> ctrlEdges = new ArrayList<EdgeConfig>();
        for (List<Edge> edges : controlEdges.values()) {
            ctrlEdges.addAll(marshalEdges(edges));
        }
        config.setControlEdgeList(ctrlEdges);

        IBindingFactory bfact = BindingDirectory.getFactory(ControlConfig.class);
        IMarshallingContext mctx = bfact.createMarshallingContext();
        mctx.setIndent(4);
        StringWriter stringWriter = new StringWriter();
        mctx.marshalDocument(config, "UTF-8", null, stringWriter);
        return stringWriter.toString();
    }

    private ArrayList<EdgeConfig> marshalEdges(Collection<Edge> edges) {
        ArrayList<EdgeConfig> controlEdges = new ArrayList<EdgeConfig>();
        for (Edge controlEdge : edges) {
            EdgeConfig edgeConfig = new EdgeConfig();
            edgeConfig.setName(controlEdge.getName());
            edgeConfig.setCommandType(controlEdge.getCommandType().toString());
            edgeConfig.setSourcePlugin(controlEdge.getSourcePlugin());
            edgeConfig.setSourceDeviceIdList(controlEdge.getSourceDeviceIds());
            edgeConfig.setTargetPlugin(controlEdge.getTargetPlugin());
            edgeConfig.setTargetDeviceIdList(controlEdge.getTargetDeviceIds());
            if (controlEdge.getTranslator() != null) {
                TranslatorConfig translatorConfig = new TranslatorConfig();
                translatorConfig.set_Class(controlEdge.getTranslator().getClass().getCanonicalName());
                translatorConfig.setParameterList(new Vector<Parameter>());
                for (Map.Entry<String, String> parameter : controlEdge.getTranslator().getConfig().entrySet()) {
                    Parameter xmlParameter = new Parameter();
                    xmlParameter.setName(parameter.getKey());
                    xmlParameter.setValue(parameter.getValue());
                    translatorConfig.getParameterList().add(xmlParameter);
                }
                List<String> translateTo = new Vector<String>();
                for (String command : controlEdge.getTranslator().getTranslateTo()) {
                    translateTo.add(command);
                }
                translatorConfig.setTranslateToList(translateTo);
                edgeConfig.setTranslator(translatorConfig);
            }
            controlEdges.add(edgeConfig);
        }
        return controlEdges;
    }
}
