package org.ambientdynamix.contextplugins.ambientcontrol;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by workshop on 3/13/14.
 */
public class PYRSensor extends IControlMessage {
    /**
     * Required CREATOR field that generates instances of this Parcelable class from a Parcel.
     *
     * @see http://developer.android.com/reference/android/os/Parcelable.Creator.html
     */
    public static Parcelable.Creator<PYRSensor> CREATOR = new Parcelable.Creator<PYRSensor>() {
        /**
         * Create a new instance of the Parcelable class, instantiating it from the given Parcel whose data had
         * previously been written by Parcelable.writeToParcel().
         */
        public PYRSensor createFromParcel(Parcel in) {
            return new PYRSensor(in);
        }

        /**
         * Create a new array of the Parcelable class.
         */
        public PYRSensor[] newArray(int size) {
            return new PYRSensor[size];
        }
    };


//    horizontal position pitch = 0 if front of device faces away from user and top faces up. pitch falls from [0 -90 0] as nose goes up.
//    pitch rises from [0 90 0] if nose goes down.
//    if upside down with nose facing towards user pitch value goes to 0
    private int pitch;

//  yaw ranges betwen [-180 180] degrees. It decreases if device spins clock wise and increases if device spins counter clock wise.
//  transistions 180 <-> -180 if passing the +/-180 degrees marc
    private int yaw;

//    horizontal position roll = 0 if front of device faces away from user and top faces up. roll rises from [0 90] as device rolls clockwise to
//    90 degrees, and from [90 180] if continuing clockwise roll = +/-180 if device is up side down.
//    roll goes down from [0 -90] if rolling counter clock wise up to 90 degrees and then from [-90 180] until upside down
//    if upside down with nose facing away from user pitch transitions from 180 <-> -180 if turn continues
    private int roll;



    /**
     * Returns the type of the context information represented by the IContextInfo. This string must match one of the
     * supported context information type strings described by the source ContextPlugin.
     */
    @Override
    public String getContextType() {
        return CONTEXT_TYPE;
    }

    /**
     * Returns the fully qualified class-name of the class implementing the IContextInfo interface. This allows Dynamix
     * applications to dynamically cast IContextInfo objects to their original type using reflection. A Java
     * "instanceof" compare can also be used for this purpose.
     */
    @Override
    public String getImplementingClassname() {
        return this.getClass().getName();
    }

    /**
     * Returns a Set of supported string-based context representation format types or null if no representation formats
     * are supported. Examples formats could include MIME, Dublin Core, RDF, etc. See the plug-in documentation for
     * supported representation types.
     */
    @Override
    public Set<String> getStringRepresentationFormats() {
        Set<String> formats = new HashSet<String>();
        formats.add("text/plain");
        return formats;
    }

    /**
     * Returns a string-based representation of the IContextInfo for the specified format string (e.g.
     * "application/json") or null if the requested format is not supported.
     */
    @Override
    public String getStringRepresentation(String format) {
        if (format.equalsIgnoreCase("text/plain"))
            return "command: " + " pyr: " + pitch + "'" + yaw + "'" + roll;
        else
            // Format not supported, so return an empty string
            return "";
    }

    /**
     * Create a MyBatteryLevelInfo
     */
    public PYRSensor(int pitch, int yaw, int roll,String name) {
        super(Commands.SENSOR_PYR, name);
        this.pitch = pitch;
        this.yaw = yaw;
        this.roll = roll;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    };

    /**
     * Used by Parcelable when sending (serializing) data over IPC.
     */
    public void writeParcel(Parcel out, int flags) {
        out.writeInt(pitch);
        out.writeInt(yaw);
        out.writeInt(roll);
    }

    /**
     * Used by the Parcelable.Creator when reconstructing (deserializing) data sent over IPC.
     */
    private PYRSensor(final Parcel in) {
        super(in);
        pitch = in.readInt();
        yaw = in.readInt();
        roll = in.readInt();
    }

    /**
     * Default implementation that returns 0.
     *
     * @return 0
     */
    @Override
    public int describeContents() {
        return 0;
    }

    public int getPitch() {
        return pitch;
    }

    public int getYaw() {
        return yaw;
    }

    public int getRoll() {
        return roll;
    }

    public void setPitch(int pitch) {
        this.pitch = pitch;
    }

    public void setYaw(int yaw) {
        this.yaw = yaw;
    }

    public void setRoll(int roll) {
        this.roll = roll;
    }

}
