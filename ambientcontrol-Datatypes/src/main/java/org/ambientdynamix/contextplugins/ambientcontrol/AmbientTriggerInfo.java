package org.ambientdynamix.contextplugins.ambientcontrol;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by workshop on 04-Sep-14.
 */
public class AmbientTriggerInfo extends IControlMessage{
    /**
     * Required CREATOR field that generates instances of this Parcelable class from a Parcel.
     *
     * @see http://developer.android.com/reference/android/os/Parcelable.Creator.html
     */
    public static Parcelable.Creator<AmbientTriggerInfo> CREATOR = new Parcelable.Creator<AmbientTriggerInfo>() {
        /**
         * Create a new instance of the Parcelable class, instantiating it from the given Parcel whose data had
         * previously been written by Parcelable.writeToParcel().
         */
        public AmbientTriggerInfo createFromParcel(Parcel in) {
            return new AmbientTriggerInfo(in);
        }

        /**
         * Create a new array of the Parcelable class.
         */
        public AmbientTriggerInfo[] newArray(int size) {
            return new AmbientTriggerInfo[size];
        }
    };

    private String triggerContextType;
    private String pluginId;
    private int hashKey;

    public AmbientTriggerInfo(String name, String triggerContextType, String pluginId, int hashKey) {
        super(Commands.TRIGGER, name);
        this.triggerContextType = triggerContextType;
        this.pluginId = pluginId;
        this.hashKey = hashKey;
    }

    /**
     * Returns the type of the context information represented by the IContextInfo. This string must match one of the
     * supported context information type strings described by the source ContextPlugin.
     */
    @Override
    public String getContextType() {
        return CONTEXT_TYPE;
    }

    /**
     * Returns the fully qualified class-name of the class implementing the IContextInfo interface. This allows Dynamix
     * applications to dynamically cast IContextInfo objects to their original type using reflection. A Java
     * "instanceof" compare can also be used for this purpose.
     */
    @Override
    public String getImplementingClassname() {
        return this.getClass().getName();
    }

    /**
     * Returns a Set of supported string-based context representation format types or null if no representation formats
     * are supported. Examples formats could include MIME, Dublin Core, RDF, etc. See the plug-in documentation for
     * supported representation types.
     */
    @Override
    public Set<String> getStringRepresentationFormats() {
        Set<String> formats = new HashSet<String>();
        formats.add("text/plain");
        return formats;
    }

    /**
     * Returns a string-based representation of the IContextInfo for the specified format string (e.g.
     * "application/json") or null if the requested format is not supported.
     */
    @Override
    public String getStringRepresentation(String format) {
        if (format.equalsIgnoreCase("text/plain"))
            return "Trigger: triggerContextType = " + triggerContextType + " pluginId = " + pluginId + " hashKey = " + hashKey;
        else
            // Format not supported, so return an empty string
            return "";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    };

    /**
     * Used by Parcelable when sending (serializing) data over IPC.
     */
    public void writeParcel(Parcel out, int flags) {
        out.writeString(triggerContextType);
        out.writeString(pluginId);
        out.writeInt(hashKey);
    }

    /**
     * Used by the Parcelable.Creator when reconstructing (deserializing) data sent over IPC.
     */
    private AmbientTriggerInfo(final Parcel in) {
        super(in);
        triggerContextType = in.readString();
        pluginId = in.readString();
        hashKey = in.readInt();
    }

    /**
     * Default implementation that returns 0.
     *
     * @return 0
     */
    @Override
    public int describeContents() {
        return 0;
    }

    public String getTriggerContextType() {
        return triggerContextType;
    }

    public String getPluginId() {
        return pluginId;
    }

    public int getHashKey() {
        return hashKey;
    }

}
