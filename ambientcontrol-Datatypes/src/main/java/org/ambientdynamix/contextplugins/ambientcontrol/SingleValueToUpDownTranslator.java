package org.ambientdynamix.contextplugins.ambientcontrol;

import java.util.*;

/**
 * Created by workshop on 07-Jul-14.
 */
public class SingleValueToUpDownTranslator extends Translator {

    private static final String[] generatedCommands = new String[]{Commands.MOVEMENT_UP, Commands.MOVEMENT_DOWN};

    @Override
    public String getTranslateFrom() {
        return Commands.SENSOR_SINGLE_VALUE;
    }

    @Override
    public String[] getGeneratedCommands() {
        return generatedCommands;
    }


    @Override
    public IControlMessage process(IControlMessage info) {
        if (info instanceof SingleValueSensor) {
            SingleValueSensor sensor = (SingleValueSensor) info;
            if (sensor.getValue() == 0)
                return new ToggleCommand(Commands.MOVEMENT_UP, 0,info.getName());
            else if (sensor.getValue() > 0)
                return new ToggleCommand(Commands.MOVEMENT_UP, 0.5,info.getName());

            else
                return new ToggleCommand(Commands.MOVEMENT_DOWN, -0.5,info.getName());
        } else
            throw new RuntimeException("cannot translate this contextType " + info.getContextType());
    }
}
