package org.ambientdynamix.contextplugins.ambientcontrol;

import java.util.*;

/**
 * Created by workshop on 6/3/14.
 */
public class ToggleSensorToPlaybackStopTranslator extends Translator {

    private static final String[] generatedCommands = new String[]{Commands.PLAYBACK_STOP};



    @Override
    public String getTranslateFrom() {
        return Commands.SENSOR_TOGGLE;
    }

    @Override
    public String[] getGeneratedCommands() {
        return generatedCommands;
    }

    @Override
    public IControlMessage process(IControlMessage info) {
        if (info instanceof ToggleSensor) {
            return new ToggleCommand(Commands.PLAYBACK_STOP,info.getName());
        }else {
            throw new RuntimeException("cannot translate this contextType " + info.getContextType());
        }
    }
}
