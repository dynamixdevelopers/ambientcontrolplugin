package org.ambientdynamix.contextplugins.ambientcontrol;

import java.util.*;

/**
 * Created by maxpagel on 17.11.14.
 */
public class AppConnection extends ControlConnection{

    private Set<String> commandTypesRegistered;
    private Map<String,Translator> stringTranslatorMap;

    public AppConnection(UUID id, Collection<String> commandTypes){
        this.id = id;
        commandTypesRegistered  = new HashSet<String>();
        stringTranslatorMap = new HashMap<String, Translator>();
        commandTypesRegistered.addAll(commandTypes);
    }

    @Override
    public IControlMessage process(IControlMessage command, String sourcePluginId) {
        if(stringTranslatorMap.containsKey(command.getCommand()))
            return stringTranslatorMap.get(command.getCommand()).translate(command);
        else return command;
    }

    public Set<String> getCommandTypesRegistered() {
        return commandTypesRegistered;
    }

    public Map<String, Translator> getStringTranslatorMap() {
        return stringTranslatorMap;
    }

    public void setStringTranslatorMap(Map<String, Translator> stringTranslatorMap) {
        this.stringTranslatorMap = stringTranslatorMap;
    }
}
