package org.ambientdynamix.contextplugins.ambientcontrol;

import android.util.Log;

import java.util.*;

/**
 * Created by workshop on 6/3/14.
 */
public class ToggleSensorToStartStopTranslator extends Translator {

    private static final String[] generatedCommands = new String[]{Commands.MOVEMENT_START_STOP};

    @Override
    public String getTranslateFrom() {
        return Commands.SENSOR_TOGGLE;
    }

    @Override
    public String[] getGeneratedCommands() {
        return generatedCommands;
    }


    @Override
    public IControlMessage process(IControlMessage info) {
        if (info instanceof ToggleSensor) {
            Log.i("ToggleSensorToStartStopTranslator", "translating toggle to Start stop");
            return new ToggleCommand(Commands.MOVEMENT_START_STOP,info.getName());
        }else {
            throw new RuntimeException("cannot translate this contextType " + info.getContextType());
        }
    }
}
