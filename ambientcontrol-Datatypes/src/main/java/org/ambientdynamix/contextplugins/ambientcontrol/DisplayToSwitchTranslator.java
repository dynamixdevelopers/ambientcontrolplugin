package org.ambientdynamix.contextplugins.ambientcontrol;

/**
 * Created by maxpagel on 03.11.14.
 */
public class DisplayToSwitchTranslator extends Translator {
    private static final String[] generatedCommands = new String[]{Commands.SWITCH};
    private final String TAG = this.getClass().getSimpleName();

    @Override
    public String getTranslateFrom() {
        return Commands.DISPLAY_COLOR;
    }

    @Override
    public IControlMessage process(IControlMessage info) {
//        Log.i(TAG, "process Message!! " + info);
        if (info instanceof DisplayCommand && info.getCommand().equals(Commands.DISPLAY_COLOR)) {
            DisplayCommand command = (DisplayCommand) info;

            if(command.getR()==0 && command.getG() == 0 && command.getB() == 0)
                return new ToggleCommand(Commands.SWITCH,false,command.getName());
            else
                return new ToggleCommand(Commands.SWITCH,true,command.getName());

        } else
            throw new RuntimeException("cannot translate this contextType " + info.getContextType());
    }

    @Override
    public String[] getGeneratedCommands() {
        return generatedCommands;
    }
}
