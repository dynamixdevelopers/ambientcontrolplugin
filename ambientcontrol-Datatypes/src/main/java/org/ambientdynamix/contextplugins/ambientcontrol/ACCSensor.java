package org.ambientdynamix.contextplugins.ambientcontrol;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by workshop on 3/25/14.
 */
public class ACCSensor extends IControlMessage{

    /**
     * Required CREATOR field that generates instances of this Parcelable class from a Parcel.
     *
     * @see http://developer.android.com/reference/android/os/Parcelable.Creator.html
     */
    public static Parcelable.Creator<ACCSensor> CREATOR = new Parcelable.Creator<ACCSensor>() {
        /**
         * Create a new instance of the Parcelable class, instantiating it from the given Parcel whose data had
         * previously been written by Parcelable.writeToParcel().
         */
        public ACCSensor createFromParcel(Parcel in) {
            return new ACCSensor(in);
        }

        /**
         * Create a new array of the Parcelable class.
         */
        public ACCSensor[] newArray(int size) {
            return new ACCSensor[size];
        }
    };


    private double x;
    private double y;
    private double z;


    /**
     * Returns the type of the context information represented by the IContextInfo. This string must match one of the
     * supported context information type strings described by the source ContextPlugin.
     */
    @Override
    public String getContextType() {
        return CONTEXT_TYPE;
    }

    /**
     * Returns the fully qualified class-name of the class implementing the IContextInfo interface. This allows Dynamix
     * applications to dynamically cast IContextInfo objects to their original type using reflection. A Java
     * "instanceof" compare can also be used for this purpose.
     */
    @Override
    public String getImplementingClassname() {
        return this.getClass().getName();
    }

    /**
     * Returns a Set of supported string-based context representation format types or null if no representation formats
     * are supported. Examples formats could include MIME, Dublin Core, RDF, etc. See the plug-in documentation for
     * supported representation types.
     */
    @Override
    public Set<String> getStringRepresentationFormats() {
        Set<String> formats = new HashSet<String>();
        formats.add("text/plain");
        return formats;
    }

    /**
     * Returns a string-based representation of the IContextInfo for the specified format string (e.g.
     * "application/json") or null if the requested format is not supported.
     */
    @Override
    public String getStringRepresentation(String format) {
        if (format.equalsIgnoreCase("text/plain"))
            return "ACC sensor data: " + " xyz: " + x + "'" + y + "'" + z;
        else
            // Format not supported, so return an empty string
            return "";
    }

    /**
     * Create a MyBatteryLevelInfo
     */
    public ACCSensor( double x, double y, double z, String name) {
        super(Commands.SENSOR_ACC, name);
        this.x = x;
        this.y = y;
        this.z = z;
    }



    /**
     * {@inheritDoc}
     */


    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    };

    /**
     * Used by Parcelable when sending (serializing) data over IPC.
     */
    public void writeParcel(Parcel out, int flags) {
        out.writeDouble(x);
        out.writeDouble(y);
        out.writeDouble(z);
    }

    /**
     * Used by the Parcelable.Creator when reconstructing (deserializing) data sent over IPC.
     */
    private ACCSensor(final Parcel in) {
        super(in);
        x = in.readDouble();
        y = in.readDouble();
        z = in.readDouble();

    }

    /**
     * Default implementation that returns 0.
     *
     * @return 0
     */
    @Override
    public int describeContents() {
        return 0;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getZ() {
        return z;
    }

    public void setZ(double z) {
        this.z = z;
    }
}
