package org.ambientdynamix.contextplugins.ambientcontrol;

import org.ambientdynamix.api.application.ContextHandler;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by maxpagel on 17.11.14.
 */
public class GraphConnection extends ControlConnection{

    private ContextHandler contextHandler;
    private Map<String, UUID> subscriptions;

    private ControlGraphConfig controlGraphConfig;

    public GraphConnection(ControlGraphConfig controlGraphConfig) {
        this.controlGraphConfig = controlGraphConfig;
        subscriptions = new HashMap<String, UUID>();
    }

    public ControlGraphConfig getControlGraphConfig() {
        return controlGraphConfig;
    }

    public ContextHandler getContextHandler() {
        return contextHandler;
    }

    public void setContextHandler(ContextHandler contextHandler) {
        this.contextHandler = contextHandler;
    }

    @Override
    public IControlMessage process(IControlMessage command, String sourcePluginId) {
        Edge edge = null;
        for (Edge edgeCandidate : controlGraphConfig.getControlEdges().get(command.getCommand())) {
            if (edgeCandidate.getName().equals(command.getName()) && edgeCandidate.getTargetDeviceIds().containsAll(command.getTargetDeviceIds()) && (edgeCandidate.getSourceDeviceIds().isEmpty() || edgeCandidate.getSourceDeviceIds().contains(command.getSourceDeviceID()) || command.getSourceDeviceID().equals(""))) {
                edge = edgeCandidate;
                break;
            }
        }

        if (edge != null) {
//            Log.i(TAG, "found edge");
            return edge.process(command);
        } else
            return command;
    }

    public Map<String, UUID> getSubscriptions() {
        return subscriptions;
    }

    public void addSubscription(String plugin, UUID subscription) {
        this.subscriptions.put(plugin, subscription);
    }

}
