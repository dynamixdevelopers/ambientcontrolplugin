package org.ambientdynamix.contextplugins.ambientcontrol;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by workshop on 3/25/14.
 */
public class DisplayCommand extends IControlMessage {

    /**
     * Required CREATOR field that generates instances of this Parcelable class from a Parcel.
     *
     * @see http://developer.android.com/reference/android/os/Parcelable.Creator.html
     */
    public static Parcelable.Creator<DisplayCommand> CREATOR = new Parcelable.Creator<DisplayCommand>() {
        /**
         * Create a new instance of the Parcelable class, instantiating it from the given Parcel whose data had
         * previously been written by Parcelable.writeToParcel().
         */
        public DisplayCommand createFromParcel(Parcel in) {
            return new DisplayCommand(in);
        }

        /**
         * Create a new array of the Parcelable class.
         */
        public DisplayCommand[] newArray(int size) {
            return new DisplayCommand[size];
        }
    };

    private String url;
    private int red;
    private int green;
    private int blue;
    private int transitionTime;
    private String deviceId;
    private String message;


    public int getTransitionTime() {
        return transitionTime;
    }

    public int getR() {
        return red;
    }

    public int getG() {
        return green;
    }

    public int getB() {
        return blue;
    }

    public String getURL() {
        return url;
    }


    /**
     * Returns the type of the context information represented by the IContextInfo. This string must match one of the
     * supported context information type strings described by the source ContextPlugin.
     */
    @Override
    public String getContextType() {
        return CONTEXT_TYPE;
    }

    /**
     * Returns the fully qualified class-name of the class implementing the IContextInfo interface. This allows Dynamix
     * applications to dynamically cast IContextInfo objects to their original type using reflection. A Java
     * "instanceof" compare can also be used for this purpose.
     */
    @Override
    public String getImplementingClassname() {
        return this.getClass().getName();
    }

    /**
     * Returns a Set of supported string-based context representation format types or null if no representation formats
     * are supported. Examples formats could include MIME, Dublin Core, RDF, etc. See the plug-in documentation for
     * supported representation types.
     */
    @Override
    public Set<String> getStringRepresentationFormats() {
        Set<String> formats = new HashSet<String>();
        formats.add("text/plain");
        return formats;
    }

    /**
     * Returns a string-based representation of the IContextInfo for the specified format string (e.g.
     * "application/json") or null if the requested format is not supported.
     */
    @Override
    public String getStringRepresentation(String format) {
        if (format.equalsIgnoreCase("text/plain"))
            return "Display command: " + command + " " + red + " " + green + " " + blue + " " + url;
        else
            // Format not supported, so return an empty string
            return "";
    }

    public DisplayCommand(String command, int red, int green, int blue, String name) {
        this(command,red,green,blue,0,name);
    }

    public DisplayCommand(String command, int red, int green, int blue, int transitionTime, String name) {
        super(command,name);
        this.red = red;
        this.green = green;
        this.blue = blue;
        this.transitionTime = transitionTime;
    }

    public DisplayCommand(String command, String url, String name) {
        super(command,name);
        this.url = url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setRed(int red) {
        this.red = red;
    }

    public void setGreen(int green) {
        this.green = green;
    }

    public void setBlue(int blue) {
        this.blue = blue;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    };

    /**
     * Used by Parcelable when sending (serializing) data over IPC.
     */
    public void writeParcel(Parcel out, int flags) {
        out.writeString(url);
        out.writeInt(red);
        out.writeInt(green);
        out.writeInt(blue);
        out.writeString(deviceId);

    }

    /**
     * Used by the Parcelable.Creator when reconstructing (deserializing) data sent over IPC.
     */
    private DisplayCommand(final Parcel in) {
        super(in);
        url = in.readString();
        red = in.readInt();
        green = in.readInt();
        blue = in.readInt();
        deviceId = in.readString();
    }

    /**
     * Default implementation that returns 0.
     *
     * @return 0
     */
    @Override
    public int describeContents() {
        return 0;
    }
}
