package org.ambientdynamix.contextplugins.ambientcontrol;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by workshop on 3/25/14.
 */
public class KeyPressCommand extends IControlMessage{

    /**
     * Required CREATOR field that generates instances of this Parcelable class from a Parcel.
     *
     * @see http://developer.android.com/reference/android/os/Parcelable.Creator.html
     */
    public static Parcelable.Creator<KeyPressCommand> CREATOR = new Parcelable.Creator<KeyPressCommand>() {
        /**
         * Create a new instance of the Parcelable class, instantiating it from the given Parcel whose data had
         * previously been written by Parcelable.writeToParcel().
         */
        public KeyPressCommand createFromParcel(Parcel in) {
            return new KeyPressCommand(in);
        }

        /**
         * Create a new array of the Parcelable class.
         */
        public KeyPressCommand[] newArray(int size) {
            return new KeyPressCommand[size];
        }
    };

    public enum Keypress{
        KEY_DOWN("KEY_DOWN"),
        KEY_UP("KEY_UP");

        private String code;

        private Keypress(String code){

            this.code = code;
        }

        @Override
        public String toString() {
            return code;
        }
    }

    private String keypressType;
    private String keyCode;

    /**
     * Returns the type of the context information represented by the IContextInfo. This string must match one of the
     * supported context information type strings described by the source ContextPlugin.
     */
    @Override
    public String getContextType() {
        return CONTEXT_TYPE;
    }

    /**
     * Returns the fully qualified class-name of the class implementing the IContextInfo interface. This allows Dynamix
     * applications to dynamically cast IContextInfo objects to their original type using reflection. A Java
     * "instanceof" compare can also be used for this purpose.
     */
    @Override
    public String getImplementingClassname() {
        return this.getClass().getName();
    }

    /**
     * Returns a Set of supported string-based context representation format types or null if no representation formats
     * are supported. Examples formats could include MIME, Dublin Core, RDF, etc. See the plug-in documentation for
     * supported representation types.
     */
    @Override
    public Set<String> getStringRepresentationFormats() {
        Set<String> formats = new HashSet<String>();
        formats.add("text/plain");
        return formats;
    }

    /**
     * Returns a string-based representation of the IContextInfo for the specified format string (e.g.
     * "application/json") or null if the requested format is not supported.
     */
    @Override
    public String getStringRepresentation(String format) {
        if (format.equalsIgnoreCase("text/plain"))
            return "Key press: " + keyCode + " " + keypressType.toString();
        else
            // Format not supported, so return an empty string
            return "";
    }

    public KeyPressCommand(Keypress keypressType, String keyCode, String name) {
        super(Commands.SENSOR_KEYPRESS,name);
        this.keypressType = keypressType.toString();
        this.keyCode = keyCode;
    }

    /**
     * {@inheritDoc}
     */


    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    };

    /**
     * Used by Parcelable when sending (serializing) data over IPC.
     */
    public void writeParcel(Parcel out, int flags) {
        out.writeString(keypressType);
        out.writeString(keyCode);

    }

    /**
     * Used by the Parcelable.Creator when reconstructing (deserializing) data sent over IPC.
     */
    private KeyPressCommand(final Parcel in) {
        super(in);
        keypressType = in.readString();
        keyCode = in.readString();
    }

    /**
     * Default implementation that returns 0.
     *
     * @return 0
     */
    @Override
    public int describeContents() {
        return 0;
    }

    public String getKeypressType() {
        return keypressType;
    }

    public void setKeypressType(String keypressType) {
        this.keypressType = keypressType;
    }

    public String getKeyCode() {
        return keyCode;
    }

    public void setKeyCode(String keyCode) {
        this.keyCode = keyCode;
    }


}
