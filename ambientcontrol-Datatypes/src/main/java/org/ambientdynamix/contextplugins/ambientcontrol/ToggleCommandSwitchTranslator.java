package org.ambientdynamix.contextplugins.ambientcontrol;

import android.util.Log;

/**
 * Created by MsMe on 25.09.2014.
 */
public class ToggleCommandSwitchTranslator extends Translator {
    private static final String[] generatedCommands = new String[]{Commands.SWITCH};
    private final String TAG = this.getClass().getSimpleName();
    private static boolean state = true;


    @Override
    public String getTranslateFrom() {
        return Commands.SENSOR_TOGGLE;
    }

    @Override
    public IControlMessage process(IControlMessage info) {
        Log.i(TAG, "process Message!! " + info);
        if (info instanceof ToggleSensor) {
            ToggleSensor sensor = (ToggleSensor) info;

            state = !state;
            if(state)
                return new ToggleCommand(Commands.SWITCH, true, sensor.getName());
            else
                return new ToggleCommand(Commands.SWITCH, false, sensor.getName());

        } else
            throw new RuntimeException("cannot translate this contextType " + info.getContextType());
    }

    @Override
    public String[] getGeneratedCommands() {
        return generatedCommands;
    }
}