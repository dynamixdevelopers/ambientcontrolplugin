package org.ambientdynamix.contextplugins.ambientcontrol;

import android.os.Bundle;

/**
 * Created by maxpagel on 11.05.15.
 */
public class BundleCommandParser {

    public static IControlMessage parseCommandBundle(Bundle command) {
        String commandType = command.getString(ControlConnectionManager.COMMAND_TYPE);
        IControlMessage message;
        switch (commandType) {
            case Commands.DISPLAY_COLOR:
                message = new DisplayCommand(commandType, Integer.parseInt(command.getString("RED","0")), Integer.parseInt(command.getString("GREEN","0")), Integer.parseInt(command.getString("BLUE","0")), Integer.parseInt(command.getString("TRANSITION", "0")), commandType);
                break;
            case Commands.DISPLAY_VIDEO:
            case Commands.DISPLAY_IMAGE:
            case Commands.DISPLAY_MESSAGE:
                message = new DisplayCommand(commandType, command.getString("URL",""), commandType);
                break;
            case Commands.SWITCH:
            case Commands.PLAYBACK_PLAY_PAUSE:
            case Commands.PLAYBACK_NEXT:
            case Commands.PLAYBACK_PREVIOUS:
            case Commands.PLAYBACK_STOP:
            case Commands.PLAYBACK_BACKWARD_SEEK:
            case Commands.PLAYBACK_FORWARD_SEEK:
            case Commands.MOVEMENT_NEUTRAL:
            case Commands.MOVEMENT_BACKWARD:
            case Commands.MOVEMENT_BACKWARD_LEFT:
            case Commands.MOVEMENT_BACKWARD_RIGHT:
            case Commands.MOVEMENT_DOWN:
            case Commands.MOVEMENT_FORWARD:
            case Commands.MOVEMENT_FORWARD_LEFT:
            case Commands.MOVEMENT_FORWARD_RIGHT:
            case Commands.MOVEMENT_LEFT:
            case Commands.MOVEMENT_RIGHT:
            case Commands.MOVEMENT_SPIN_CCW:
            case Commands.MOVEMENT_SPIN_CW:
            case Commands.MOVEMENT_UP:
                message = new ToggleCommand(commandType, Double.parseDouble(command.getString("VELOCITY", "100")), Boolean.parseBoolean(command.getString("DOWN", "true")), commandType);
                break;
            default:
                message = null;
        }
        if (message != null) {
            String deviceID = command.getString(ControlConnectionManager.TARGET_DEVICE_ID,"");
            if(!deviceID.equals("")){
                message.getTargetDeviceIds().add(deviceID);
            }
        }
        return message;

    }
}
