package org.ambientdynamix.contextplugins.ambientcontrol;

/**
 * Created by workshop on 6/27/14.
 */
public class PYRToPlaybackControlTranslator extends Translator {
    private static final String[] generatedCommands = new String[]{Commands.PLAYBACK_PLAY_PAUSE,
            Commands.PLAYBACK_FORWARD_SEEK, Commands.PLAYBACK_BACKWARD_SEEK, Commands.PLAYBACK_STOP, Commands.MOVEMENT_NEUTRAL};

    private static final int ROLL_THRESHOLD = 30;
    private static final int PITCH_THRESHOLD = 30;
    private final String TAG = this.getClass().getSimpleName();
    private static boolean state = true;
    private long lastCommand;



    @Override
    public String getTranslateFrom() {
        return Commands.SENSOR_PYR;
    }

    @Override
    public String[] getGeneratedCommands() {
        return generatedCommands;
    }

    @Override
    protected IControlMessage process(IControlMessage info) {
        if (info instanceof PYRSensor) {

            PYRSensor ipyrSensor = (PYRSensor) info;
            String command = Commands.MOVEMENT_NEUTRAL;
            float speed = 0;
            if (Math.abs(ipyrSensor.getRoll()) > ROLL_THRESHOLD || Math.abs(ipyrSensor.getPitch()) > PITCH_THRESHOLD) {
                float pitchNorm = (float) ipyrSensor.getPitch() / (float) Math.max(Math.abs(ipyrSensor.getPitch()), Math.abs(ipyrSensor.getRoll()));
                float rollNorm = (float) ipyrSensor.getRoll() / (float) Math.max(Math.abs(ipyrSensor.getPitch()), Math.abs(ipyrSensor.getRoll()));

                final float heading;

                if (pitchNorm > 0) { //forward
                    if (rollNorm < 0) { //left 270 - 359
                        heading = (315f + pitchNorm * 45f + rollNorm * 45f);
                    } else { // right 0 - 90
                        heading = (45f - pitchNorm * 45f + rollNorm * 45f);
                    }
                } else { //backward
                    if (rollNorm < 0) { //left 180 - 270
                        heading = (225f + pitchNorm * 45f - rollNorm * 45f);
                    } else { // right 90 - 180
                        heading = (135 - pitchNorm * 45f - rollNorm * 45f);
                    }
                }

                speed = (Math.abs(ipyrSensor.getRoll()) + Math.abs(ipyrSensor.getPitch())) / 360f;
//                Log.i("TAG", ipyrSensor.getPitch() +"," + pitchNorm + "," + ipyrSensor.getRoll() +"," +rollNorm + " h: " + heading + " s: " + speed) ;

                if (speed > 0.1) {
                    if (heading >= 330 || heading < 60) {
                        if(System.currentTimeMillis() - lastCommand > 1000) {
                            command = Commands.PLAYBACK_PLAY_PAUSE;
                            state = !state;
                        }else
                            command = Commands.MOVEMENT_NEUTRAL;
                        lastCommand = System.currentTimeMillis();
                    }
                    else if (heading >= 60 && heading < 150) {
                        command = Commands.PLAYBACK_FORWARD_SEEK;
                        state = false;
                    }
                    else if (heading >= 150 && heading < 250){
                        command = Commands.PLAYBACK_STOP;
                        state = false;
                    }
                    else if (heading >= 250 && heading < 330){
                        command = Commands.PLAYBACK_BACKWARD_SEEK;
                        state = false;
                    }
                }

//                Log.i(TAG, "heading: " + heading + " at speed: " + speed + " sending " + command);

            }
            return new ToggleCommand(command, (int) (100 * speed), state, info.getName());
        } else {
                throw new RuntimeException("cannot translate this contextType " + info.getContextType());
        }
    }
}
