package org.ambientdynamix.contextplugins.ambientcontrol;

/**
 * Created by Max Pagel on 29-Sep-14.
 */
public class PyrToUpDownTranslator extends Translator {

    private static final String[] generatedCommands = new String[]{Commands.MOVEMENT_UP, Commands.MOVEMENT_DOWN};

    @Override
    public String getTranslateFrom() {
        return Commands.SENSOR_PYR;
    }

    @Override
    public String[] getGeneratedCommands() {
        return generatedCommands;
    }


    @Override
    public IControlMessage process(IControlMessage info) {
        if (info instanceof PYRSensor) {
            PYRSensor sensor = (PYRSensor) info;
            float speed = (sensor.getPitch() - Math.signum(sensor.getPitch()) * 10) / 90.0f;
            if (sensor.getPitch() > 10)
                return new ToggleCommand(Commands.MOVEMENT_DOWN, speed, info.getName());
            else if (sensor.getPitch() < -10)
                return new ToggleCommand(Commands.MOVEMENT_UP, speed, info.getName());
            else
                return new ToggleCommand(Commands.MOVEMENT_DOWN, 0, info.getName());
        } else
            throw new RuntimeException("cannot translate this contextType " + info.getContextType());
    }
}
