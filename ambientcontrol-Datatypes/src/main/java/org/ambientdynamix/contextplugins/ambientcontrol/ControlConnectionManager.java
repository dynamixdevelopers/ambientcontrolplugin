package org.ambientdynamix.contextplugins.ambientcontrol;

import android.os.Bundle;
import android.os.Handler;
import android.os.RemoteException;
import android.util.Log;
import org.ambientdynamix.api.application.*;
import org.ambientdynamix.api.contextplugin.*;
import org.jibx.runtime.JiBXException;
import org.json.JSONException;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by workshop on 6/12/14.
 */
public class ControlConnectionManager {
    public static final String CONNECTION_CONTROL = "CONNECTION_CONTROL";
    public static final String CONFIG_TEARDOWN = "TEARDOWN";
    public static final String CONFIG_CONNECT_SERVER = "CONFIG_CONNECT_SERVER";
    public static final String SERVER_PLUGIN_ID = "SERVER_PLUGIN_ID";
    public static final String CONTEXT_TYPE = "CONTEXT_TYPE";
    public static final String CHANNEL = "CHANNEL";
    public static final String CLIENT_PLUGIN_ID = "CLIENT_PLUGIN_ID";
    public static final String CONFIG_INIT = "INIT";
    public static final String CONFIG_STOP = "STOP";
    public static final String CONFIG_SUBSCRIBE = "SUBSCRIBE";
    public static final String CONNECTION_CONFIG = "CONNECTION_CONFIG";
    public static final String CONTROL_COMMAND = "CONTROL_COMMAND";
    public static final String COMMAND = "COMMAND";
    public static final String TRIGGER = "TRIGGER";
    public static final String JASON_TRIGGER = "JASON_TRIGGER";
    public static final String TRIGGER_SOURCES = "TRIGGER_SOURCES";
    public static final String DEVICE_ID = "DEVICE_ID";
    public static final String REQUESTED_COMMANDS = "REQUESTED_COMMANDS";
    public static final String LOAD_SCENARIO = "LOAD_SCENARIO";
    public static final String SCENARIO_ID = "SCENARIO_ID";
    public static final String COMMAND_TYPE = "COMMAND_TYPE";
    public static final String TARGET_DEVICE_ID = "TARGET_DEVICE_ID";
    public static final String SCENARIO_XML = "SCENARIO_XML";


    private String TAG = this.getClass().getSimpleName();
    private final Handler handler = new Handler();
    private final IControllConectionManagerListener controlListener;

    private ContextPluginRuntime runtime;
    private DynamixFacade facade;
    private Map<ICallback, Bundle> frozenRequests;
    private ConcurrentHashMap<UUID, ControlConnection> controllChannels;
    private String thisPluginId;

    private IProfileMatcher profileMatcher;
    private boolean stopping;
    private TriggerManager triggerManager;


    public interface IControllConectionManagerListener {
        void controllRequest(String command, String name);

        void stopControlling(String command, String name);

        boolean consumeCommand(IControlMessage command, String sourcePluginId, String requestId);

        Set<String> getDeviceAdresses();
    }

    public static abstract class ControllConnectionManagerListenerAdapter implements IControllConectionManagerListener {
        @Override
        public boolean consumeCommand(IControlMessage command, String sourcePluginId, String requestId) {
            return true;
        }

        @Override
        public Set<String> getDeviceAdresses() {
            return new HashSet<>();
        }
    }

    public interface ACCallback {
        void onSuccess();

        void onFailure(String message, int errorCode);
    }

    public ControlConnectionManager(ContextPluginRuntime runtime, IControllConectionManagerListener controlListener,
                                    String tag) throws RemoteException {
        this.runtime = runtime;
        this.facade = runtime.getPluginFacade().getDynamixFacadeWrapper(runtime.getSessionId());
        this.controlListener = controlListener;
        this.profileMatcher = new TranslatingProfileMatcher();
        this.controllChannels = new ConcurrentHashMap<UUID, ControlConnection>(8, 0.9f, 1);
        this.frozenRequests = new HashMap<ICallback, Bundle>();
        this.TAG = "ControlConnectionManager: " + tag;
        this.thisPluginId = runtime.getPluginFacade().getPluginInfo(runtime.getSessionId()).getPluginId();
        this.triggerManager = new TriggerManager(runtime, this);
//        Debug.startMethodTracing("ControlConnectionManager");
    }

    private void openSession() throws RemoteException {
        facade.openSession(new ISessionCallback.Stub() {
                               @Override
                               public void onSuccess(DynamixFacade iDynamixFacade) throws RemoteException {
                                   for (Map.Entry<ICallback, Bundle> frozenRequest : frozenRequests.entrySet()) {
                                       initControlGraph(frozenRequest.getValue(), frozenRequest.getKey());
                                   }
                                   frozenRequests.clear();
                               }

                               @Override
                               public void onFailure(String s, int i) throws RemoteException {
                                   Log.e(TAG, "cannot open session");
                                   for (Map.Entry<ICallback, Bundle> frozenRequest : frozenRequests.entrySet()) {
                                       frozenRequest.getKey().onFailure(s, i);
                                   }
                                   frozenRequests.clear();
                               }
                           }
        );
    }

    public boolean handleConfiguredContextRequest(final UUID requestId, String contextType, Bundle config) {
        config.setClassLoader(this.getClass().getClassLoader());
        if (config != null && config.containsKey(ControlConnectionManager.CONNECTION_CONTROL)) {
            String connectionControl = config.getString(ControlConnectionManager.CONNECTION_CONTROL);
            if (connectionControl.equals(ControlConnectionManager.CONFIG_STOP)) {
                stop(new ACCallback() {
                    @Override
                    public void onSuccess() {
                        runtime.sendContextRequestSuccess(requestId);
                        try {
                            Log.i(TAG, "Closing session");
                            facade.closeSession();
                        } catch (RemoteException e) {
                            Log.w(TAG, "error closing Session");
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(String message, int errorCode) {
                        runtime.sendContextRequestError(requestId, message, errorCode);
                    }
                });
                return true;
            } else if (connectionControl.equals(ControlConnectionManager.CONFIG_INIT)) {
                Log.i(TAG, "Received configured context request to init control");
                try {
                    initControlGraph(config, new ICallback.Stub() {

                        @Override
                        public void onSuccess() throws RemoteException {
                            runtime.sendContextRequestSuccess(requestId);
                        }

                        @Override
                        public void onFailure(String s, int i) throws RemoteException {
                            runtime.sendContextRequestError(requestId, s, i);
                        }
                    });
                } catch (RemoteException e) {
                    Log.i(TAG, e.toString());
                }
                return true;
            } else if (connectionControl.equals(ControlConnectionManager.CONTROL_COMMAND)) {
                if (config.getBoolean(PluginConstants.WEB_REQUEST)) {
                    IControlMessage command = BundleCommandParser.parseCommandBundle(config);
                    if (command != null) {
                        controlListener.consumeCommand(command, command.getSourceDeviceID(), "");
                        if (command.getTargetDeviceIds().isEmpty()) {
                            runtime.sendContextRequestSuccess(requestId,DynamixHelper.InteractionEvent.createIntent(new ArrayList<>(controlListener.getDeviceAdresses())));
                        }else
                            runtime.sendContextRequestSuccess(requestId, DynamixHelper.InteractionEvent.createIntent(command.getTargetDeviceIds()));
                        return true;
                    } else {
                        return false;
                    }

                } else {
                    IControlMessage controlMessage = config.getParcelable(ControlConnectionManager.COMMAND);
                    if (controlMessage != null) {
                        controlListener.consumeCommand(controlMessage, controlMessage.getSourceDeviceID(), "");
                        if (controlMessage.getTargetDeviceIds().isEmpty()) {
                            runtime.sendContextRequestSuccess(requestId,DynamixHelper.InteractionEvent.createIntent(new ArrayList<>(controlListener.getDeviceAdresses())));
                        }else
                            runtime.sendContextRequestSuccess(requestId,DynamixHelper.InteractionEvent.createIntent(controlMessage.getTargetDeviceIds()));
                        return true;
                    }else{
                        return false;
                    }
                }
            } else if (connectionControl.equals(ControlConnectionManager.JASON_TRIGGER)) {
                try {
                    Log.i(TAG, "received JasonTrigger request with sources: " + config.getString(TRIGGER_SOURCES));
                    triggerManager.registerTrigger(new JsonTrigger(config.getString(TRIGGER_SOURCES),
                            config.getString(JASON_TRIGGER), config.getString(CONNECTION_CONFIG), this));
                } catch (JSONException e) {
                    e.printStackTrace();
                    return false;
                }
                return true;
            }
        }
        return false;
    }

    private ControlGraphConfig getControlConfiguration(Set<String> servers) throws RemoteException, IllegalAccessException, InstantiationException {
        final Collection<Edge> controlEdges = profileMatcher.matchProfiles(servers, thisPluginId);
        if (controlEdges == null) {
            Log.w(TAG, "No valid interaction could be found between given plugins");
            return null;
        }
        return new ControlGraphConfig(thisPluginId, servers, controlEdges);
    }

    public void initControlGraph(Bundle config, final ICallback callback) throws RemoteException {
        stopping = false;     //TODO this is hacky
        if (!facade.isSessionOpen()) {
            frozenRequests.put(callback, config);
            Log.w(TAG, "session not open yet, freeezing request");
            openSession();
            return;
        }

        final ControlGraphConfig controlGraphConfig;
        try {
            if (config.containsKey(CONNECTION_CONFIG))
                controlGraphConfig = new ControlGraphConfig(config.getString(CONNECTION_CONFIG));
            else {
                final ArrayList<String> serversArray = config.getStringArrayList(ControlConnectionManager.SERVER_PLUGIN_ID);
                Set<String> servers = new HashSet<String>();
                servers.addAll(serversArray);

                Log.i(TAG, "starting interaction:");
                for (String controller : servers) {
                    Log.i(TAG, "matching controller: " + controller);
                }
                controlGraphConfig = getControlConfiguration(servers);
                if (controlGraphConfig == null) {
                    callback.onFailure("No valid interaction could be found between given plugins", ErrorCodes.CONFIGURATION_ERROR);
                    return;
                }
            }
            Log.i(TAG, "Control config: " + controlGraphConfig.marshal());

        } catch (Exception e) {
            e.printStackTrace();
            callback.onFailure(e.toString(), ErrorCodes.CONFIGURATION_ERROR);
            return;
        }

        final GraphConnection controllerConnection = new GraphConnection(controlGraphConfig);

        facade.createContextHandler(new ContextHandlerCallback() {
            @Override
            public void onSuccess(ContextHandler handler) throws RemoteException {
                Log.i(TAG, "created ContextHandler");
                controllerConnection.setContextHandler(handler);
                controllerConnection.setId(handler.getId());
                ControlListener controlListener = new ControlListener(controllerConnection);


                Bundle config = new Bundle();
                config.putString(CONNECTION_CONTROL, CONFIG_SUBSCRIBE);
                config.putString(CHANNEL, controllerConnection.getId().toString());
                config.putString(CLIENT_PLUGIN_ID, thisPluginId);
                try {
                    config.putString(CONNECTION_CONFIG, controlGraphConfig.marshal());
                } catch (JiBXException e) {
                    e.printStackTrace();
                    callback.onFailure(e.toString(), ErrorCodes.CONFIGURATION_ERROR);
                }
                List<String> servers = new Vector<String>();
                servers.addAll(controlGraphConfig.getServerPlugins());
                final int[] countSupport = new int[1];
                handler.addContextSupport(servers, IControlMessage.CONTEXT_TYPE, config, controlListener, new ContextSupportCallback() {
                    @Override
                    public void onSuccess(ContextSupportInfo contextSupportInfo) throws RemoteException {
                        countSupport[0]++;
                        controllChannels.put(controllerConnection.getId(), controllerConnection);
                        Log.i(TAG, countSupport[0] + " - all contextsupport added");

                        final Bundle b = new Bundle();
                        b.putString(CONNECTION_CONTROL, ControlConnectionManager.CONFIG_CONNECT_SERVER);
                        b.putString(CHANNEL, controllerConnection.getId().toString());
                        final int[] countServers = new int[1];
                        for (String serverId : controlGraphConfig.getServerPlugins()) {
                            Log.i(TAG, "sending control init command to server: " + serverId);
                            runtime.sendMessage(serverId, b, new IMessageResultHandler() {
                                @Override
                                public void onResult(Bundle bundle) {
                                    countServers[0]++;
                                    Log.i(TAG, "Connection " + controllerConnection.getId() + " succesfull to server " + controllerConnection.getControlGraphConfig().getServerPlugins());
                                    if (countServers[0] == controlGraphConfig.getServerPlugins().size())
                                        try {
                                            callback.onSuccess();
                                        } catch (RemoteException e) {
                                            Log.e(TAG, e.toString());
                                        }
                                }

                                @Override
                                public void onFailure(String s, int errorCode) {
                                    countServers[0]++;
                                    Log.e(TAG, "Error Connecting " + controllerConnection.getId() + "  to server " + controllerConnection.getControlGraphConfig().getServerPlugins() + " " + s);
                                    if (countServers[0] == controlGraphConfig.getServerPlugins().size())
                                        try {
                                            callback.onFailure(s, errorCode);
                                        } catch (RemoteException e) {
                                            Log.e(TAG, e.toString());
                                        }
                                }
                            });
                        }
                    }

                    @Override
                    public void onFailure(String message, int errorCode) throws RemoteException {
                        Log.e(TAG, "error adding context support: " + message + " code: " + errorCode);
                        try {
                            callback.onFailure(message, errorCode);
                        } catch (RemoteException e) {
                            Log.e(TAG, e.toString());
                        }
                    }
                });
            }


            @Override
            public void onFailure(String s, int errorCode) {
                Log.e(TAG, "Error: Creating ContextHandler with contextSupport failed" + s);
                try {
                    callback.onFailure(s, errorCode);
                } catch (RemoteException e) {
                    Log.e(TAG, e.toString());
                }
            }
        });
    }


    public boolean addContextListener(ContextListenerInformation listenerInfo) {
        if (listenerInfo.getListenerConfig() == null)
            return false;
        try {
            Bundle requestConfig = listenerInfo.getListenerConfig();
            Log.i(TAG, "got requestConfig");

            if (requestConfig.containsKey(CONNECTION_CONTROL) && requestConfig.getString(CONNECTION_CONTROL).equals(CONFIG_SUBSCRIBE)) {
                ControlConnection connection;
                stopping = false;
                Log.i(TAG, "Config valid");
                final UUID channelId = UUID.fromString(requestConfig.getString(CHANNEL));
                Log.i(TAG, "context subscription added for " + listenerInfo.getContextType() + " on connection: " + listenerInfo.getListenerId());
                Log.i(TAG, "UUID valid for channel: " + channelId);
                connection = controllChannels.get(channelId);
                if (connection == null) {
                    Log.i(TAG, "creating new connection");
                    connection = new GraphConnection(new ControlGraphConfig(requestConfig.getString(CONNECTION_CONFIG)));
                    connection.setId(channelId);
                    ((GraphConnection) connection).addSubscription(requestConfig.getString(CLIENT_PLUGIN_ID), listenerInfo.getListenerId());
                    controllChannels.put(connection.getId(), connection);
                    return true;
                } else if (connection instanceof GraphConnection) {
                    Log.i(TAG, "found connection");
                    ((GraphConnection) connection).addSubscription(requestConfig.getString(CLIENT_PLUGIN_ID), listenerInfo.getListenerId());
                    Log.i(TAG, "set subscription");
                    return true;
                } else
                    Log.e(TAG, "Connection already registered as App Connection, This should never happen");
                return false;
            } else if (listenerInfo.getContextType().equals(IControlMessage.CONTEXT_TYPE)) {
                List<String> commands;
                if (requestConfig.getBoolean(PluginConstants.WEB_REQUEST, false)) {
//                    read csv
                    String commandsString = requestConfig.getString(REQUESTED_COMMANDS);
                    String[] split = commandsString.split(",");
                    commands = Arrays.asList(split);
                } else {
                    commands = requestConfig.getStringArrayList(REQUESTED_COMMANDS);
                }
                if (commands != null) {
                    stopping = false;
                    Log.i(TAG, "creating new Single point connection");
                    AppConnection connection = new AppConnection(listenerInfo.getListenerId(), commands);
                    TranslatingProfileMatcher translatingProfileMatcher = new TranslatingProfileMatcher();
                    Map<String, Translator> stringTranslatorMap = translatingProfileMatcher.matchAppConnection(commands, thisPluginId);
                    if (stringTranslatorMap != null) {
                        connection.setStringTranslatorMap(stringTranslatorMap);
                        controllChannels.put(connection.getId(), connection);
                        for (String command : commands) {
                            boolean translated = false;
                            for (Translator translator : stringTranslatorMap.values()) {
                                if (translator.getTranslateTo().contains(command)) {
                                    translated = true;
                                    controlListener.controllRequest(translator.getTranslateFrom(), "");
                                    break;
                                }
                            }
                            if (!translated)
                                controlListener.controllRequest(command, "");
                        }

                    } else {
                        Log.e(TAG, "requested commands cannot be provided by this plugin");
                        return false;
                    }
                }
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return false;
    }

    private Set<String> commandsFromStrings(List<String> strings) {
        Set<String> commands = new HashSet<String>();
        for (String string : strings) {
            commands.add(String.valueOf(string));
        }
        return commands;
    }

    private ArrayList<String> stringListFromCommands(Set<String> commands) {
        ArrayList<String> commandsArray = new ArrayList<String>();
        for (String command : commands) {
            commandsArray.add(command.toString());
        }
        return commandsArray;
    }

    private List<Message> frozenMessages = new Vector<Message>();

    private void openSessionAfterMessage() throws RemoteException {
        facade.openSession(new ISessionCallback.Stub() {
                               @Override
                               public void onSuccess(DynamixFacade iDynamixFacade) throws RemoteException {
                                   for (Message frozenMessage : frozenMessages) {
                                       handleConfigCommand(frozenMessage);
                                   }
                                   frozenMessages.clear();
                               }

                               @Override
                               public void onFailure(String s, int i) throws RemoteException {
                                   Log.e(TAG, "cannot open session");
                                   for (Message frozenMessage : frozenMessages) {
                                       frozenMessage.getResultHandler().onFailure(s, i);
                                   }
                                   frozenMessages.clear();
                               }
                           }
        );
    }

    public boolean handleConfigCommand(final Message message) {

        final Bundle configurationBundle = message.getMessage();
        if (configurationBundle == null || !configurationBundle.containsKey(ControlConnectionManager.CONNECTION_CONTROL)) {
            return false;
        }
        try {
            if (!facade.isSessionOpen()) {
                frozenMessages.add(message);
                Log.w(TAG, "session not open yet, freeezing message");
                openSessionAfterMessage();
                return true;
            }
        } catch (RemoteException e) {
            if (message.getResultHandler() != null) {
                message.getResultHandler().onFailure("Error opening Session", ErrorCodes.APPLICATION_EXCEPTION);
            }
            return false;
        }
        UUID channel = UUID.fromString(configurationBundle.getString(ControlConnectionManager.CHANNEL));
        ControlConnection conn = controllChannels.get(channel);
        if (conn != null && !(conn instanceof GraphConnection)) {
            if (message.getResultHandler() != null) {
                message.getResultHandler().onFailure("no valid connection found", ErrorCodes.MISSING_CONFIGURATION);
            }
            return false;
        }
        final GraphConnection connection = (GraphConnection) conn;
        String config = configurationBundle.getString(ControlConnectionManager.CONNECTION_CONTROL);

        if (config.equals(ControlConnectionManager.CONFIG_TEARDOWN)) {
            Log.i(TAG, "received config teardown message for channel " + channel.toString());
            removeChannel(connection, new ACCallback() {
                @Override
                public void onSuccess() {
                    if (message.getResultHandler() != null) {
                        message.getResultHandler().onResult(configurationBundle);
                    }
                    try {
                        Log.i(TAG, "Closing session");
                        facade.closeSession();
                    } catch (RemoteException e) {
                        Log.w(TAG, "error closing session");
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(String errorMessage, int errorCode) {
                    if (message.getResultHandler() != null) {
                        message.getResultHandler().onFailure(errorMessage, errorCode);
                    }
                }
            });

            return true;

        } else if (config.equals(ControlConnectionManager.CONFIG_CONNECT_SERVER)) {
            startControlling(connection);
            message.getResultHandler().onResult(new Bundle());
            return true;
        }

        return false;
    }

    private void startControlling(final GraphConnection connection) {
        stopping = false;
        Log.i(TAG, "start controlling for connection: " + connection.getId());
//        Log.i(TAG,"Opened controll Channel with id: " + requestId.toString());
        IControllConectionManagerListener listener;
        Collection<Edge> edges = new Vector<Edge>();


        Log.i(TAG, "client: " + connection.getControlGraphConfig().getClientPlugin() + " server: " + connection.getControlGraphConfig().getServerPlugins());

        listener = controlListener;
        for (List<Edge> edgeList : connection.getControlGraphConfig().getControlEdges().values()) {
            edges.addAll(edgeList);
        }

        if (listener == null)
            return;

        for (Edge edge : edges) {
            if (!edge.getSourcePlugin().equals(thisPluginId))
                continue;
            Log.i(TAG, "start controlling with " + edge);
            listener.controllRequest(edge.getCommandType(), edge.getName());
        }
    }

    public void sendControllCommand(IControlMessage controllEvent, String sourceDeviceId) {
        if (stopping)
            return;
//        Log.d(TAG, "sending " + controllEvent.getCommand() + " to controllChannels: " + controllChannels.size());
        boolean sent = false;
        for (ControlConnection pluginConnection : controllChannels.values()) {
            if (pluginConnection instanceof GraphConnection) {
                GraphConnection connection = (GraphConnection) pluginConnection;
                List<Edge> edges = null;
                String clientPluginId = connection.getControlGraphConfig().getClientPlugin();
                Set<String> serverPlugins = connection.getControlGraphConfig().getServerPlugins();

                edges = connection.getControlGraphConfig().getControlEdges().get(controllEvent.getCommand());

                if (edges != null && edges.size() != 0) {
                    for (Edge edge : edges) {
//                        Log.i(TAG, "sending " + controllEvent.getCommand() + " from: " + thisPluginId + ":" + sourceDeviceId +" to " + connection.getControlGraphConfig().getClientPlugin() + ":" + edge.getTargetDeviceIds());
                        if (edge.getName().equals(controllEvent.getName()) && (edge.getSourceDeviceIds().isEmpty() || edge.getSourceDeviceIds().contains(sourceDeviceId))) {
                            UUID uuid = connection.getSubscriptions().get(edge.getTargetPlugin());
                            controllEvent.setTargetDeviceIds(edge.getTargetDeviceIds());
                            controllEvent.setSourceDeviceID(sourceDeviceId);
                            if (serverPlugins.contains(clientPluginId)
                                    && thisPluginId.equals(clientPluginId)) {//client and server plugin are the same, looping back
                                controllEvent.getTargetDeviceIds().remove(sourceDeviceId);
//                                Log.i(TAG, "sending " + controllEvent.getCommand() + " from: " + thisPluginId + ":" + sourceDeviceId +" to " + connection.getControlGraphConfig().getClientPlugin() + ":" + edge.getTargetDeviceIds());
                                if (controllEvent.getTargetDeviceIds().size() > 0)
                                    controlListener.consumeCommand(controllEvent, thisPluginId, "");
                            } else {
                                runtime.sendContextEvent(uuid, controllEvent,DynamixHelper.InteractionEvent.createIntent(sourceDeviceId));
                            }
                            sent = true;
                        }
                    }
                }

            } else if (pluginConnection instanceof AppConnection) {
                AppConnection connection = (AppConnection) pluginConnection;
                IControlMessage controlMessage = connection.process(controllEvent, thisPluginId);
                if (connection.getCommandTypesRegistered().contains(controlMessage.getCommand()))
                    runtime.sendContextEvent(connection.getId(), controlMessage,DynamixHelper.InteractionEvent.createIntent(sourceDeviceId));
            }

        }
//        if(!sent && controlListener != null){
//            controlListener.stopControlling(controllEvent.getCommand(),controllEvent.getName());
//        }
    }


    private class ControlListener extends ContextListener {

        private ControlConnection connection;

        private ControlListener(ControlConnection connection) {
            this.connection = connection;
        }

        @Override
        public void onContextResult(ContextResult event) throws RemoteException {
//            if (connection != null && event.hasIContextInfo() && event.getIContextInfo() instanceof IControlMessage) {
//                Log.i(TAG, thisPluginId + " received controlMessage: " + ((IControlMessage) event.getIContextInfo()).getCommand().toString() + "on channel: " + UUID.fromString(event.getResponseId()));
//            }
            if (connection != null && event.hasIContextInfo() && event.getIContextInfo() instanceof IControlMessage) {
//                Log.i(TAG, thisPluginId + " received controlMessage: " + ((IControlMessage) event.getIContextInfo()).getCommand().toString());
                IControlMessage controlMessage = connection.process((IControlMessage) event.getIContextInfo(), event.getResultSource().getPluginId());
//                TODO allow more than one edge from an output
//                Log.i(TAG, "translated to: " + controlMessage.getCommand().toString() + " " + controlMessage.getClass().getCanonicalName());
//                Log.i(TAG,"listener: " + listener.toString());
//                Log.i(TAG,"connection: " + connection.getId());
//                Log.i(TAG, "event.getContextInfo.getCommand(): " + controlMessage.getCommand());
//                Log.i(TAG, "UUID string: " + event.getResponseId());
                controlListener.consumeCommand(controlMessage, event.getResultSource().getPluginId(), event.getResponseId());
            }
        }
    }


    /**
     * gather a list of all
     *
     * @return
     */

    public Set<String> getAllOutgoingInteractions() {
        Set<String> interactions = new HashSet<String>();
        for (ControlConnection controllChannel : controllChannels.values()) {
            if (controllChannel instanceof GraphConnection) {
                GraphConnection connection = (GraphConnection) controllChannel;
                for (List<Edge> edges : connection.getControlGraphConfig().getControlEdges().values()) {
                    for (Edge edge : edges) {
                        interactions.add(edge.getName());
                    }
                }
            }
        }
        return interactions;
    }

    /**
     * This removes all data associated with the channel, but not the channel from controllChannels. You have to do that manually later.
     *
     * @param conn The connection to remove
     */
    private void removeChannel(ControlConnection conn, final ACCallback callback) {
        if (conn instanceof AppConnection) {
            controllChannels.remove(conn.getId());
            for (String command : ((AppConnection) conn).getCommandTypesRegistered()) {
                controlListener.stopControlling(command, "");
            }
            return;
        }
        if (conn instanceof GraphConnection) {
            GraphConnection connection = (GraphConnection) conn;
            new Thread(new Runnable() {
                @Override
                public void run() {
                    for (int i = 0; i < 10; i++) {
                        for (ControlConnection controlConnection : controllChannels.values()) {
                            Log.i(TAG, "has open connection: " + ((GraphConnection) controlConnection).getSubscriptions());
                        }
                        Log.i(TAG, "scan for open channels done");
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e1) {
                            e1.printStackTrace();
                        }
                    }
                }
            }).start();

            if (connection == null) {
                callback.onFailure("conection not found", ErrorCodes.NOT_FOUND);
                return;
            }
            controllChannels.remove(connection.getId());
            Set<String> outgoingInteractions = getAllOutgoingInteractions();
            Map<String, List<Edge>> edgesToCut;
            ControlConnectionManager.IControllConectionManagerListener listener;

            edgesToCut = connection.getControlGraphConfig().getControlEdges();
            listener = controlListener;

            if (listener == null) {
                callback.onSuccess();
                return;
            }
            for (List<Edge> edges : edgesToCut.values()) {
                for (Edge edge : edges) {
                    if (edge.getSourcePlugin().equals(thisPluginId) && !outgoingInteractions.contains(edge.getName())) {
                        Log.i(TAG, "stopping Control of " + edge.getCommandType() + " as it is not longer needed");
                        listener.stopControlling(edge.getCommandType(), edge.getName());
                    } else {
                        Log.i(TAG, "command " + edge.getCommandType() + " is still needed");
                    }
                }
            }
            if (connection.getContextHandler() != null) {
                Log.i(TAG, "destroying Context Handler: " + connection.getContextHandler().getId());
                try {
                    facade.removeContextHandler(connection.getContextHandler().getContextHandler(), new Callback() {
                        @Override
                        public void onSuccess() throws RemoteException {
                            callback.onSuccess();
                        }

                        @Override
                        public void onFailure(String message, int errorCode) throws RemoteException {
                            callback.onFailure(message, errorCode);
                        }
                    });
                } catch (RemoteException e) {
                    Log.e(TAG, "Error removing Context handler");
                    callback.onFailure("Error removing Context handler", ErrorCodes.DYNAMIX_FRAMEWORK_ERROR);
                }
            } else
                callback.onSuccess();
        }
    }

    public void disconnectChannel(final ControlConnection connection, final ACCallback callback) {
        Log.i(TAG, "disconnecting Channel " + connection.getId().toString());
        sendDisconnectMessage(connection, new IMessageResultHandler() {
            @Override
            public void onResult(Bundle bundle) {
                callback.onSuccess();
            }

            @Override
            public void onFailure(String s, int errorCode) {
                callback.onFailure(s, errorCode);
            }
        });
    }

    private void sendDisconnectMessage(ControlConnection conn, IMessageResultHandler resultHandler) {
        if (conn instanceof GraphConnection) {
            GraphConnection connection = (GraphConnection) conn;
            Bundle b = new Bundle();
            b.putString(ControlConnectionManager.CONNECTION_CONTROL, ControlConnectionManager.CONFIG_TEARDOWN);
            b.putString(ControlConnectionManager.CHANNEL, connection.getId().toString());


            if (connection != null) {
                if (connection.getControlGraphConfig().getClientPlugin().equals(thisPluginId))
                    for (String serverId : connection.getControlGraphConfig().getServerPlugins()) {
                        runtime.sendMessage(serverId, b, resultHandler);
                    }
                else
                    runtime.sendMessage(connection.getControlGraphConfig().getClientPlugin(), b, resultHandler);
            }
        }

    }

    public void stop(){
        stop(null);
    }

    public void stop(final ACCallback callback) {
        stopping = true;
        Log.i(TAG, "stopping all controls");
        final int[] connectionCounter = {0};
        final int numConnections = controllChannels.size();
        triggerManager.stop();
        if (controllChannels.size() > 0) {
            for (final ControlConnection pluginConnection : controllChannels.values()) {
                final int[] counter = {0};
                if (pluginConnection instanceof GraphConnection) {
                    final int numRequest = ((GraphConnection) pluginConnection).getControlGraphConfig().getServerPlugins().size();

                    disconnectChannel(pluginConnection, new ACCallback() {
                        @Override
                        public void onSuccess() {
                            counter[0]++;
                            Log.i(TAG, "canceled: " + counter[0] + " of " + numRequest);
                            if (counter[0] == numRequest) {
                                removeChannel(pluginConnection, new ACCallback() {
                                    @Override
                                    public void onSuccess() {
                                        connectionCounter[0]++;
                                        if (connectionCounter[0] == numConnections)
                                            controllChannels.clear();
                                    }

                                    @Override
                                    public void onFailure(String message, int errorCode) {
                                        connectionCounter[0]++;
                                        if (connectionCounter[0] == numConnections)
                                            controllChannels.clear();
                                    }
                                });

                                if(callback != null)
                                    callback.onSuccess();
                            }
                        }

                        @Override
                        public void onFailure(String message, int errorCode) {
                            Log.e(TAG, message + " errorcode: " + errorCode);
                            Log.i(TAG, "canceled: " + counter[0] + " of " + numRequest);
                            counter[0]++;
                            if (counter[0] == numRequest) {
                                removeChannel(pluginConnection, new ACCallback() {
                                    @Override
                                    public void onSuccess() {
                                        connectionCounter[0]++;
                                        if (connectionCounter[0] == numConnections)
                                            controllChannels.clear();
                                    }

                                    @Override
                                    public void onFailure(String message, int errorCode) {
                                        connectionCounter[0]++;
                                        if (connectionCounter[0] == numConnections)
                                            controllChannels.clear();
                                    }
                                });
                                if(callback != null)
                                    callback.onSuccess();
                            }
                        }
                    });
                } else {
                    removeChannel(pluginConnection, new ACCallback() {
                        @Override
                        public void onSuccess() {
                            connectionCounter[0]++;
                            if (connectionCounter[0] == numConnections)
                                controllChannels.clear();
                        }

                        @Override
                        public void onFailure(String message, int errorCode) {
                            connectionCounter[0]++;
                            if (connectionCounter[0] == numConnections)
                                controllChannels.clear();
                        }
                    });
                    if(callback != null)
                        callback.onSuccess();
                }
            }
        } else
        if(callback != null)
            callback.onSuccess();

    }

    public ControlConnection getConnection(UUID uuid) {
        for (ControlConnection pluginConnection : controllChannels.values()) {
            if (pluginConnection.getId().equals(uuid))
                return pluginConnection;
        }
        return null;
    }

    public Collection<ControlConnection> getControllChannels() {
        return controllChannels.values();
    }

    public ContextPluginRuntime getRuntime() {
        return runtime;
    }
}
