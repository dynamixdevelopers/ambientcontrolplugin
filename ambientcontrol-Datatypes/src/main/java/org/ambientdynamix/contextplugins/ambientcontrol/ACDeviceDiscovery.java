/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.contextplugins.ambientcontrol;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.Gson;
import org.ambientdynamix.api.contextplugin.DynamixHelper;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ACDeviceDiscovery implements IACDeviceDiscovery {
	/**
	 * Required CREATOR field that generates instances of this Parcelable class from a Parcel.
	 * 
	 * @see http://developer.android.com/reference/android/os/Parcelable.Creator.html
	 */
	public static Parcelable.Creator<ACDeviceDiscovery> CREATOR = new Parcelable.Creator<ACDeviceDiscovery>() {
		/**
		 * Create a new instance of the Parcelable class, instantiating it from the given Parcel whose data had
		 * previously been written by Parcelable.writeToParcel().
		 */
		public ACDeviceDiscovery createFromParcel(Parcel in) {
			return new ACDeviceDiscovery(in);
		}

		/**
		 * Create a new array of the Parcelable class.
		 */
		public ACDeviceDiscovery[] newArray(int size) {
			return new ACDeviceDiscovery[size];
		}
	};
	// Public static variable for our supported context type
	public static String CONTEXT_TYPE = "org.ambientdynamix.contextplugins.ambientcontrol.discovery";
	// Private data
	private List<String> deviceIdList = new ArrayList();
	private String type;
	private String radio;

	/**
	 * Returns the type of the context information represented by the IContextInfo. This string must match one of the
	 * supported context information type strings described by the source ContextPlugin.
	 */
	@Override
	public String getContextType() {
		return CONTEXT_TYPE;
	}

	/**
	 * Returns the fully qualified class-name of the class implementing the IContextInfo interface. This allows Dynamix
	 * applications to dynamically cast IContextInfo objects to their original type using reflection. A Java
	 * "instanceof" compare can also be used for this purpose.
	 */
	@Override
	public String getImplementingClassname() {
        return this.getClass().getName();
	}

	/**
	 * Returns a Set of supported string-based context representation format types or null if no representation formats
	 * are supported. Examples formats could include MIME, Dublin Core, RDF, etc. See the plug-in documentation for
	 * supported representation types.
	 */
	@Override
	public Set<String> getStringRepresentationFormats() {
		Set<String> formats = new HashSet<String>();
		formats.add("text/plain");
		return formats;
	}

	/**
	 * Returns a string-based representation of the IContextInfo for the specified format string (e.g.
	 * "application/json") or null if the requested format is not supported.
	 */
	@Override
	public String getStringRepresentation(String format) {
		if (format.equalsIgnoreCase("text/plain"))
			return "Discovery: " + new Gson().toJson(deviceIdList);
		else if(format.equals("application/json")){
			StringBuilder builder = new StringBuilder();
			builder.append("{\"deviceIdList\": ");
            builder.append(new Gson().toJson(deviceIdList));
			builder.append(",\"radio\": \"");
			builder.append(radio);
			builder.append("\",\"type\": \"");
			builder.append(type);
			builder.append("\"}");
			return builder.toString();
        }
			// Format not supported, so return an empty string
		return "";
	}

	/**
	 * Create a MyBatteryLevelInfo
	 *
	 *            The device's detected battery level as a percentage of 100.
	 */
	public ACDeviceDiscovery(Set<String> devices, String type, DynamixHelper.Connection radio) {
		this.type = type;
		this.radio = radio.toString();
		deviceIdList = new ArrayList();
        deviceIdList.addAll(devices);
	}

	@Override
    public List<String> getDeviceIDList() {
        return deviceIdList;
    }

    @Override
	public String toString() {
		return this.getClass().getSimpleName();
	};

	/**
	 * Used by Parcelable when sending (serializing) data over IPC.
	 */
	public void writeToParcel(Parcel out, int flags) {
		out.writeString(new Gson().toJson(deviceIdList));
		out.writeString(type);
		out.writeString(radio);
	}

	/**
	 * Used by the Parcelable.Creator when reconstructing (deserializing) data sent over IPC.
	 */
	private ACDeviceDiscovery(final Parcel in) {
        Gson gson = new Gson();
        String[] arrayDeviceIds  = gson.fromJson(in.readString(),String[].class);

        deviceIdList = new ArrayList();
	    for(int i = 0; i<arrayDeviceIds.length;i++){
            deviceIdList.add(arrayDeviceIds[i]);
        }
		this.type = in.readString();
		this.radio = in.readString();
    }

	/**
	 * Default implementation that returns 0.
	 * 
	 * @return 0
	 */
	@Override
	public int describeContents() {
		return 0;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getRadio() {
		return radio;
	}

	public void setRadio(String radio) {
		this.radio = radio;
	}
}