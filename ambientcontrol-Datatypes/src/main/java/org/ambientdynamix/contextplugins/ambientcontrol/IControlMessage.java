package org.ambientdynamix.contextplugins.ambientcontrol;

import android.os.Parcel;
import org.ambientdynamix.api.application.IContextInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by workshop on 4/9/14.
 */
public abstract class IControlMessage implements IContextInfo {

    protected String command  = "";
    protected String name  = "";
    protected ArrayList<String> targetDeviceIds = new ArrayList<String>();
    protected String sourceDeviceID = "";


    public String getCommand() {
        return command;
    }


    public void setCommand(String command) {

        this.command = command;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    protected IControlMessage(String command, String name) {
        this.command = command;
        this.name = name;
    }

    public final void writeToParcel(Parcel out, int flags) {
        out.writeString(command.toString());
        out.writeString(name);
        out.writeList(targetDeviceIds);
        out.writeString(sourceDeviceID);
        writeParcel(out,flags);
    }

    public abstract void writeParcel(Parcel out, int flags);

    protected IControlMessage(final Parcel in) {
        command = in.readString();
        name = in.readString();
        targetDeviceIds = in.readArrayList(this.getClass().getClassLoader());
        sourceDeviceID = in.readString();
    }

    public static final String CONTEXT_TYPE = "org.ambientdynamix.contextplugins.ambientcontrol.controlmessage";

    public void setTargetDeviceIds(ArrayList<String> targetDeviceIds) {
        this.targetDeviceIds = targetDeviceIds;
    }

    public ArrayList<String> getTargetDeviceIds() {
        return targetDeviceIds;
    }

    public String getSourceDeviceID() {
        return sourceDeviceID;
    }

    public void setSourceDeviceID(String sourceDeviceID) {
        this.sourceDeviceID = sourceDeviceID;
    }
}
