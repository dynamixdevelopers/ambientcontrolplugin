package org.ambientdynamix.contextplugins.ambientcontrol;

import android.util.Log;

import java.io.IOException;
import java.util.*;

/**
 * Created by workshop on 3/25/14.
 */
public class ProfileMatcher implements IProfileMatcher {

    private final String TAG = this.getClass().getSimpleName();

    /**
     * Generates a map of all available control combinations
     *
     * @param controllers Set of Control IDs
     * @return
     */
    public Map<String,Map<String,String>> generateControllerProfile(Set<String> controllers){
        Map<String,Map<String, String>> controls = new HashMap<String, Map<String, String>>();
        for (String controller : controllers) {
            Log.i(TAG,"matching controller: " + controller);
            ControlProfile controllerProfile = profiles.get(controller);
            if(controllerProfile == null){
                Log.w(TAG, "no profile for controller: " + controller);
                continue;
            }
            for (String command : controllerProfile.getOutputList().values()) {
                Log.i(TAG,"adding " + command.toString());
            }
            controls.put(controller,controllerProfile.getOutputList());
        }
        return controls;
    }

    protected Map<String,ControlProfile> profiles;

    protected ControlProfile spheroProfile;
    protected ControlProfile arDroneProfile;
    protected ControlProfile ambientMediaProfile;
    protected ControlProfile pitchControllerProfile;
    protected ControlProfile hueProfile;
    protected ControlProfile activityRecognitionProfile;
    protected ControlProfile wemoProfile;
    protected ControlProfile myoProfile;
    /**
     * This method can be used to generate some example Profiles trough java Objects.
     * Those will be written into the database after generating them.
     *
     */
    public void generateProfilesFromJava(){
        Log.e(TAG,"Couldn't load Plugin profiles falling back to local definition");
        spheroProfile = new ControlProfile();
        arDroneProfile = new ControlProfile();
        ambientMediaProfile = new ControlProfile();
        pitchControllerProfile = new ControlProfile();
        hueProfile = new ControlProfile();
        activityRecognitionProfile = new ControlProfile();
        wemoProfile = new ControlProfile();
        myoProfile = new ControlProfile();

        spheroProfile.setPluginVersion("1.0.0");
        arDroneProfile.setPluginVersion("1.0.0");
        ambientMediaProfile.setPluginVersion("1.0.0");
        pitchControllerProfile.setPluginVersion("1.0.0");
        hueProfile.setPluginVersion("1.0.0");
        activityRecognitionProfile.setPluginVersion("1.0.0");
        wemoProfile.setPluginVersion("1.0.0");
        myoProfile.setPluginVersion("1.0.0");

        profiles = new HashMap<String, ControlProfile>();


        spheroProfile.setArtifact_id("org.ambientdynamix.contextplugins.spheronative");
        arDroneProfile.setArtifact_id("org.ambientdynamix.contextplugins.ardrone");
        ambientMediaProfile.setArtifact_id("org.ambientdynamix.contextplugins.ambientmedia");
        pitchControllerProfile.setArtifact_id("org.ambientdynamix.contextplugins.pitchtracker");
        hueProfile.setArtifact_id("org.ambientdynamix.contextplugins.hueplugin");
        activityRecognitionProfile.setArtifact_id("org.ambientdynamix.contextplugins.activityrecognition");
        wemoProfile.setArtifact_id("org.ambientdynamix.contextplugins.wemoplugin");
        myoProfile.setArtifact_id("org.ambientdynamix.contextplugins.myoplugin");

        spheroProfile.setName("spheronative");
        arDroneProfile.setName("ardrone");
        ambientMediaProfile.setName("ambientmedia");
        pitchControllerProfile.setName("pitchtracker");
        hueProfile.setName("hueplugin");
        activityRecognitionProfile.setName("activityrecognition");
        wemoProfile.setName("wemoplugin");
        myoProfile.setName("myoplugin");

        profiles.put("org.ambientdynamix.contextplugins.spheronative",spheroProfile);
        profiles.put("org.ambientdynamix.contextplugins.ardrone",arDroneProfile);
        profiles.put("org.ambientdynamix.contextplugins.ambientmedia",ambientMediaProfile);
        profiles.put("org.ambientdynamix.contextplugins.pitchtracker",pitchControllerProfile);
        profiles.put("org.ambientdynamix.contextplugins.hueplugin",hueProfile);
        profiles.put("org.ambientdynamix.contextplugins.activityrecognition",activityRecognitionProfile);
        profiles.put("org.ambientdynamix.contextplugins.wemoplugin",wemoProfile);
        profiles.put("org.ambientdynamix.contextplugins.myoplugin", myoProfile);

        spheroProfile.addOptionalControls(Commands.DISPLAY_COLOR);
        spheroProfile.addOptionalControls(Commands.SWITCH);

        spheroProfile.addOptionalControls(Commands.DISPLAY_COLOR);
        ControlProfile.ControllableProfileItem ideal = spheroProfile.createProfileforPriority(1);
        ideal.addMandatory(Commands.SENSOR_PYR);

        ControlProfile.ControllableProfileItem ideal2 = spheroProfile.createProfileforPriority(2);
        ideal2.addMandatory(Commands.SENSOR_AXIS);

        ControlProfile.ControllableProfileItem minimum = spheroProfile.createProfileforPriority(3);
        minimum.addMandatory(Commands.MOVEMENT_BACKWARD);
        minimum.addMandatory(Commands.MOVEMENT_BACKWARD_LEFT);
        minimum.addMandatory(Commands.MOVEMENT_BACKWARD_RIGHT);
        minimum.addMandatory(Commands.MOVEMENT_FORWARD);
        minimum.addMandatory(Commands.MOVEMENT_FORWARD_LEFT);
        minimum.addMandatory(Commands.MOVEMENT_FORWARD_RIGHT);
        minimum.addMandatory(Commands.MOVEMENT_LEFT);
        minimum.addMandatory(Commands.MOVEMENT_RIGHT);

        ControlProfile.ControllableProfileItem display = spheroProfile.createProfileforPriority(4);
        display.addMandatory(Commands.DISPLAY_COLOR);

        ControlProfile.ControllableProfileItem switchable = spheroProfile.createProfileforPriority(5);
        switchable.addMandatory(Commands.SWITCH);

        spheroProfile.addOutput("Gyroscope",Commands.SENSOR_GYRO);
        spheroProfile.addOutput("Pitch Yaw Roll",Commands.SENSOR_PYR);
        spheroProfile.addOutput("Accelerometer",Commands.SENSOR_ACC);
        spheroProfile.addOutput("Collision",Commands.SENSOR_TOGGLE);


//        arDroneProfile.addOptional(Commands.SENSOR_ACC);
        arDroneProfile.addOptionalControls(Commands.MOVEMENT_UP);
        arDroneProfile.addOptionalControls(Commands.MOVEMENT_DOWN);


//        spheroProfile.addOutput("Gyroscope",Commands.SENSOR_GYRO);
//        spheroProfile.addOutput("Pitch Yaw Roll",Commands.SENSOR_PYR);
//        spheroProfile.addOutput("Accelerometer",Commands.SENSOR_ACC);
////        spheroProfile.addAvailableControl(Commands.DISPLAY_COLOR);
//        spheroProfile.addOutput("Collision",Commands.SENSOR_TOGGLE);
//
//        arDroneProfile.addOptionalControls(Commands.SENSOR_ACC);
//        arDroneProfile.addOptionalControls(Commands.MOVEMENT_UP);
//        arDroneProfile.addOptionalControls(Commands.MOVEMENT_DOWN);

        ControlProfile.ControllableProfileItem droneIdeal = arDroneProfile.createProfileforPriority(1);
        droneIdeal.addMandatory(Commands.SENSOR_PYR);
        droneIdeal.addMandatory(Commands.MOVEMENT_START_STOP);

        ControlProfile.ControllableProfileItem droneIdeal2 = arDroneProfile.createProfileforPriority(2);
        droneIdeal2.addMandatory(Commands.SENSOR_AXIS);
        droneIdeal.addMandatory(Commands.MOVEMENT_START_STOP);

        ControlProfile.ControllableProfileItem droneMinimum = arDroneProfile.createProfileforPriority(3);
        droneMinimum.addMandatory(Commands.MOVEMENT_BACKWARD);
        droneMinimum.addMandatory(Commands.MOVEMENT_BACKWARD_LEFT);
        droneMinimum.addMandatory(Commands.MOVEMENT_BACKWARD_RIGHT);
        droneMinimum.addMandatory(Commands.MOVEMENT_FORWARD);
        droneMinimum.addMandatory(Commands.MOVEMENT_FORWARD_LEFT);
        droneMinimum.addMandatory(Commands.MOVEMENT_FORWARD_RIGHT);
        droneMinimum.addMandatory(Commands.MOVEMENT_LEFT);
        droneMinimum.addMandatory(Commands.MOVEMENT_RIGHT);
        droneMinimum.addMandatory(Commands.MOVEMENT_START_STOP);

        arDroneProfile.addOutput("Accelerometer",Commands.SENSOR_ACC);
        arDroneProfile.addOutput("Pitch Yaw Roll",Commands.SENSOR_PYR);
        arDroneProfile.addOutput("Gyroscope",Commands.SENSOR_GYRO);

        ambientMediaProfile.addOptionalControls(Commands.DISPLAY_IMAGE);
        ambientMediaProfile.addOptionalControls(Commands.DISPLAY_VIDEO);
        ambientMediaProfile.addOptionalControls(Commands.PLAYBACK_BACKWARD_SEEK);
        ambientMediaProfile.addOptionalControls(Commands.PLAYBACK_FORWARD_SEEK);
        ambientMediaProfile.addOptionalControls(Commands.PLAYBACK_NEXT);
        ambientMediaProfile.addOptionalControls(Commands.PLAYBACK_PREVIOUS);
        ambientMediaProfile.addOptionalControls(Commands.PLAYBACK_STOP);

        ControlProfile.ControllableProfileItem mediaminimum = ambientMediaProfile.createProfileforPriority(1);
        mediaminimum.addMandatory(Commands.PLAYBACK_PLAY_PAUSE);

        ambientMediaProfile.addOutput("Playback Color",Commands.DISPLAY_COLOR);

        pitchControllerProfile.addOutput("Pitch",Commands.SENSOR_SINGLE_VALUE);
        pitchControllerProfile.addOutput("Loudness",Commands.SENSOR_SINGLE_VALUE);
        pitchControllerProfile.addOutput("First Derivative",Commands.SENSOR_SINGLE_VALUE);
        pitchControllerProfile.addOutput("Second Derivative",Commands.SENSOR_SINGLE_VALUE);

        ControlProfile.ControllableProfileItem hueIdeal1 = hueProfile.createProfileforPriority(1);
        hueIdeal1.addMandatory(Commands.DISPLAY_COLOR);
        hueIdeal1.addMandatory(Commands.SWITCH);

        ControlProfile.ControllableProfileItem hueIdeal2 = hueProfile.createProfileforPriority(2);
        hueIdeal2.addMandatory(Commands.SWITCH);

        hueProfile.addOutput("Color", Commands.DISPLAY_COLOR);

        activityRecognitionProfile.addOutput("activity",Commands.DISPLAY_COLOR);
        activityRecognitionProfile.addOutput("playPause",Commands.PLAYBACK_PLAY_PAUSE);

        ControlProfile.ControllableProfileItem wemominimum = wemoProfile.createProfileforPriority(1);
        wemominimum.addMandatory(Commands.SWITCH);

        wemoProfile.addOutput("Switch",Commands.SWITCH);


//        //Write all profiles to database
//        ServerConnection sc = new ServerConnection();
//        for (Map.Entry<String, ControlProfile> entry : profiles.entrySet()) {
//            sc.writeInDatabase(entry.getValue());
//        }

        myoProfile.addOutput("Switch",Commands.SWITCH);
        myoProfile.addOutput("Forward seek",Commands.PLAYBACK_FORWARD_SEEK);
        myoProfile.addOutput("Backward seek",Commands.PLAYBACK_BACKWARD_SEEK);
        myoProfile.addOutput("Stop",Commands.PLAYBACK_STOP);
        myoProfile.addOutput("IMU",Commands.SENSOR_PYR);

        myoProfile.addOutput("Switch",Commands.SWITCH);
        myoProfile.addOptionalControls(Commands.SENSOR_TOGGLE);
    }


    public ProfileMatcher() {
        //generateProfilesFromJava();
        System.out.println("profileMatcher");
        try {
            connectServer();
        } catch (IOException e) {
            e.printStackTrace();
            generateProfilesFromJava();
        }
    }

    /**
     * Start connection to server.
     * Initialize the list of ControlProfiles
     */
    private void connectServer() throws IOException {
        //Log.i(TAG,"Connect Server");
        ServerConnection sc = new ServerConnection();
        profiles = sc.getAllProfiles();
        if(profiles.size() > 0 ) {
            for (String id : profiles.keySet()) {
                Log.i(TAG, "Profile ID: " + id);
            }
        }else
            generateProfilesFromJava();


    }

    @Override
    public Collection<Edge> matchProfiles(Set<String> controllerPlugins, String clientPluginId) throws InstantiationException, IllegalAccessException {
        //TODO get this data from a webservice
        Map<String, Map<String, String>> inflatedControllerProfile = generateControllerProfile(controllerPlugins);

        ControlProfile clientProfile = profiles.get(clientPluginId);

        List<Edge> edges = new Vector<Edge>();
        if(clientProfile.getInputList().size() == 0) {
            matchOptionals(clientProfile, inflatedControllerProfile, edges, clientPluginId);
            return edges;
        }
        else{
            for (ControlProfile.ControllableProfileItem controllableProfileItem : clientProfile.getInputList()) {
                Log.i(TAG, "trying profile: " + controllableProfileItem.getPriority());
                boolean profileSelected = true;
                for (String controlToMatch : controllableProfileItem.getMandatoryControls()) {
                    Log.i(TAG, "matching: " + controlToMatch.toString());
                    boolean matchFound = false;
                    for (Map.Entry<String, Map<String, String>> providedControls : inflatedControllerProfile.entrySet()) {
                        Log.i(TAG, "checking: " + providedControls.getKey());
                        for (Map.Entry<String, String> providedControl : providedControls.getValue().entrySet()) {
                            Log.i(TAG, "checking command: " + providedControl.getValue().toString());
                            if (providedControl.getValue().equals(controlToMatch)) { //adding null filter
                                edges.add(new Edge(providedControl.getValue(), providedControl.getKey(), providedControls.getKey(), new ArrayList<String>(), clientPluginId, new ArrayList<String>()));
                                matchFound = true;
                                break;
                            }
                        }

                    }
                    if (!matchFound) {
                        Log.i(TAG, "match not found");
                        profileSelected = false;
                        break;
                    }
                }
                if (profileSelected) {
                    matchOptionals(clientProfile, inflatedControllerProfile, edges, clientPluginId);
                    return edges;
                } else
                    edges.clear();
            }
        }
        return null;

    }

    private void matchOptionals(ControlProfile clientProfile, Map<String, Map<String, String>> inflatedControllerProfile, List<Edge> edges, String clientPluginId){
        for (String optionalCommand : clientProfile.getOptionalInputList()) {
            for (Map.Entry<String, Map<String, String>> providedControls : inflatedControllerProfile.entrySet()) {
                Log.i(TAG, "checking: " + providedControls.getKey());
                for (Map.Entry<String, String> providedControl : providedControls.getValue().entrySet()) {
                    Log.i(TAG, "checking command: " + providedControl.getValue().toString());
                    if (providedControl.getValue().equals(optionalCommand)) {
                        Log.i(TAG, "adding optional");
                        edges.add(new Edge(providedControl.getValue(), providedControl.getKey(), providedControls.getKey(), new ArrayList<String>(), clientPluginId, new ArrayList<String>()));
                    }
                }

            }
        }
    }

    public Map<String, ControlProfile> getProfiles() {
        return profiles;
    }
}
