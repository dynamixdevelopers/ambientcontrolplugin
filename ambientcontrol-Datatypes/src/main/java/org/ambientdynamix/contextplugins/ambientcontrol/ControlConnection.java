package org.ambientdynamix.contextplugins.ambientcontrol;

import java.util.UUID;

/**
 * Created by workshop on 4/24/14.
 */
public abstract class ControlConnection {

    protected final String TAG = this.getClass().getSimpleName();
    protected UUID id; // The channel over which we send contextevents

    public ControlConnection() {

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ControlConnection that = (ControlConnection) o;

        return this.id.equals(that.getId());

    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        return result;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public abstract IControlMessage process(IControlMessage command, String sourcePluginId);

}
