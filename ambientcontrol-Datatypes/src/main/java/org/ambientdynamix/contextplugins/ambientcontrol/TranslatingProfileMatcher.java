package org.ambientdynamix.contextplugins.ambientcontrol;

import android.util.Log;

import java.util.*;

/**
 * Created by workshop on 4/7/14.
 */
public class TranslatingProfileMatcher extends ProfileMatcher {

    private final String TAG = this.getClass().getSimpleName();
    private static Map<String, List<Class>> translators = new HashMap<String, List<Class>>();

    static {
        List<Class> pyrTranslators = new Vector<Class>();
        pyrTranslators.add(PyrToMoveTranslator.class);
        pyrTranslators.add(PYRToPlaybackControlTranslator.class);
        pyrTranslators.add(HeadingToColorTranslator.class);
        pyrTranslators.add(PyrToUpDownTranslator.class);
        translators.put(Commands.SENSOR_PYR, pyrTranslators);

//        List<Class> SingleValueTranslators = new Vector<Class>();
//        SingleValueTranslators.add(SingleValueToUpD   ownTranslator.class);
//        translators.put(Commands.SENSOR_SINGLE_VALUE, SingleValueTranslators);


        List<Class> sensorToggleTranslators = new Vector<Class>();
        sensorToggleTranslators.add(ToggleSensorToStartStopTranslator.class);
        sensorToggleTranslators.add(ToggleSensorToPlaybackStopTranslator.class);
        sensorToggleTranslators.add(ToggleCommandSwitchTranslator.class);
        translators.put(Commands.SENSOR_TOGGLE, sensorToggleTranslators);

        List<Class> switchTranslators = new Vector<Class>();
        switchTranslators.add(SwitchToPlaybackPlayPauseTranslator.class);
        switchTranslators.add(SwitchToStartStopTranslator.class);
        translators.put(Commands.SWITCH, switchTranslators);

        List<Class> displayTranslators = new Vector<Class>();
        displayTranslators.add(DisplayToSwitchTranslator.class);
        translators.put(Commands.DISPLAY_COLOR, displayTranslators);


    }

    @Override
    public Collection<Edge> matchProfiles(Set<String> controllerPlugins, String clientPluginId) throws InstantiationException, IllegalAccessException {
        ControlProfile clientProfile = profiles.get(clientPluginId);
        Map<String, Map<String, String>> inflatedControllerProfile = generateControllerProfile(controllerPlugins);
        Map<String, Edge> edges = new HashMap<String, Edge>();
        if (clientProfile == null)
            return null;
        if (clientProfile.getInputList().size() == 0)
            findMatches(clientPluginId, clientProfile.getOptionalInputList(), inflatedControllerProfile, edges);
        else {
            for (ControlProfile.ControllableProfileItem controllableProfileItem : clientProfile.getInputList()) {
                Log.i(TAG, "trying profile: " + controllableProfileItem.getPriority());
                boolean profileSelected = findMatches(clientPluginId, controllableProfileItem.getMandatoryControls(), inflatedControllerProfile, edges);
                if (profileSelected) {
                    findMatches(clientPluginId, clientProfile.getOptionalInputList(), inflatedControllerProfile, edges);
                    return edges.values();
                } else
                    edges.clear();

            }
        }
        return null;

    }

    private boolean findMatches(String clientPluginId, Set<String> controlsToMatch, Map<String, Map<String, String>> inflatedControllerProfile, Map<String, Edge> edges) throws IllegalAccessException, InstantiationException {
        boolean matchedAll = true;
        Log.i(TAG, "trying to match without translation: ");
        for (String controlToMatch : controlsToMatch) {
            boolean matchFound = false;
            Log.i(TAG, "matching: " + controlToMatch);
            if (edges.containsKey(controlToMatch)) {
                Log.i(TAG, "already have edge from this control: " + controlToMatch + ", skipping");
                continue;
            }
            for (Map.Entry<String, Map<String, String>> providedControls : inflatedControllerProfile.entrySet()) {
                Log.i(TAG, "checking: " + providedControls.getKey());
                for (Map.Entry<String, String> providedControl : providedControls.getValue().entrySet()) {
                    Log.i(TAG, "checking command: " + providedControl.getValue());
                    if (providedControl.getValue().equals(controlToMatch)) { //adding null filter
                        edges.put(providedControl.getValue(), new Edge(providedControl.getValue(), providedControl.getKey(), providedControls.getKey(), new ArrayList<String>(), clientPluginId, new ArrayList<String>()));
                        matchFound = true;
                        Log.i(TAG, "direct match found: " + providedControl.getValue() + " to " + controlToMatch + " from " + providedControls.getKey());
                        break;
                    }
                }
            }
            if (!matchFound) {
                Log.i(TAG, "match not found looking for translation  ");
                for (Map.Entry<String, Map<String, String>> providedControls : inflatedControllerProfile.entrySet()) {
                    Log.i(TAG, "checking: " + providedControls.getKey());
                    for (Map.Entry<String, String> providedControl : providedControls.getValue().entrySet()) {
                        Log.i(TAG, "checking for translation from: " + providedControl.getKey());
                        List<Class> possibleTranslators = translators.get(providedControl.getValue());
                        if (possibleTranslators == null)
                            continue;
                        for (Class possibleTranslatorClass : possibleTranslators) {
                            Translator possibleTranslator = (Translator) possibleTranslatorClass.newInstance();
                            if (possibleTranslator.getTranslateFrom().equals(providedControl.getValue()) &&
                                    possibleTranslator.possibleTranslations().contains(controlToMatch)) {
                                if (edges.containsKey(providedControl.getValue())) {
                                    Edge edge = edges.get(providedControl.getValue());
                                    if (edge.getTranslator() != null && edge.getTranslator().getClass().equals(possibleTranslator.getClass())) {
                                        edge.getTranslator().getTranslateTo().add(controlToMatch);
                                        Log.i(TAG, "adding translation to existing translator: " + providedControl.getValue() + " to " + controlToMatch + " from " + providedControls.getKey());
                                    } else {
//                                        Translator translator = possibleTranslator.getClass().newInstance();
//                                        translator.getTranslateTo().add(controlToMatch);
//                                        edges.put(providedControl.getValue(),new Edge(providedControl.getValue(), providedControl.getKey(), providedControls.getKey(), new ArrayList<String>(), clientPluginId, new ArrayList<String>(), translator));
//                                        Log.i(TAG,"added new translation (got direct edge already): " + providedControl.getValue() + " to " + controlToMatch+" from " + providedControls.getKey());
                                        Log.i(TAG, "already have edge from this control: " + controlToMatch + ", skipping");

                                    }
                                } else {
                                    Translator translator = possibleTranslator.getClass().newInstance();
                                    translator.getTranslateTo().add(controlToMatch);
                                    edges.put(providedControl.getValue(), new Edge(providedControl.getValue(), providedControl.getKey(), providedControls.getKey(), new ArrayList<String>(), clientPluginId, new ArrayList<String>(), translator));
                                    Log.i(TAG, "added new translation: " + providedControl.getValue() + " to " + controlToMatch + " from " + providedControls.getKey());
                                }
                                matchFound = true;
                                Log.i(TAG, "found translation, tranlsating " + possibleTranslator.getTranslateFrom() + " to " + possibleTranslator.possibleTranslations().toString());
                                break;
                            }
                        }
                        if (matchFound)
                            break;
                    }

                }
            }
            if (!matchFound)
                matchedAll = false;
        }
        return matchedAll;

    }

    public <T extends Translator> void registerTranslator(Class<T> translatorClass) {
        try {
            Translator translator = translatorClass.newInstance();
            if (translators.containsKey(translator.getTranslateFrom()))
                translators.get(translator.getTranslateFrom()).add(translatorClass);
            else {
                translators.put(translator.getTranslateFrom(), new Vector<Class>());
                translators.get(translator.getTranslateFrom()).add(translatorClass);
            }
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public static Map<String, List<Class>> getTranslators() {
        return translators;
    }

    public Map<String, Translator> matchAppConnection(Collection<String> commands, String serverPlugin) throws IllegalAccessException, InstantiationException {
        HashMap<String, Translator> result = new HashMap<String, Translator>();
        ControlProfile controlProfile = profiles.get(serverPlugin);
        Collection<String> availableControls = controlProfile.getOutputList().values();
        for (String command : commands) {
            boolean matchedThis = false;
            if (availableControls.contains(command))
                continue;
            else{
                Log.i(TAG, "Looking for translation to: " + command);
                for (String providedControl : availableControls) {
                    Log.i(TAG, "checking for translation from: " + providedControl);
                    List<Class> possibleTranslators = translators.get(providedControl);
                    if (possibleTranslators == null)
                        continue;
                    for (Class possibleTranslatorClass : possibleTranslators) {
                        Translator possibleTranslator = (Translator) possibleTranslatorClass.newInstance();
                        if (possibleTranslator.getTranslateFrom().equals(providedControl) &&
                                possibleTranslator.possibleTranslations().contains(command)) {
                            Translator translator = result.get(providedControl);
                            if (translator != null && translator.possibleTranslations().contains(command)) {
                                translator.getTranslateTo().add(command);
                                Log.i(TAG, "adding translation to existing translator: " + providedControl + " to " + command + " from " + serverPlugin);
                            }else {
                                translator = possibleTranslator.getClass().newInstance();
                                translator.getTranslateTo().add(command);
                                result.put(providedControl, translator);
                            }
                            Log.i(TAG, "added new translation: " + providedControl + " to " + command + " from " + serverPlugin);
                            matchedThis = true;
                            Log.i(TAG, "found translation, translating " + possibleTranslator.getTranslateFrom() + " to " + possibleTranslator.possibleTranslations().toString());
                            break;
                        }
                    }
                    if(matchedThis)
                        break;
                }
            }

            if (!matchedThis) {
                return null;
            }
        }
        return result;
    }
}

