package org.ambientdynamix.contextplugins.ambientcontrol;

import android.util.Log;

/**
 * Created by workshop on 6/3/14.
 */
public class SwitchToStartStopTranslator extends Translator {

    private static final String[] generatedCommands = new String[]{Commands.MOVEMENT_START_STOP};

    @Override
    public String getTranslateFrom() {
        return Commands.SWITCH;
    }

    @Override
    public String[] getGeneratedCommands() {
        return generatedCommands;
    }


    @Override
    public IControlMessage process(IControlMessage info) {
        if (info instanceof ToggleCommand && info.getCommand().equals(Commands.SWITCH)) {
            Log.i("SwitchToStartStopTranslator", "translating toggle to Start stop");
            return new ToggleCommand(Commands.MOVEMENT_START_STOP,info.getName());
        }else {
            throw new RuntimeException("cannot translate this contextType " + info.getContextType());
        }
    }
}
