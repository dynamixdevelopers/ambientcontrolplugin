package org.ambientdynamix.contextplugins.ambientcontrol;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by workshop on 3/31/14.
 */
public interface IProfileMatcher {
    public java.util.Collection<Edge> matchProfiles(Set<String> controllerPlugins, String clientPluginId) throws InstantiationException, IllegalAccessException;
}
