package org.ambientdynamix.contextplugins.ambientcontrol;

import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.ambientdynamix.api.application.Callback;
import org.ambientdynamix.api.application.ContextResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by workshop on 02-Sep-14.
 */
public class JsonTrigger extends Trigger {
    private final String TAG = JsonTrigger.class.getSimpleName();
    private String jsonTrigger;
    private Map<String, String> triggerValues;
    private Gson gson = new Gson();
    private Type stringStringMap = new TypeToken<Map<String, String>>() {}.getType();

    public JsonTrigger(String contextType, String pluginId, String jsonTrigger, String controlConfig, ControlConnectionManager cCM) throws JSONException {
        super(contextType, pluginId, cCM, controlConfig);
        this.jsonTrigger = jsonTrigger;
        triggerValues = readJsonTrigger(jsonTrigger).get(0);
    }

    public JsonTrigger(String contextSources, String jsonTrigger, String controlConfig, ControlConnectionManager cCM) throws JSONException {
        super(null, cCM, controlConfig);
        this.contextSources = gson.fromJson(contextSources, stringStringMap);
        this.jsonTrigger = jsonTrigger;
        triggerValues = readJsonTrigger(jsonTrigger).get(0);
    }

    private List<Map<String, String>> readJsonTrigger(String jsonTrigger) throws JSONException {
        List<Map<String, String>> out = new Vector<Map<String, String>>();
        if (!jsonTrigger.startsWith("[")) {
            Map<String, String> map = gson.fromJson(jsonTrigger, stringStringMap);
            if (map != null)
                out.add(map);
            return out;
        } else {
            JSONArray jsonArray = new JSONArray(jsonTrigger);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                Map<String, String> map = gson.fromJson(jsonObject.toString(), stringStringMap);
                if (map != null)
                    out.add(map);
            }
            return out;
        }
    }

    @Override
    public boolean checkTrigger(ConcurrentHashMap<String, ContextResult> contextState) {
        List<Map<String, String>> contextStateData = new Vector<Map<String, String>>();
        for (String contextType : getContextSources().values()) {
            try {
                if (!contextState.containsKey(contextType))
                    return false;    //one required contextType info is not in the current state quick fail check
                String stringRepresentation = contextState.get(contextType).getStringRepresentation("application/json");
                contextStateData.addAll(readJsonTrigger(stringRepresentation));
            } catch (JSONException e) {
                Log.e(TAG, "Error during Control graph creation from Trigger: " + e);
                return false;
            }
        }
        Log.i(TAG,contextStateData.toString());
        for (String triggerKey : triggerValues.keySet()) {
            boolean triggeredHere = false;
            for (Map<String, String> map : contextStateData) {
                if (map.containsKey(triggerKey) && map.get(triggerKey).equals(triggerValues.get(triggerKey))) {
                    triggeredHere = true;
                }
            }
            if (!triggeredHere)
                return false;

        }
        execute();
        return true;
    }

}

