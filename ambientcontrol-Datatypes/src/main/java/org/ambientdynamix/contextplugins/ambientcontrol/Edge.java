package org.ambientdynamix.contextplugins.ambientcontrol;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by workshop on 09-Jul-14.
 */
public class Edge {
    private String commandType;
    private String name;
    private String sourcePlugin;
    private ArrayList<String> sourceDeviceIds;
    private String targetPlugin;
    private ArrayList<String> targetDeviceIds;
    private Translator translator;
    private final String TAG = this.getClass().getSimpleName();


    public Edge(String commandType, String name, String sourcePlugin, ArrayList<String> sourceDeviceIds, String targetPlugin, ArrayList<String> targetDeviceIds, Translator translator) {
        this.commandType = commandType;
        this.name = name;
        this.sourcePlugin = sourcePlugin;
        this.sourceDeviceIds = sourceDeviceIds == null ? new ArrayList<String>() : sourceDeviceIds;
        this.targetPlugin = targetPlugin;
        this.targetDeviceIds = targetDeviceIds == null ? new ArrayList<String>() : targetDeviceIds;
        this.translator = translator;
    }

    public Edge(String commandType, String name, String sourcePlugin, ArrayList<String> sourceDeviceIds, String targetPlugin,  ArrayList<String> targetDeviceIds) {
        this.commandType = commandType;
        this.name = name;
        this.sourcePlugin = sourcePlugin;
        this.sourceDeviceIds = sourceDeviceIds == null ? new ArrayList<String>() : sourceDeviceIds;
        this.targetPlugin = targetPlugin;
        this.targetDeviceIds = targetDeviceIds == null ? new ArrayList<String>() : targetDeviceIds;
        this.translator = null;
    }

    public String getCommandType() {
        return commandType;
    }

    public String getName() {
        return name;
    }

    public String getSourcePlugin() {
        return sourcePlugin;
    }

    public Translator getTranslator() {
        return translator;
    }

    public IControlMessage process(IControlMessage message){
        if(message.getName().equals(name) && message.getCommand().equals(commandType) && translator != null) {
//            Log.i(TAG, "translating");
            return translator.translate(message);
        }
        else
//            Log.i(TAG, "translator not applicable");
            return message;

    }

    public String getTargetPlugin() {
        return targetPlugin;
    }

    @Override
    public String toString() {
        return "Edge{" +
                "commandType=" + commandType +
                ", name='" + name + '\'' +
                ", sourcePlugin='" + sourcePlugin + '\'' +
                ", targetPlugin='" + targetPlugin + '\'' +
                ", translator=" + ((translator == null)? "null" : translator.getClass().getSimpleName()) +
                '}';
    }

    public ArrayList<String> getSourceDeviceIds() {
        return sourceDeviceIds;
    }

    public ArrayList<String> getTargetDeviceIds() {
        return targetDeviceIds;
    }
}
