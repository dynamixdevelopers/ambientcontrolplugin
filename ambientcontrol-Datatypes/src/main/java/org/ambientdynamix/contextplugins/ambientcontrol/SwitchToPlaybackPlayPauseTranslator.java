package org.ambientdynamix.contextplugins.ambientcontrol;

/**
 * Created by maxpagel on 03.11.14.
 */
public class SwitchToPlaybackPlayPauseTranslator extends Translator {
    private static final String[] generatedCommands = new String[]{Commands.PLAYBACK_PLAY_PAUSE};
    private final String TAG = this.getClass().getSimpleName();

    @Override
    public String getTranslateFrom() {
        return Commands.SWITCH;
    }

    @Override
    public IControlMessage process(IControlMessage info) {
//        Log.i(TAG, "process Message!! " + info);
        if (info instanceof ToggleCommand && info.getCommand().equals(Commands.SWITCH)) {
            ToggleCommand command = (ToggleCommand) info;

            if(command.isDown())
                return new ToggleCommand(Commands.PLAYBACK_PLAY_PAUSE,true,command.getName());
            else
                return new ToggleCommand(Commands.PLAYBACK_PLAY_PAUSE,false,command.getName());

        } else
            throw new RuntimeException("cannot translate this contextType " + info.getContextType());
    }

    @Override
    public String[] getGeneratedCommands() {
        return generatedCommands;
    }
}
