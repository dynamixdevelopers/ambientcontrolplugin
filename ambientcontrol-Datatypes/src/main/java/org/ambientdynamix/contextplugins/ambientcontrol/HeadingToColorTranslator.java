package org.ambientdynamix.contextplugins.ambientcontrol;

/**
 * Created by workshop on 25-Aug-14.
 */
public class HeadingToColorTranslator extends Translator {

    private static final String[] generatedCommands = new String[]{Commands.DISPLAY_COLOR};
    private final String TAG = this.getClass().getSimpleName();

    @Override
    public String getTranslateFrom() {
        return Commands.SENSOR_PYR;
    }

    @Override
    public IControlMessage process(IControlMessage info) {
        if (info instanceof PYRSensor) {
            PYRSensor sensor = (PYRSensor) info;
            int[] rgb = hsvToRgb(180 + sensor.getYaw(), Math.max(0,100-Math.abs(sensor.getRoll())), Math.max(0,100-Math.abs(sensor.getRoll())));
//            Log.i(TAG, "YAW: " + sensor.getYaw());
            return new DisplayCommand(Commands.DISPLAY_COLOR,rgb[0],rgb[1],rgb[2],0,sensor.getName());
        } else
            throw new RuntimeException("cannot translate this contextType " + info.getContextType());
    }

    @Override
    public String[] getGeneratedCommands() {
        return generatedCommands;
    }

    /**
     * HSV to RGB color conversion
     *
     * H runs from 0 to 360 degrees
     * S and V run from 0 to 100
     *
     * Ported from the excellent java algorithm by Eugene Vishnevsky at:
     * http://www.cs.rit.edu/~ncs/color/t_convert.html
     */
    public int [] hsvToRgb(double h, double s, double v) {
        double r, g, b;
        int i;
        double f, p, q, t;

        // Make sure our arguments stay in-range
        h = Math.max(0, Math.min(360, h));
        s = Math.max(0, Math.min(100, s));
        v = Math.max(0, Math.min(100, v));

        // We accept saturation and value arguments from 0 to 100 because that's
        // how Photoshop represents those values. Internally, however, the
        // saturation and value are calculated from a range of 0 to 1. We make
        // That conversion here.
        s /= 100.0;
        v /= 100.0;

        if(s == 0) {
            // Achromatic (grey)
            r = g = b = v;
            return new int[]{(int)Math.round(r * 255), (int)Math.round(g * 255),(int) Math.round(b * 255)};
        }

        h /= 60.0; // sector 0 to 5
        i = (int)h;
        f = h - i; // factorial part of h
        p = v * (1 - s);
        q = v * (1 - s * f);
        t = v * (1 - s * (1 - f));

        switch(i) {
            case 0:
                r = v;
                g = t;
                b = p;
                break;

            case 1:
                r = q;
                g = v;
                b = p;
                break;

            case 2:
                r = p;
                g = v;
                b = t;
                break;

            case 3:
                r = p;
                g = q;
                b = v;
                break;

            case 4:
                r = t;
                g = p;
                b = v;
                break;

            default: // case 5:
                r = v;
                g = p;
                b = q;
        }

        return new int[]{(int)Math.round(r * 255),(int) Math.round(g * 255), (int)Math.round(b * 255)};
    }
}
