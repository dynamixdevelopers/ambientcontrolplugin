package org.ambientdynamix.contextplugins.ambientcontrol;


import java.util.*;

/**
 * Created by workshop on 4/7/14.
 */
public abstract class Translator {

    private Set<String> translateTo = new HashSet<String>();
    private Map<String, String> config = new HashMap<String, String>();


    public abstract String getTranslateFrom();

    public Set<String> possibleTranslations() {
        HashSet<String> commands = new HashSet<String>();
        commands.addAll(Arrays.asList(getGeneratedCommands()));
        return commands;
    }

    public IControlMessage translate(IControlMessage info){
        if(getTranslateFrom().equals(info.getCommand())) {
            IControlMessage message = process(info);
            message.setTargetDeviceIds(info.getTargetDeviceIds());
            message.setSourceDeviceID(info.getSourceDeviceID());
            return message;
        }
        else
            return info;
    };

    protected abstract <T extends IControlMessage> T process(IControlMessage info);

    public void setConfig(Map<String,String> config){

        this.config = config;
    }

    public Map<String,String> getConfig(){
        return config;
    }

    public void setTranslateTo(Set<String> translateTo){
        this.translateTo = translateTo;
    }

    public Set<String> getTranslateTo(){
        return translateTo;
    }

    public abstract String[] getGeneratedCommands();


}
