package org.ambientdynamix.contextplugins.ambientcontrol;

import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import org.ambientdynamix.api.application.Callback;
import org.ambientdynamix.api.application.ContextResult;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by workshop on 02-Sep-14.
 */
public abstract class Trigger {
    private final String TAG = this.getClass().getSimpleName();
    protected Map<String,String> contextSources;
    protected ControlConnectionManager cCM;
    protected String controlConfig;
    protected int hashId;
    private boolean executing;

    protected Trigger(String contextType, String pluginId, ControlConnectionManager cCM, String controlConfig) {
        this.contextSources = new HashMap<String, String>();
        contextSources.put(pluginId,contextType);
        this.cCM = cCM;
        hashId = controlConfig.hashCode();
        this.controlConfig = controlConfig;
    }

    protected Trigger(Map<String, String> contextSources, ControlConnectionManager cCM, String controlConfig) {
        this.contextSources = contextSources;
        this.cCM = cCM;
        hashId = controlConfig.hashCode();
        this.controlConfig = controlConfig;
    }

    public abstract boolean checkTrigger(ConcurrentHashMap<String, ContextResult> contextState );

    public boolean isActive() {
        if(executing)
            return false;
        for (ControlConnection controlConnection : cCM.getControllChannels()) {
            if (controlConnection instanceof GraphConnection && ((GraphConnection)controlConnection).getControlGraphConfig().getHashID() == hashId)
                return false;
        }
        return true;
    }

    public Map<String,String> getContextSources() {
        return contextSources;
    }

    public void execute() {
        if(executing)
            return;
        executing = true;
        Bundle bundle = new Bundle();
        bundle.putString(ControlConnectionManager.CONNECTION_CONFIG, controlConfig);
        bundle.putString(ControlConnectionManager.CONNECTION_CONTROL, ControlConnectionManager.CONFIG_INIT);
        try {
            cCM.initControlGraph(bundle, new Callback(){
                @Override
                public void onSuccess() throws RemoteException {
                    Log.i(TAG, "Triggered succesfull, sending notification");
                    cCM.getRuntime().sendBroadcastContextEvent(new ToggleCommand(Commands.TRIGGER,"" + hashId),10000);
                    executing = false;
                }

                @Override
                public void onFailure(String message, int errorCode) throws RemoteException {
                    executing = false;
                }
            });
        } catch (RemoteException e) {
            Log.e(TAG, "Error during Control graph creation from Trigger: " + e);
        }
    }
}