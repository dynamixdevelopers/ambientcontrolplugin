package org.ambientdynamix.contextplugins.ambientcontrol;

/**
 * Created by workshop on 4/7/14.
 */
public class PyrToMoveTranslator extends Translator {
    private static final String[] generatedCommands = new String[]{Commands.MOVEMENT_BACKWARD, Commands.MOVEMENT_BACKWARD_LEFT, Commands.MOVEMENT_BACKWARD_RIGHT,
            Commands.MOVEMENT_FORWARD, Commands.MOVEMENT_FORWARD_RIGHT, Commands.MOVEMENT_FORWARD_LEFT, Commands.MOVEMENT_LEFT, Commands.MOVEMENT_RIGHT,
            Commands.MOVEMENT_NEUTRAL, Commands.MOVEMENT_SPIN_CCW, Commands.MOVEMENT_SPIN_CW};
    private static final int ROLL_THRESHOLD = 10;
    private static final int PITCH_THRESHOLD = 10;

    @Override
    public String getTranslateFrom() {
        return Commands.SENSOR_PYR;
    }

    @Override
    public String[] getGeneratedCommands() {
        return generatedCommands;
    }

    @Override
    public IControlMessage process(IControlMessage info) {
        if (info instanceof PYRSensor) {

            PYRSensor ipyrSensor = (PYRSensor) info;
            String command = Commands.MOVEMENT_NEUTRAL;
            float speed = 0;
            if (Math.abs(ipyrSensor.getRoll()) > ROLL_THRESHOLD || Math.abs(ipyrSensor.getPitch()) > PITCH_THRESHOLD) {
                float pitchNorm = (float) ipyrSensor.getPitch() / (float) Math.max(Math.abs(ipyrSensor.getPitch()), Math.abs(ipyrSensor.getRoll()));
                float rollNorm = (float) ipyrSensor.getRoll() / (float) Math.max(Math.abs(ipyrSensor.getPitch()), Math.abs(ipyrSensor.getRoll()));
                final float heading;
                if (pitchNorm > 0) { //forward
                    if (rollNorm < 0) { //left 270 - 359
                        heading = (315f + pitchNorm * 45f + rollNorm * 45f);
                    } else { // right 0 - 90
                        heading = (45f - pitchNorm * 45f + rollNorm * 45f);
                    }
                } else { //backward
                    if (rollNorm < 0) { //left 180 - 270
                        heading = (225f + pitchNorm * 45f - rollNorm * 45f);
                    } else { // right 90 - 180
                        heading = (135 - pitchNorm * 45f - rollNorm * 45f);
                    }
                }
                speed = (Math.abs(ipyrSensor.getRoll()) + Math.abs(ipyrSensor.getPitch())) / 360f;
//                Log.i(TAG,"driving: " + heading + " at speed: " + speed);
                if(heading >= 340 || heading < 20)
                    command = Commands.MOVEMENT_FORWARD;
                else if(heading >= 20 && heading < 80)
                    command = Commands.MOVEMENT_FORWARD_RIGHT;
                else if(heading >= 80 && heading < 100)
                    command = Commands.MOVEMENT_RIGHT;
                else if(heading >= 100 && heading < 160)
                    command = Commands.MOVEMENT_BACKWARD_RIGHT;
                else if(heading >= 160 && heading < 200)
                    command = Commands.MOVEMENT_BACKWARD;
                else if(heading >= 200 && heading < 260)
                    command = Commands.MOVEMENT_BACKWARD_LEFT;
                else if(heading > 260 && heading < 280)
                    command = Commands.MOVEMENT_LEFT;
                else if(heading >= 280 && heading < 340)
                    command = Commands.MOVEMENT_FORWARD_LEFT;
            }
            return new ToggleCommand(command,(int) (100 * speed),info.getName());
        } else {
            throw new RuntimeException("cannot translate this contextType " + info.getContextType());
        }

    }
}
