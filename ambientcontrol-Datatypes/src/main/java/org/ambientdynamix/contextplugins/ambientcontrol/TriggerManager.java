package org.ambientdynamix.contextplugins.ambientcontrol;

import android.os.RemoteException;
import android.util.Log;
import org.ambientdynamix.api.application.*;
import org.ambientdynamix.api.contextplugin.ContextPluginRuntime;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by workshop on 29-Aug-14.
 */
public class TriggerManager {
    private static final String TIMER_NAME = "Trigger_Timer";
    private static final long STATE_TIMEOUT = 60000;

    private final String TAG = this.getClass().getSimpleName();
    private ContextHandler triggerHandler;
    private ContextPluginRuntime runtime;
    private DynamixFacade facade;
    private Set<Trigger> triggers;
    private ControlConnectionManager cCM;
    private boolean timerStarted;
    private Timer timer;
    private TriggerChecker triggerChecker;
    private StateChecker stateChecker;
    private ConcurrentHashMap<String, ContextResult> stateMonitor;


    public TriggerManager(ContextPluginRuntime runtime, ControlConnectionManager cCM) {
        this.runtime = runtime;
        this.cCM = cCM;
        this.facade = runtime.getPluginFacade().getDynamixFacadeWrapper(runtime.getSessionId());
        triggers = new HashSet<Trigger>();
        timer = new Timer(TIMER_NAME, true);
        stateMonitor = new ConcurrentHashMap<String, ContextResult>(8, 0.9f, 1);
    }

    private void connect(final Callback callback) {
        if (triggerHandler == null) {
            try {
                final ContextHandlerCallback handlerCallback = new ContextHandlerCallback() {
                    @Override
                    public void onSuccess(ContextHandler handler) throws RemoteException {
                        triggerHandler = handler;
                        callback.onSuccess();
                    }

                    @Override
                    public void onFailure(String message, int errorCode) throws RemoteException {
                        callback.onFailure(message, errorCode);
                    }
                };
                if (facade.isSessionOpen()) {
                    facade.createContextHandler(handlerCallback);
                } else {
                    facade.openSession(new SessionCallback() {
                        @Override
                        public void onSuccess(DynamixFacade facade) throws RemoteException {
                            facade.createContextHandler(handlerCallback);
                        }

                        @Override
                        public void onFailure(String message, int errorCode) throws RemoteException {
                            Log.w(TAG, "Error connecting TriggerManager to Dynamix: " + message + " " + errorCode);
                        }
                    });
                }
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }


    public void start() {
        Log.i(TAG, "Starting TriggerManager");
        if (triggerChecker == null) {
            triggerChecker = new TriggerChecker();
            stateChecker = new StateChecker();
            timer.scheduleAtFixedRate(triggerChecker, 0, 10000);
            timer.scheduleAtFixedRate(stateChecker, 0, 7000);
        }
    }

    public void stop() {
        Log.i(TAG, "Stopping TriggerManager");
        if (triggerChecker != null) {
            triggerChecker.cancel();
            stateChecker.cancel();
            timer.purge();
            triggerChecker = null;
            stateChecker = null;
        }
        if(triggerHandler != null){
            try {
                facade.removeContextHandler(triggerHandler.getContextHandler());
            } catch (RemoteException e) {
                Log.d(TAG, "error during stop: " + e.toString());
            }
        }
        triggers.clear();
    }

    private IContextListener triggerListener = new ContextListener() {
        @Override
        public void onContextResult(ContextResult result) throws RemoteException {
            stateMonitor.put(result.getContextType(), result);
            checkTriggers(result.getContextType());
        }
    };

    public void registerTrigger(final Trigger trigger) {
        Log.i(TAG, "received register Trigger call: " + trigger.getContextSources());
        if (triggerHandler != null) {
            try {
                registerTriggerContexts(trigger);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        } else {
            connect(new Callback() {
                @Override
                public void onSuccess() throws RemoteException {
                    Log.i(TAG, "connected succesfully: " + trigger.getContextSources());
                    registerTriggerContexts(trigger);
                }

                @Override
                public void onFailure(String message, int errorCode) throws RemoteException {
                    Log.w(TAG, "Error connecting TriggerManager to Dynamix: " + message + " " + errorCode);
                }
            });
        }
    }

    private void registerTriggerContexts(final Trigger trigger) throws RemoteException {
        for (final Map.Entry<String, String> contextSource : trigger.getContextSources().entrySet()) {
            triggers.add(trigger);
            Log.i(TAG, "registering Trigger context Support: " + trigger.getContextSources());
            triggerHandler.addContextSupport(contextSource.getKey(), contextSource.getValue(), triggerListener, new ContextSupportCallback() {
                @Override
                public void onSuccess(ContextSupportInfo supportInfo) throws RemoteException {
                    Log.i(TAG, "Successfully registered Trigger for: " + trigger.getContextSources());
                    start();
                }

                @Override
                public void onFailure(String message, int errorCode) throws RemoteException {
                    Log.w(TAG, "Error adding context support for TriggerManager removing trigger: " + message + " " + errorCode);
                    triggers.remove(trigger);
                }
            });
        }
    }

    public void removeTrigger(Trigger trigger) throws RemoteException {
        triggers.remove(trigger);
        if (triggers.isEmpty()) {
            triggers.remove(trigger);
            for (String contextType : trigger.getContextSources().values()) {
                boolean stillUsed = false;
                for (Trigger activeTrigger : triggers) {
                    if(activeTrigger.getContextSources().containsValue(contextType))
                        stillUsed = true;
                }
                if (!stillUsed) {
                    triggerHandler.removeContextSupport(contextType);
                }
            }
        }
        if (triggers.isEmpty()) {
            stop();
        }
    }

    private class TriggerChecker extends TimerTask {
        @Override
        public void run() {
            if (triggerHandler != null) {
                Log.d(TAG, "checking Triggers");
                for (final Trigger trigger : triggers) {
                    if (!trigger.isActive())
                        continue;
                    trigger.checkTrigger(stateMonitor);
                }
            }
        }
    }

    private void checkTriggers(String contextType) {
        if (triggerHandler != null) {
            Log.d(TAG, "checking Triggers");
            for (final Trigger trigger : triggers) {
                if (!trigger.isActive() || !trigger.getContextSources().containsValue(contextType))
                    continue;
                trigger.checkTrigger(stateMonitor);
            }
        }
    }

    private class StateChecker extends TimerTask {
        @Override
        public void run() {
            if (triggerHandler != null) {
                Log.d(TAG, "updating Triggers");
                for (String s : stateMonitor.keySet()) {
                    ContextResult contextState = stateMonitor.get(s);
                    if (System.currentTimeMillis() > contextState.getExpireTime().getTime())
                        stateMonitor.remove(s);
                }
                for (final Trigger trigger : triggers) {
                    if (!trigger.isActive())
                        continue;
                    for (final Map.Entry<String, String> contextSource : trigger.getContextSources().entrySet()) {
                        try {
                            triggerHandler.contextRequest(contextSource.getKey(), contextSource.getValue(), new ContextRequestCallback() {
                                @Override
                                public void onSuccess(ContextResult result) throws RemoteException {
                                    Log.d(TAG, "updating Triggers result: " + result.getStringRepresentation("application/json"));
                                    stateMonitor.put(result.getContextType(), result);
                                }

                                @Override
                                public void onFailure(String message, int errorCode) throws RemoteException {
                                    Log.w(TAG, "failed during contextStateUpdate: " + contextSource.getKey() + " " + contextSource.getValue() + " : " + message);
                                    stateMonitor.remove(contextSource.getValue());
                                }
                            });
                        } catch (RemoteException e) {
                            Log.w(TAG, "Error during Trigger check: " + contextSource.getKey() + " " + contextSource.getValue() + " : " + e);
                            stateMonitor.remove(contextSource.getValue());
                        }
                    }
                }

            }
        }
    }
}
