package org.ambientdynamix.contextplugins.ambientcontrol;

import android.util.Log;
import org.jibx.runtime.*;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.List;
import java.util.UUID;
import java.util.Vector;

/**
 * Created by maxpagel on 20.11.14.
 */
public class ControlScenarioConfig {

    private final String TAG = this.getClass().getSimpleName();
    private List<ControlGraphConfig> controlGraphs;
    private UUID id;
    private String name;

    public ControlScenarioConfig(String xml) throws JiBXException {
        this();
        Log.i(TAG, "initializing control scenario: " + xml);
        IBindingFactory bfact = BindingDirectory.getFactory(ControlScenario.class);
        IUnmarshallingContext uctx = bfact.createUnmarshallingContext();
        StringReader stringReader = new StringReader(xml);
        Object obj = uctx.unmarshalDocument(stringReader, null);
        ControlScenario scenario = (ControlScenario) obj;
        IMarshallingContext mctx = bfact.createMarshallingContext();
        mctx.setIndent(4);
        StringWriter stringWriter = new StringWriter();
        mctx.marshalDocument(scenario, "UTF-8", null, stringWriter);
        Log.i(TAG, "scenario read: " + stringWriter.toString());
        for (ControlConfig controlConfig : scenario.getControlGraphList()) {
            stringWriter = new StringWriter();
            mctx.marshalDocument(controlConfig, "UTF-8", null, stringWriter);
            try {
                controlGraphs.add(new ControlGraphConfig(stringWriter.toString()));
            } catch (Exception e) {
                e.printStackTrace();
                Log.e(TAG,"could not read control graph: " + stringWriter.toString());
            }
        }
        this.name = scenario.getName();
    }

    public ControlScenarioConfig(){
        controlGraphs = new Vector<ControlGraphConfig>();
    }

    public void addGraph(ControlGraphConfig graph){
        controlGraphs.add(graph);
    }

    public List<ControlGraphConfig> getControlGraphs() {
        return controlGraphs;
    }

    public String marshal() throws JiBXException {
        ControlScenario scenario = new ControlScenario();
        List<ControlConfig> graphs = new Vector<ControlConfig>();
        IBindingFactory bfact = BindingDirectory.getFactory(ControlConfig.class);
        IUnmarshallingContext uctx = bfact.createUnmarshallingContext();
        for (ControlGraphConfig controlGraph : controlGraphs) {
            StringReader stringReader = new StringReader(controlGraph.marshal());
            Object obj = uctx.unmarshalDocument(stringReader, null);
            ControlConfig config = (ControlConfig) obj;
            graphs.add(config);
        }
        scenario.setControlGraphList(graphs);
        IBindingFactory bfactScenario = BindingDirectory.getFactory(ControlScenario.class);
        IMarshallingContext mctx = bfact.createMarshallingContext();
        mctx.setIndent(4);
        StringWriter stringWriter = new StringWriter();
        mctx.marshalDocument(scenario, "UTF-8", null, stringWriter);
        return stringWriter.toString();
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

