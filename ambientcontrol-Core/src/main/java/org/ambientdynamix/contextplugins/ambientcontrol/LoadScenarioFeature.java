package org.ambientdynamix.contextplugins.ambientcontrol;

import android.content.Intent;
import android.view.View;
import org.ambientdynamix.api.contextplugin.ActivityController;
import org.ambientdynamix.api.contextplugin.IPluginView;

/**
 * Created by maxpagel on 21.11.14.
 */
public class LoadScenarioFeature implements IPluginView{

    @Override
    public void setActivityController(ActivityController controller) {

    }

    @Override
    public View getView() {
        return null;
    }

    @Override
    public void destroyView() {

    }

    @Override
    public void handleIntent(Intent newIntent) {

    }

    @Override
    public int getPreferredOrientation() {
        return 0;
    }
}
