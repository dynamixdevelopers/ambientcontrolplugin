/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.contextplugins.ambientcontrol;

import android.content.Context;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import org.ambientdynamix.HTTPRequestMaker;
import org.ambientdynamix.api.application.*;
import org.ambientdynamix.api.contextplugin.ContextPluginRuntime;
import org.ambientdynamix.api.contextplugin.ContextPluginSettings;
import org.ambientdynamix.api.contextplugin.PluginConstants;
import org.ambientdynamix.api.contextplugin.PowerScheme;
import org.jibx.runtime.JiBXException;

import java.util.List;
import java.util.UUID;
import java.util.Vector;

/**
 * Example auto-reactive plug-in that detects the device's battery level.
 *
 * @author Darren Carlson
 */
public class AmbientcontrolRuntime extends ContextPluginRuntime {
    private static final int VALID_CONTEXT_DURATION = 60000;
    // Static logging TAG
    private final String TAG = this.getClass().getSimpleName();
    // Our secure context
    private Context context;
    private Bundle stopBundle;
    private List<ControlScenarioConfig> sceanriosLoaded;
    // A BroadcastReceiver variable that is used to receive battery status updates from Android


    private DynamixFacade facade;
    private ContextHandler contextHandler;

    @Override
    public void init(PowerScheme arg0, ContextPluginSettings arg1) throws Exception {
        sceanriosLoaded = new Vector<ControlScenarioConfig>();
        facade = getPluginFacade().getDynamixFacadeWrapper(getSessionId());
        if (facade == null)
            throw new Exception("Could not access Dynamix Facade!");
        Log.i(TAG, "init");
        context = this.getSecuredContext();
        stopBundle = new Bundle();
        stopBundle.putString(ControlConnectionManager.CONNECTION_CONTROL, ControlConnectionManager.CONFIG_STOP);

        facade.openSession(new ISessionCallback.Stub() {
            @Override
            public void onSuccess(DynamixFacade iDynamixFacade) throws RemoteException {

            }

            @Override
            public void onFailure(String s, int i) throws RemoteException {
                Log.e(TAG, "can't open session: " + s + " code: " + i);
            }
        });

    }


    /**
     * Called by the Dynamix Context Manager to start (or prepare to start) context sensing or acting operations.
     */
    @Override
    public void start() throws RemoteException {
        facade = getPluginFacade().getDynamixFacadeWrapper(getSessionId());
        Log.d(TAG, "Started!");
    }

    /**
     * Called by the Dynamix Context Manager to stop context sensing or acting operations; however, any acquired
     * resources should be maintained, since start may be called again.
     */
    @Override
    public void stop() throws RemoteException {
        Log.d(TAG, "Stopped!");
    }

    /**
     * Stops the runtime (if necessary) and then releases all acquired resources in preparation for garbage collection.
     * Once this method has been called, it may not be re-started and will be reclaimed by garbage collection sometime
     * in the indefinite future.
     */
    @Override
    public void destroy() throws RemoteException {
        this.stop();
        facade.closeSession();
        context = null;
        facade = null;
        Log.d(TAG, "Destroyed!");
    }

    @Override
    public void setPowerScheme(PowerScheme scheme) throws Exception {

    }

    @Override
    public void updateSettings(ContextPluginSettings settings) throws Exception {

    }


    @Override
    public void handleContextRequest(UUID requestId, String contextType) {
        Log.i(TAG, "received context request for:" + contextType);
        return;
    }

    @Override
    public void handleConfiguredContextRequest(UUID requestId, String contextType, final Bundle config) {
        Log.i(TAG, "received configured context request for:" + contextType);
        if(config.getBoolean(PluginConstants.WEB_REQUEST)){
            for (String s : config.keySet()) {
                Log.i(TAG, s);

            }
            if (config.containsKey(ControlConnectionManager.CONNECTION_CONTROL)) {

                if (config.getString(ControlConnectionManager.CONNECTION_CONTROL).equals(ControlConnectionManager.LOAD_SCENARIO)) {
                    String controlGraphId = config.getString(ControlConnectionManager.SCENARIO_ID);
                    if(controlGraphId != null) {
                        getScenario(controlGraphId, new ScenarioCallback() {
                            @Override
                            public void onSuccess(String result) {
                                receivedControlGraphXML(result);
                            }

                            @Override
                            public void onfailure() {
                                receivedControlGraphXML("");
                            }
                        });
                    }else{
                        receivedControlGraphXML(config.getString(ControlConnectionManager.SCENARIO_XML,""));
                    }
                }else if(config.getString(ControlConnectionManager.CONNECTION_CONTROL).equals(ControlConnectionManager.CONFIG_STOP)) {
                    String controlGraphId = config.getString(ControlConnectionManager.SCENARIO_ID);
                    getScenario(controlGraphId, new ScenarioCallback() {
                        @Override
                        public void onSuccess(String result) {
                            try {
                                stopScenario(result);
                            } catch (RemoteException e) {
                                Log.e(TAG,"can't load Scenario to stop");
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onfailure() {
                            Log.e(TAG,"can't load Scenario to stop");
                        }
                    });
                }
            }
        }
        return;
    }

    private void stopScenario(String controlScenarioXML) throws RemoteException {
        if(contextHandler == null) {
            Log.i(TAG,"No active scenario found to stop");
            return;
        }
        ControlScenarioConfig scenario;
        try {
            scenario = new ControlScenarioConfig(controlScenarioXML);
        } catch (JiBXException e) {
            e.printStackTrace();
            return;
        }
        for (final ControlGraphConfig controlGraphConfig : scenario.getControlGraphs()) {
            contextHandler.contextRequest(controlGraphConfig.getClientPlugin(), IControlMessage.CONTEXT_TYPE, stopBundle,
                    new IContextRequestCallback.Stub() {
                        @Override
                        public void onSuccess(ContextResult contextEvent) throws RemoteException {
                            Log.i(TAG, "Stopped " + controlGraphConfig.getClientPlugin());
                        }

                        @Override
                        public void onFailure(String s, int i) throws RemoteException {
                            Log.w(TAG, "registered CONNECTION_CONFIG Call was unsuccessful! Message: " + s + " | Error code: "
                                    + i);
                            Log.w(TAG, "failed to Stop " + controlGraphConfig.getClientPlugin());
                        }
                    });
        }
    }

    public void tapToInteractFeature() {
        Log.i(TAG, "tapToInteractFeature!");

        openView(new TapToInteractFeature(getSecuredContext(), this));
    }

    private interface ScenarioCallback{
        public void onSuccess(String result);
        public void onfailure();
    }
    public void getScenario(String ScenarioId, final ScenarioCallback callback) {
        Log.i(TAG, "call RestCall");
//        int port = 8080;
//        String hostname = "10.0.1.106";
//        String uri = "/ControlProfileServer-1.0.0/ControlGraph/" + ScenarioId + "?format=xml"; //TODO: Dynamic
        int port = 8080;
        String hostname = "205.185.117.11";
//        String hostname = "192.168.1.110";
        String uri = "/ControlProfileServer-1.0.0/ControlGraph/" + ScenarioId + "?format=xml"; //TODO: Dynamic
        new HTTPRequestMaker(hostname, port).doHTTP("GET", uri, new HTTPRequestMaker.Callback() {
            @Override
            public void execute(String result) {
                callback.onSuccess(result);
            }

            @Override
            public void onFailure() {
                Log.i(TAG, "httpRequestMaker request to failed");
                callback.onfailure();
            }
        });
    }

    private void receivedControlGraphXML(final String controlScenarioXML) {
        Log.i(TAG, "controlScenarioXML: " + controlScenarioXML);

        stopControlling();
        final ControlScenarioConfig scenario;
        try {
            scenario = new ControlScenarioConfig(controlScenarioXML);
        } catch (JiBXException e) {
            e.printStackTrace();
            return;
        }

        try {
            connect(new Callback(){
                @Override
                public void onSuccess() throws RemoteException {
                    SendLoadGraphRequest(scenario, new Callback());
                }

                @Override
                public void onFailure(String message, int errorCode) throws RemoteException {
                    super.onFailure(message, errorCode);
                }
            });
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }



    public void SendLoadGraphRequest(final ControlScenarioConfig scenario, final Callback callback) {
        if (contextHandler != null) {
            List<String> receivers = new Vector<String>();
            for (final ControlGraphConfig controlGraphConfig : scenario.getControlGraphs()) {
                if(!receivers.contains(controlGraphConfig.getClientPlugin()))
                    receivers.add(controlGraphConfig.getClientPlugin());
            }
            try {
                contextHandler.addContextSupport(receivers,IControlMessage.CONTEXT_TYPE, new Bundle(), new ContextSupportCallback(){
                    @Override
                    public void onSuccess(ContextSupportInfo supportInfo) throws RemoteException {
                        Log.i(TAG, "successfully added contextSupport for: " + supportInfo.toString());
                        for (final ControlGraphConfig controlGraphConfig : scenario.getControlGraphs()) {
                            Bundle bundle = new Bundle();
                            try {
                                bundle.putString(ControlConnectionManager.CONNECTION_CONFIG, controlGraphConfig.marshal());
                            } catch (JiBXException e) {
                                return;
                            }
                            bundle.putString(ControlConnectionManager.CONNECTION_CONTROL, ControlConnectionManager.CONFIG_INIT);
                            try {
                                Log.i(TAG, "Loading " + controlGraphConfig.marshal());
                            } catch (Exception e) {
                                e.printStackTrace();
                                return;
                            }
                            Log.i(TAG, " requesting to " + controlGraphConfig.getClientPlugin());
                            contextHandler.contextRequest(controlGraphConfig.getClientPlugin(), IControlMessage.CONTEXT_TYPE, bundle,
                                    new IContextRequestCallback.Stub() {
                                        @Override
                                        public void onSuccess(ContextResult contextEvent) throws RemoteException {
                                            Log.i(TAG, "registered CONNECTION_CONFIG succesfully");
                                            callback.onSuccess();
                                        }

                                        @Override
                                        public void onFailure(String s, int i) throws RemoteException {
                                            Log.w(TAG, "registered CONNECTION_CONFIG Call was unsuccessful! Message: " + s + " | Error code: "
                                                    + i);
                                            callback.onFailure(s, i);
                                        }
                                    });
                        }
                    }

                    @Override
                    public void onFailure(String message, int errorCode) throws RemoteException {
                        callback.onFailure(message, errorCode);
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void stopControlling() {
        try {

            Thread.sleep(1000);
        }catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    private void connect(final ICallback callback) throws RemoteException {
        if (contextHandler == null) {
            try {
                facade.openSession(new ISessionCallback.Stub() {
                    @Override
                    public void onSuccess(DynamixFacade iDynamixFacade) throws RemoteException {
                        facade.createContextHandler(new ContextHandlerCallback.Stub() {

                            @Override
                            public void onSuccess(ContextHandler handler) throws RemoteException {
                                contextHandler = handler;
                                callback.onSuccess();
                            }

                            @Override
                            public void onFailure(String message, int errorCode) throws RemoteException {
                                Log.i(TAG, "onFailure, create Context Handler");
                                Log.e(TAG, message);
                                callback.onFailure(message, errorCode);
                            }
                        });
                    }

                    @Override
                    public void onFailure(String s, int i) throws RemoteException {
                        Log.i(TAG, "onFailure, open Connection");
                    }
                });
            } catch (RemoteException e) {
                Log.i(TAG, "Catch Exception, connect");
                e.printStackTrace();
            }
        }else
            callback.onSuccess();

    }

}