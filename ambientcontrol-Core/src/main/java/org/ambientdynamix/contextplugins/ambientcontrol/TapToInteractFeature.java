package org.ambientdynamix.contextplugins.ambientcontrol;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.NfcA;
import android.os.Bundle;
import android.os.Handler;
import android.os.RemoteException;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import org.ambientdynamix.api.application.*;
import org.ambientdynamix.api.contextplugin.ActivityController;
import org.ambientdynamix.api.contextplugin.ContextPluginRuntime;
import org.ambientdynamix.api.contextplugin.IPluginView;

import java.util.*;

/**
 * Created by maxpagel on 20.11.14.
 */
public class TapToInteractFeature implements IPluginView {
    private final Context context;
    private ActivityController controller;
    private DynamixFacade facade;

    public TapToInteractFeature(Context context, ContextPluginRuntime runtime) {
        this.context = context;
        activeReceivers = new ArrayList<String>();
        activeReceiversMap = new HashMap<String, ContextSupportInfo>();
        tapSession = new ArrayList<String>();
        facade = runtime.getPluginFacade().getDynamixFacadeWrapper(runtime.getSessionId());
    }


    private final String TAG = this.getClass().getSimpleName();
    private ArrayList<String> tapSession;
    private ContextHandler contextHandler;
    private boolean tappingInProcess;
    private long lastTap;
    private final int TAP_SESSION_TIMEOUT = 20000;
    private Handler handler;
    private TextView notifications;
    private static Map<String, String> ids;
    private NfcAdapter mNfcAdapter;
    private Map<String, ContextSupportInfo> activeReceiversMap;
    private ArrayList<String> activeReceivers;

    static {
        ids = new HashMap<String, String>();
        ids.put("05381E58D1B0C1", "org.ambientdynamix.contextplugins.hueplugin");
        ids.put("053EF02DF2B0C1", "org.ambientdynamix.contextplugins.hueplugin");
        ids.put("0531ACF103B0C1", "org.ambientdynamix.contextplugins.spheronative");
        ids.put("053F9339D1B0C1", "org.ambientdynamix.contextplugins.spheronative");
        ids.put("053D007F90B0C1", "org.ambientdynamix.contextplugins.ardrone");
        ids.put("053B7CF103B0C1", "org.ambientdynamix.contextplugins.wemoplugin");
        ids.put("053F8BCC91B0C1", "org.ambientdynamix.contextplugins.ambientmedia");
        ids.put("0539A78562B0C1", "org.ambientdynamix.contextplugins.myoplugin");
    }

    private PendingIntent mPendingIntent;
    private IntentFilter[] mFilters;
    private ListView listView;
    private ArrayAdapter<String> adapter;

    public void handleIntent(Intent intent) {
        Log.i(TAG, "started by NFC");
        String action = intent.getAction();
        if (NfcAdapter.ACTION_TECH_DISCOVERED.equals(action) || NfcAdapter.ACTION_TAG_DISCOVERED.equals(action)) {

            // In case we would still use the Tech Discovered Intent
            final Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
            String[] techList = tag.getTechList();
            NfcA tagNfcA = NfcA.get(tag);
            Log.i(TAG, "sensed TAG with ID: " + getIdFrombytes(tag.getId()));
            if (tappingInProcess && System.currentTimeMillis() - lastTap < TAP_SESSION_TIMEOUT) {
                lastTap = System.currentTimeMillis();
            } else {
                newSession();
            }
            final String pluginId = ids.get(getIdFrombytes(tag.getId()));
            if (pluginId != null) {
                tapSession.add(pluginId);
                displayMessage("Discovered: " + pluginId);

            } else {
                displayMessage("Unknown Device Id");
            }
        }
    }

    List<Thread> pendingThreads = new Vector<Thread>();

    private void done(final String message) {
        try {
            resetSession();

            displayMessage(message);

            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(3000);
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                notifications.setText("Start new interaction by tapping a receiver");
                            }
                        });
                    } catch (InterruptedException e) {

                    }
                }
            });
            thread.start();
            pendingThreads.add(thread);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            e.printStackTrace();
        }
    }

    public void displayMessage(final String message) {
        for (Thread pendingThread : pendingThreads) {
            pendingThread.interrupt();
        }
        pendingThreads.clear();
        handler.post(new Runnable() {
            @Override
            public void run() {
                notifications.setText(message);
            }
        });
    }


    private void resetSession() {
        tapSession.clear();
        tappingInProcess = false;
        lastTap = System.currentTimeMillis();
    }

    private void newSession() {
        tapSession.clear();
        tappingInProcess = true;
        lastTap = System.currentTimeMillis();
    }


    private void tapCompleted() throws RemoteException {
        tappingInProcess = false;
//        tapSession.clear();
//        tapSession.add("org.ambientdynamix.contextplugins.ambientmedia");
//        tapSession.add("org.ambientdynamix.contextplugins.hueplugin");
//        tapSession.add("org.ambientdynamix.contextplugins.myoplugin");
//        tapSession.add("org.ambientdynamix.contextplugins.spheronative");
        handler.post(new Runnable() {
            @Override
            public void run() {
                notifications.setText("Selection completed loading interaction");
            }
        });
        if (tapSession.size() >= 2) {
            final String receiverId = tapSession.get(0);
            connect(receiverId, new ContextSupportCallback() {
                        @Override
                        public void onSuccess(ContextSupportInfo supportInfo) throws RemoteException {

                            Bundle bundle = new Bundle();
                            bundle.putString(ControlConnectionManager.CLIENT_PLUGIN_ID, receiverId);
                            ArrayList<String> controllers = new ArrayList<String>();
                            for (int i = 1; i < tapSession.size(); i++) {
                                controllers.add(tapSession.get(i));
                            }
                            bundle.putStringArrayList(ControlConnectionManager.SERVER_PLUGIN_ID, controllers);
                            bundle.putString(ControlConnectionManager.CONNECTION_CONTROL, ControlConnectionManager.CONFIG_INIT);

                            contextHandler.contextRequest(receiverId, IControlMessage.CONTEXT_TYPE, bundle,
                                    new IContextRequestCallback.Stub() {
                                        @Override
                                        public void onSuccess(ContextResult contextEvent) throws RemoteException {
                                            syncReceivers();
                                            done("Interaction set up successful");
                                        }

                                        @Override
                                        public void onFailure(final String s, final int i) throws RemoteException {
                                            done("Interaction setup failed: " + s + " | Error code: " + i);
                                            Log.w(TAG, "Call was unsuccessful! Message: " + s + " | Error code: "
                                                    + i);
                                            stopControlling(receiverId);
                                        }
                                    });
                        }

                        @Override
                        public void onFailure(String message, int errorCode) throws RemoteException {
                            done("error on connection");
                            Log.e(TAG, "error on connecting: " + message);
                        }
                    }
            );
        } else {
            done("Invalid device selection");
        }

    }

    private void connect(final String receiverId, final ContextSupportCallback callback) throws RemoteException {
        if (contextHandler == null || !facade.isSessionOpen()) {
            contextHandler = null;
            try {
                ISessionCallback.Stub sessionCallback = new ISessionCallback.Stub() {
                    @Override
                    public void onSuccess(DynamixFacade iDynamixFacade) throws RemoteException {
                        facade.createContextHandler(new ContextHandlerCallback() {
                            @Override
                            public void onSuccess(ContextHandler handler) throws RemoteException {
                                contextHandler = handler;
                                try {
                                    addSupport(receiverId, callback);
                                } catch (RemoteException e) {
                                    callback.onFailure(e.toString(), ErrorCodes.REQUEST_FAILED);
                                }
                            }

                            @Override
                            public void onFailure(String message, int errorCode) throws RemoteException {
                                done("failure to connect to create Context Handler");
                                Log.e(TAG, message);
                            }
                        });
                    }

                    @Override
                    public void onFailure(String s, int i) throws RemoteException {
                        done("failure to connect to Dynamix");
                        Log.e(TAG, s);
                    }
                };
                if (!facade.isSessionOpen())
                    facade.openSession(sessionCallback);
                else if (facade != null)
                    sessionCallback.onSuccess(facade);
            } catch (RemoteException e) {
                done("Unknown Error: " + e);
                e.printStackTrace();
            }
        } else
            try {
                addSupport(receiverId, callback);
            } catch (RemoteException e) {
                done("Unknown Error: " + e);
                e.printStackTrace();
            }

    }

    private void addSupport(final String receiverId, final ContextSupportCallback callback) throws RemoteException {
        contextHandler.addContextSupport(receiverId, IControlMessage.CONTEXT_TYPE, new ContextSupportCallback() {
            @Override
            public void onSuccess(ContextSupportInfo supportInfo) throws RemoteException {
                Log.i(TAG, "support added for: " + supportInfo);
                activeReceiversMap.put(receiverId, supportInfo);
                callback.onSuccess(supportInfo);

            }

            @Override
            public void onFailure(String message, int errorCode) throws RemoteException {
                Log.w(TAG,
                        "Call was unsuccessful! Message: " + message + " | Error code: "
                                + errorCode);
                callback.onFailure(message, errorCode);
            }
        });
    }


    private void stopControlling() {
        Bundle stopBundle = new Bundle();
        stopBundle.putString(ControlConnectionManager.CONNECTION_CONTROL, ControlConnectionManager.CONFIG_STOP);
        try {
            for (String activeReceiver : activeReceivers) {
                if (contextHandler != null)
                    contextHandler.contextRequest(activeReceiver, IControlMessage.CONTEXT_TYPE, stopBundle);
                activeReceiversMap.remove(activeReceiver);
            }
            Thread.sleep(500);
            if (contextHandler != null)
                facade.removeContextHandler(contextHandler.getContextHandler());
            contextHandler = null;
            done("All Interactions Canceled");
        } catch (Exception e) {
            done("Unknown Error: " + e);
            e.printStackTrace();
        } finally {
            syncReceivers();
        }
    }

    private void stopControlling(String receiverId) {
        try {
            Bundle stopBundle = new Bundle();
            stopBundle.putString(ControlConnectionManager.CONNECTION_CONTROL, ControlConnectionManager.CONFIG_STOP);

            if(contextHandler != null) {
                contextHandler.contextRequest(receiverId, IControlMessage.CONTEXT_TYPE, stopBundle);
                Thread.sleep(500);
                contextHandler.removeContextSupport(activeReceiversMap.get(receiverId));
                if (contextHandler.getContextSupport().size() == 0) {
                    facade.removeContextHandler(contextHandler.getContextHandler());
                    contextHandler = null;
                }
            }
            activeReceiversMap.remove(receiverId);
        } catch (Exception e) {
            done("Unknown Error: " + e);
            e.printStackTrace();
        } finally {
            syncReceivers();
        }

    }

    private String getIdFrombytes(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        for (byte b : bytes) {
            sb.append(String.format("%02X", b));
        }
        return sb.toString();
    }


    private void syncReceivers() {
        activeReceivers.clear();
        activeReceivers.addAll(activeReceiversMap.keySet());
        handler.post(new Runnable() {
            @Override
            public void run() {
                adapter.notifyDataSetChanged();
            }
        });
    }

    public void playVideo(View view, final String URL) {
        try {
            connect("org.ambientdynamix.contextplugins.ambientmedia", new ContextSupportCallback() {
                @Override
                public void onSuccess(ContextSupportInfo supportInfo) throws RemoteException {
                    Log.i(TAG, "checking for media devices");
                    try {
                        contextHandler.addContextSupport("org.ambientdynamix.contextplugins.ambientmedia", "org.ambientdynamix.contextplugins.ambientmedia.discovery", new ContextSupportCallback() {
                            @Override
                            public void onSuccess(final ContextSupportInfo supportInfoDiscovery) throws RemoteException {
                                contextHandler.contextRequest("org.ambientdynamix.contextplugins.ambientmedia",
                                        "org.ambientdynamix.contextplugins.ambientmedia.discovery", new ContextRequestCallback() {

                                            @Override
                                            public void onSuccess(ContextResult contextResult) throws RemoteException {
                                                Log.i(TAG, contextResult.getIContextInfo().getStringRepresentation("application/json"));
                                                String stringRepresentation = contextResult.getIContextInfo().getStringRepresentation("application/json");//

                                                if (stringRepresentation.contains("MediaRenderer") && stringRepresentation.contains("id")) {
                                                    int indexId = stringRepresentation.indexOf("id\":");
                                                    int indexIdend = stringRepresentation.indexOf("\",\"");

                                                    Log.i(TAG, "index " + indexId);
                                                    Log.i(TAG, "indexEnd " + indexIdend);
                                                    String deviceId = stringRepresentation.substring(indexId + 5, indexIdend);
                                                    Log.i(TAG, "id: " + deviceId);


                                                    //                                    [{"id":"583f3a5a5cd76e34e2bf012513863eb","displayName":"[AirPlay] Apple TV","type":"MediaRenderer","available":"true"}]

                                                    //                                    if (contextResult.getIContextInfo() instanceof IMediaDeviceInfoList) {
                                                    //                                        final IMediaDeviceInfoList result = (IMediaDeviceInfoList) contextResult.getIContextInfo();
                                                    //                                        for (IMediaDeviceInfo deviceInfo : result.getMediaDevices()) {
                                                    //                                            Log.i(TAG, "found device: " + deviceInfo.getDisplayName() + " " + deviceInfo.getType());
                                                    final Bundle bundle = new Bundle();
                                                    bundle.putString("interaction_type", "DeviceCommand");
                                                    bundle.putString("command", "play");
                                                    //                                            bundle.putString("uri", "https://www.youtube.com/watch?v=tcSSRlFzFuw");
//                                                    bundle.putString("uri", "https://youtu.be/8F3W51dMTyI");
                                                    //                                            bundle.putString("uri", "http://10.0.1.106:8888/hawpardragons.mov");
//                                                                                                bundle.putString("uri", "http://192.168.1.110/Ode-to-Fire.mov");
                                                                                                bundle.putString("uri", URL);
                                                    //                                                            bundle.putString("uri","http://hdwallpaper2013.com/wp-content/uploads/2013/02/Download-Flower-Background-Images-HD-Wallpaper-1080x607.jpg");
//                                                                                                bundle.putString("uri","http://www.youtube.com/watch?v=-BkG5xTGvh0");
                                                    bundle.putString("device_id", deviceId);
                                                    try {
                                                        contextHandler.addContextSupport("org.ambientdynamix.contextplugins.ambientmedia", "org.ambientdynamix.contextplugins.ambientmedia.interaction", new ContextSupportCallback() {
                                                            @Override
                                                            public void onSuccess(final ContextSupportInfo supportInfoInteraction) throws RemoteException {
                                                                contextHandler.contextRequest("org.ambientdynamix.contextplugins.ambientmedia", "org.ambientdynamix.contextplugins.ambientmedia.interaction", bundle, new IContextRequestCallback.Stub() {

                                                                    @Override
                                                                    public void onSuccess(ContextResult contextResult) throws RemoteException {
                                                                        Log.i(TAG, "sucessfully started playback of: " + bundle.getString("uri"));
                                                                        //                                                                contextHandler.removeContextSupport(supportInfoDiscovery);
                                                                        //                                                                contextHandler.removeContextSupport(supportInfoInteraction);
                                                                    }

                                                                    @Override
                                                                    public void onFailure(String s, int i) throws RemoteException {
                                                                        Log.i(TAG, "error on playback of: " + bundle.getString("uri") + " " + s);
                                                                    }
                                                                });
                                                            }

                                                            @Override
                                                            public void onFailure(String message, int errorCode) throws RemoteException {
                                                                super.onFailure(message, errorCode);
                                                            }
                                                        });
                                                    } catch (RemoteException e) {
                                                        e.printStackTrace();
                                                    }
                                                }

                                                //                                    } else
                                                //                                        Log.i(TAG, "No Media device found for playback. received: " + contextResult.getContextType() + " of class: " + contextResult.getClass().getSimpleName() );
                                            }

                                            @Override
                                            public void onFailure(String s, int i) throws RemoteException {
                                                Log.e(TAG, s);
                                            }
                                        });
                            }

                            @Override
                            public void onFailure(String message, int errorCode) throws RemoteException {
                                super.onFailure(message, errorCode);
                            }
                        });

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(String message, int errorCode) throws RemoteException {
                    super.onFailure(message, errorCode);
                }
            });
        } catch (RemoteException e) {
            e.printStackTrace();
        }


    }

    @Override
    public void setActivityController(ActivityController activityController) {
        controller = activityController;
    }

    @Override
    public View getView() {
        Log.i(TAG, "Tap 2 Interact View created");
        handler = new Handler();
        LinearLayout parentLayout = new LinearLayout(context);
        parentLayout.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
        parentLayout.setOrientation(LinearLayout.VERTICAL);
        FrameLayout frameLayout = new FrameLayout(context);
        frameLayout.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT,
                FrameLayout.LayoutParams.MATCH_PARENT));


        LinearLayout actualLayout = new LinearLayout(context);
        actualLayout.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
        actualLayout.setOrientation(LinearLayout.VERTICAL);
        frameLayout.addView(actualLayout);
//        <TextView
//        android:layout_width="wrap_content"
//        android:layout_height="80dp"
//        android:id="@+id/notifications"
//        android:text="@string/Start_tapping"
//        android:textSize="10pt"
//        android:layout_marginBottom="20dp"/>
        notifications = new TextView(context);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, dpToPx(80));
        layoutParams.setMargins(0, 0, 0, dpToPx(20));
        notifications.setLayoutParams(layoutParams);
        notifications.setText("Start interaction by tapping a receiver");
        notifications.setTextSize(TypedValue.COMPLEX_UNIT_PT, 10);
        actualLayout.addView(notifications);

//        <LinearLayout android:layout_width="fill_parent" android:layout_height="wrap_content" android:orientation="horizontal"
//        android:layout_gravity="center">
        LinearLayout doneCancel = new LinearLayout(context);
        doneCancel.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        doneCancel.setOrientation(LinearLayout.HORIZONTAL);


//        <Button
//        android:layout_width="175dp"
//        android:layout_height="wrap_content"
//        android:id="@+id/done"
//        android:text="Done"
//        android:layout_marginBottom="10dp"/>

        Button done = new Button(context);
        done.setText("Done");
        LinearLayout.LayoutParams layoutParamsCancelDone = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParamsCancelDone.setMargins(0, 0, 0, dpToPx(10));
        layoutParamsCancelDone.weight = 1;
        done.setLayoutParams(layoutParamsCancelDone);
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    tapCompleted();
                } catch (RemoteException e) {
                    Log.e(TAG, e.toString());
                    done("Dynamix error");
                }
            }
        });
        doneCancel.addView(done);
//        <Button
//        android:layout_width="175dp"
//        android:layout_height="wrap_content"
//        android:id="@+id/cancel"
//        android:text="Cancel"
//        android:layout_marginBottom="10dp"/>
        Button cancel = new Button(context);
        cancel.setText("Cancel");
        cancel.setLayoutParams(layoutParamsCancelDone);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                done("Canceled Tap Session");
            }
        });
        doneCancel.addView(cancel);
        actualLayout.addView(doneCancel);


//        <Button
//        android:layout_width="fill_parent"
//        android:layout_height="wrap_content"
//        android:id="@+id/playVideo"
//        android:text="@string/playVideo"
//        android:layout_marginBottom="10dp"
//        android:onClick="playVideo"/>
        final Button playVideo = new Button(context);
        playVideo.setText("Play Video");
        LinearLayout.LayoutParams layoutPlayVideo = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutPlayVideo.setMargins(0, 0, 0, dpToPx(10));
        playVideo.setLayoutParams(layoutPlayVideo);
        playVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                playVideo(playVideo,"http://192.168.1.110/Ode-to-Fire.mov");
                playVideo(playVideo,"https://www.youtube.com/watch?v=tcSSRlFzFuw");//ode to fire
            }
        });
        actualLayout.addView(playVideo);

        final Button playPhoto = new Button(context);
        playPhoto.setText("Show Photo");
        LinearLayout.LayoutParams layoutPlayPhoto = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutPlayPhoto.setMargins(0, 0, 0, dpToPx(10));
        playPhoto.setLayoutParams(layoutPlayPhoto);
        playPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                playVideo(playPhoto,"http://192.168.1.110/monkey.jpg");
                playVideo(playPhoto,"http://7-themes.com/data_images/out/9/6797208-green-background.jpg");
            }
        });
        actualLayout.addView(playPhoto);


//        <Button
//        android:layout_width="fill_parent"
//        android:layout_height="wrap_content"
//        android:id="@+id/stopAll"
//        android:text="@string/stopAll"
//        android:layout_marginBottom="10dp"/>


        final Button stopAll = new Button(context);
        stopAll.setText("Stop All");
        stopAll.setLayoutParams(layoutPlayVideo);
        stopAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stopControlling();
            }
        });
        actualLayout.addView(stopAll);


        listView = new ListView(context);
        listView.setLayoutParams(new ListView.LayoutParams(ListView.LayoutParams.MATCH_PARENT, ListView.LayoutParams.MATCH_PARENT));
        adapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, activeReceivers);

        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView parent, View v, int position, long id) {

            }
        });

        SwipeDismissListViewTouchListener touchListener = new SwipeDismissListViewTouchListener(
                listView,
                new SwipeDismissListViewTouchListener.DismissCallbacks() {
                    @Override
                    public boolean canDismiss(int position) {
                        return true;
                    }

                    @Override
                    public void onDismiss(ListView listView,
                                          int[] reverseSortedPositions) {
                        try {
                            for (int reverseSortedPosition : reverseSortedPositions) {
                                stopControlling(activeReceivers.get(reverseSortedPosition));
                            }
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    adapter.notifyDataSetChanged();
                                }
                            });
                        }catch(Exception e){
                            e.printStackTrace();
                        }
                    }
                });
        touchListener.setEnabled(true);
        listView.setOnTouchListener(touchListener);
        actualLayout.addView(listView);

        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread thread, Throwable ex) {
                Log.e(TAG, "Thread " + thread.getName() + " (" + thread.getId() + ") threw exception");
                ex.printStackTrace();
            }
        });
        parentLayout.addView(frameLayout);
        return parentLayout;
    }

    private int dpToPx(int dpValue) {
        Resources r = context.getResources();
        int px = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                dpValue,
                r.getDisplayMetrics()
        );
        return px;
    }

    @Override
    public void destroyView() {
        Log.i(TAG, "destroy");
        /*
         * Always remove our listener and unbind so we don't leak our service connection
		 */
        stopControlling();
        try {
            if (facade != null && contextHandler != null)
                facade.removeContextHandler(contextHandler.getContextHandler());
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        try {
            if (facade.isSessionOpen()) {
                facade.closeSession();
            }
        } catch (RemoteException e) {
            Log.e(TAG, "Error closing session during destroy view ");
        }
        if (contextHandler != null) {
            contextHandler = null;
        }
        controller.closeActivity();
    }

    @Override
    public int getPreferredOrientation() {
        return ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
    }

}
