package org.ambientdynamix.contextplugins.ambientcontrol;

import android.app.Activity;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import org.ambientdynamix.api.application.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by workshop on 11/4/13.
 */
public class BindDynamixActivity extends Activity {

    private final String TAG = this.getClass().getSimpleName();
    private ContextHandler contextHandler;
    private PluginInvoker pluginInvoker;
    private DynamixFacade dynamix;

    /**
     * Called when the activity is first created.
     *
     * @param savedInstanceState If the activity is being re-initialized after
     *                           previously being shut down then this Bundle contains the data it most
     *                           recently supplied in onSaveInstanceState(Bundle). <b>Note: Otherwise it is null.</b>
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (PluginInvoker.AUTOMATIC_EXECUTION) {
            setContentView(R.layout.main_auto);
            connect();
        } else {
            setContentView(R.layout.main);

            Button btnConnect = (Button) findViewById(R.id.btnConnect);
            btnConnect.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    connect();
                }
            });

            // Setup the disconnect button
            Button btnDisconnect = (Button) findViewById(R.id.btnDisconnect);
            btnDisconnect.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    disconnect();
                }
            });

         /*
         * Setup the interactive context acquisition button. Note that this method only works if the
		 * 'org.ambientdynamix.sampleplugin' plug-in is installed.
		 */
            Button btnInvokePlugin = (Button) findViewById(R.id.invokePlugin);
            btnInvokePlugin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    invokePlugins();
                }


            });

            Button btnInverse = (Button) findViewById(R.id.inverse);
            btnInverse.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    inverse();
                }


            });

            Button btnStopControlling = (Button) findViewById(R.id.stopControlling);
            btnStopControlling.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    stopControlling();
                }


            });

            initBundles();
        }
    }

    private void stopControlling() {

        try {

            contextHandler.contextRequest(bundles[currentControlRequest].getString(ControlConnectionManager.CLIENT_PLUGIN_ID), IControlMessage.CONTEXT_TYPE, bundles[2]);
            Thread.sleep(1000);
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    private Bundle[] bundles = new Bundle[4];

    private int currentControlRequest = 0;

    private void initBundles() {
        bundles[0] = new Bundle();
        bundles[0].putString(ControlConnectionManager.CLIENT_PLUGIN_ID, "org.ambientdynamix.contextplugins.ardrone");
        ArrayList<String> servers = new ArrayList<String>();
        servers.add("org.ambientdynamix.contextplugins.spheronative");
        servers.add("org.ambientdynamix.contextplugins.pitchtracker");
        bundles[0].putStringArrayList(ControlConnectionManager.SERVER_PLUGIN_ID, servers);
        bundles[0].putString(ControlConnectionManager.CONNECTION_CONTROL, ControlConnectionManager.CONFIG_INIT);

        bundles[1] = new Bundle();
        ArrayList<String> servers2 = new ArrayList<String>();
        servers2.add("org.ambientdynamix.contextplugins.ardrone");
        servers2.add("org.ambientdynamix.contextplugins.pitchtracker");
        bundles[1].putStringArrayList(ControlConnectionManager.SERVER_PLUGIN_ID, servers2);
        bundles[1].putString(ControlConnectionManager.CLIENT_PLUGIN_ID, "org.ambientdynamix.contextplugins.spheronative");
        bundles[1].putString(ControlConnectionManager.CONNECTION_CONTROL, ControlConnectionManager.CONFIG_INIT);

        bundles[2] = new Bundle();
        ArrayList<String> servers3 = new ArrayList<String>();
        servers3.add("org.ambientdynamix.contextplugins.spheronative");
        servers3.add("org.ambientdynamix.contextplugins.artnet");
        bundles[2].putStringArrayList(ControlConnectionManager.SERVER_PLUGIN_ID, servers2);
        bundles[2].putString(ControlConnectionManager.CLIENT_PLUGIN_ID, "org.ambientdynamix.contextplugins.ambientmedia");
        bundles[2].putString(ControlConnectionManager.CONNECTION_CONTROL, ControlConnectionManager.CONFIG_INIT);



        bundles[3] = new Bundle();
        bundles[3].putString(ControlConnectionManager.CONNECTION_CONTROL, ControlConnectionManager.CONFIG_STOP);


    }

    public void inverse() {
        try {
            stopControlling();
            if (currentControlRequest == 1)
                currentControlRequest = 0;
            else
                currentControlRequest = 1;
            contextHandler.contextRequest(bundles[currentControlRequest].getString(ControlConnectionManager.CLIENT_PLUGIN_ID), IControlMessage.CONTEXT_TYPE, bundles[currentControlRequest]);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    private void invokePlugins() {
        /**
         * This gets called once the user presses the invoke plugins button on the device. The deafult behaviour is to request
         * all context Types listed in the cotextRequestType Array
         */
        if (contextHandler != null) {
            try {

                Log.i(TAG, "A1 - Requesting Programmatic Context Acquisitions");
                IdResult result;
                contextHandler.contextRequest(bundles[currentControlRequest].getString(ControlConnectionManager.CLIENT_PLUGIN_ID), IControlMessage.CONTEXT_TYPE, bundles[currentControlRequest],
                        new IContextRequestCallback.Stub() {
                            @Override
                            public void onSuccess(ContextResult contextEvent) throws RemoteException {
                                Log.i(TAG, "A1 - Request id was: " + contextEvent.getResponseId());
                            }

                            @Override
                            public void onFailure(String s, int i) throws RemoteException {
                                Log.w(TAG, "Call was unsuccessful! Message: " + s + " | Error code: "
                                        + i);
                            }
                        });


            } catch (Exception e) {
                Log.e(TAG, e.toString());
            }
        } else
            Log.w(TAG, "Dynamix not connected.");
    }

    private void disconnect() {
        if (contextHandler != null) {
            try {
                /*
                 * In this example, this Activity controls the session, so we call closeSession here. This will
                 * close the session for ALL of the application's IDynamixListeners.
                 */
                stopControlling();
                dynamix.closeSession(new Callback() {
                    @Override
                    public void onFailure(String message, int errorCode) throws RemoteException {
                        Log.w(TAG, "Call was unsuccessful! Message: " + message + " | Error code: "
                                + errorCode);
                    }

                    @Override
                    public void onSuccess() throws RemoteException {
                        Log.w(TAG, "Session closed");
                        dynamix = null;
                        contextHandler = null;
                    }
                });

            } catch (RemoteException e) {
                Log.e(TAG, e.toString());
            }
        }
    }

    private void connect() {

        if (contextHandler == null) {
            try {
                DynamixConnector.openConnection(BindDynamixActivity.this, true, null, new ISessionCallback.Stub() {
                    @Override
                    public void onSuccess(DynamixFacade iDynamixFacade) throws RemoteException {
                        dynamix = iDynamixFacade;
                        dynamix.createContextHandler(new ContextHandlerCallback() {
                            @Override
                            public void onSuccess(ContextHandler handler) throws RemoteException {
                                contextHandler = handler;
                                pluginInvoker = new PluginInvoker();
                                registerForContextTypes();
                            }

                            @Override
                            public void onFailure(String message, int errorCode) throws RemoteException {
                                Log.i(TAG,message);
                            }
                        });//

                    }

                    @Override
                    public void onFailure(String s, int i) throws RemoteException {

                    }
                });
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    /*
         * Implementation of the IDynamixListener interface. For details on the IDynamixListener interface, see the Dynamix
         * developer website.
         */
    private ContextListener contextListener = new ContextListener() {


        @Override
        public void onContextResult(ContextResult event) throws RemoteException {
            /*
             * Log some information about the incoming event
			 */
            Log.i(TAG, "A1 - onContextEvent received from plugin: " + event.getResultSource());
            Log.i(TAG, "A1 - -------------------");
            Log.i(TAG, "A1 - Event context type: " + event.getContextType());
            Log.i(TAG, "A1 - Event timestamp " + event.getTimeStamp().toLocaleString());
            if (event.expires())
                Log.i(TAG, "A1 - Event expires at " + event.getExpireTime().toLocaleString());
            else
                Log.i(TAG, "A1 - Event does not expire");
			/*
			 * To illustrate how string-based context representations are accessed, we log each contained in the event.
			 */
            for (String format : event.getStringRepresentationFormats()) {
                Log.i(TAG,
                        "Event string-based format: " + format + " contained data: "
                                + event.getStringRepresentation(format));
            }
            boolean done = true;
            for (PluginInvoker.PluginInvocation pluginInvocation : pluginInvoker.getPluginInvocations()) {
                if (pluginInvocation.getContextRequestType().equals(event.getContextType())) {
                    pluginInvocation.setSuccessfullyCalled(true);
                    pluginInvoker.invokeOnResponse(event);

                }
                if (!pluginInvocation.isSuccessfullyCalled())
                    done = false;
            }

            if (done && PluginInvoker.AUTOMATIC_EXECUTION) {
                disconnect();
            }
        }
    };


    private List<ContextPluginInformation> uninstallingPlugins = new ArrayList<ContextPluginInformation>();
    private boolean initializing;

    /**
     * Threaded initialization that automatically uninstalls the plug-ins targeted by the pluginInvoker, waiting for
     * Dynamix events as needed.
     *
     * @param pluginInvoker The PluginInvoker of target plug-ins
     * @param callback      An (optional) callback to run after initialization
     * @throws RemoteException
     */
    private synchronized void init(final PluginInvoker pluginInvoker, final Runnable callback) throws RemoteException {
        // Check for init state
        if (!initializing) {
            Log.i(TAG, "Init running");
            initializing = true;
            // Create a list of targetPluginIds
            List<String> targetPluginIds = new ArrayList<String>();
            for (PluginInvoker.PluginInvocation invocation : pluginInvoker.getPluginInvocations())
                targetPluginIds.add(invocation.getPluginId());
            // Clear the uninstall list
            uninstallingPlugins.clear();
            // Access the list of installed Dynamix plug-ins
            List<ContextPluginInformation> result = dynamix.getAllContextPluginInformation();
            if (result.size() > 0) {
                // Add the ContextPluginInformation for each target plug-in to the uninstallingPlugins list
                for (ContextPluginInformation plug : result) {
                    for (String id : targetPluginIds) {
                        if (plug.getPluginId().equalsIgnoreCase(id))
                            uninstallingPlugins.add(plug);
                    }
                }
                if (!uninstallingPlugins.isEmpty()) {
                    for (final ContextPluginInformation info : uninstallingPlugins) {
                        Log.i(TAG, "Calling uninstall for plug " + info);
                        try {
                            dynamix.requestContextPluginUninstall(info, new Callback() {
                                @Override
                                public void onSuccess() throws RemoteException {
                                    Log.i(TAG, "Uninstalled: " + info.getPluginId());
                                    uninstallingPlugins.remove(info);
                                    if (uninstallingPlugins.isEmpty()) {
                                        Log.i(TAG, "Waiting for uninstall to complete... FINISHED!");
                                        // Set init to false, since we're finished
                                        initializing = false;
                                        // Fire the callback, if necessary (i.e, if it's not null)
                                        if (callback != null)
                                            callback.run();
                                    }
                                }

                                @Override
                                public void onFailure(String message, int errorCode) throws RemoteException {
                                    Log.e(TAG, "Uninstalling: " + info.getPluginId() + " failed, reason: " + message + " moving on as if uninstall succeeded");
                                    uninstallingPlugins.remove(info);
                                    if (uninstallingPlugins.isEmpty()) {
                                        Log.i(TAG, "Waiting for uninstall to complete... FINISHED!");
                                        // Set init to false, since we're finished
                                        initializing = false;
                                        // Fire the callback, if necessary (i.e, if it's not null)
                                        if (callback != null)
                                            callback.run();
                                    }
                                }
                            });
                        } catch (RemoteException e) {
                            Log.w(TAG, e);
                        }
                    }

                } else {
                    Log.i(TAG, "No plug-ins to uninstall!");
                    // Set init to false, since we're finished
                    initializing = false;
                    // Fire the callback, if necessary (i.e, if it's not null)
                    if (callback != null)
                        callback.run();
                }
            } else
                Log.w(TAG, "Could not access plug-in information from Dynamix");
        } else
            Log.w(TAG, "Already initializing, please wait...");
    }

    /*
	 * Utility method that registers for the context types needed by this class.
	 */
    private void registerForContextTypes() throws RemoteException {
		/*
		 * Below are several examples of adding context support using context types. In this case, the plug-in (or
		 * plug-ins) assigned to handle context type will be automatically selected by Dynamix.
		 */
        for (PluginInvoker.PluginInvocation pluginInvocation : pluginInvoker.getPluginInvocations()) {
            String type = pluginInvocation.getContextRequestType();
            contextHandler.addContextSupport(pluginInvocation.getPluginId(), type, new ContextSupportCallback(){
                @Override
                public void onFailure(String message, int errorCode) throws RemoteException {
                    Log.w(TAG,
                            "Call was unsuccessful! Message: " + message + " | Error code: "
                                    + errorCode);
                }
            });
        }
    }

    @Override
    protected void onDestroy() {
        Log.i(TAG, "ON DESTROY for: Dynamix Simple Logger (A1)");
		/*
		 * Always remove our listener and unbind so we don't leak our service connection
		 */
        if (contextHandler != null) {
            try {
                dynamix.closeSession();
                contextHandler = null;
            } catch (RemoteException e) {
            }
        }
        super.onDestroy();
    }
}









