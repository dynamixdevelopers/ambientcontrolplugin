# Ambient Control Plugin
The Ambient Control Plugin enables commodity mobile devices (e.g., smart-phones) to dynamically mediate control messaging between incompatible smart devices situated in the user’s environment. Ambient Control enables a variety of control capabilities and protocol translation services to be dynamically installed into a user’s mobile device on-demand using plug-ins.

Ambient Control can be used as a library by other Dynamix plugins, to facilitate easy interoperability. The library is designed to be easily integratabel into any Dynamix plugin, without interfering with normal plugin operations.
This documentation is assuming you used the Dynamix Plugin Development maven archetype to generate your plugin project.
### Add Ambient Control dependency
Add the following lines to your plugin-core project's pom.xml in the dependency part:
```xml
<dependency>
    <groupId>org.ambientdynamix</groupId>
    <artifactId>ambientcontrol-Datatypes</artifactId>
    <version>[2.0.0,)</version>
    <scope>provided</scope>
</dependency>
```
The new dependency must also be registered with the OSGI bundle, by adding it to the imported Packages list. This is also done in the same pom.xml. Adjust the **mave-bundle-plugin** declaration as follows:
```xml
<instructions>
...
<Import-Package>org.ambientdynamix.api.application,
    org.ambientdynamix.api.contextplugin,
    org.ambientdynamix.api.contextplugin.security,
    org.ambientdynamix.contextplugins.ambientcontrol
</Import-Package>
...
</instructions>
```
We will also need a couple of modifications to our plugin descriptor. The file is located in your core-plugin project's resources folder and is called [pluginName]Descriptor.xml. This is to to ensure we have the necessary rights to operate. Add the following lines:
```xml
<supportedContextTypes>
    <contextType>org.ambientdynamix.contextplugins.ambientcontrol.discovery</contextType>
    <contextType>org.ambientdynamix.contextplugins.ambientcontrol.controlmessage</contextType>
</supportedContextTypes>
<contextPluginDependencies>
    <contextPluginDependency pluginId="org.ambientdynamix.contextplugins.ambientcontrol" pluginName="Ambient Control Library"/>
</contextPluginDependencies>
<permissions>
    <permission>Context.ACTIVITY_SERVICE</permission>
    <permission>Permissions.SEND_INTER_PLUGIN_MESSAGES</permission>
    <permission>Permissions.RECEIVE_INTER_PLUGIN_MESSAGES</permission>
    <permission>Permissions.ACCESS_FULL_CONTEXT</permission>
</permissions>
```
### Integrating Ambient control into your plugin
The core class of Ambient Control is the `org.ambientdynamix.contextplugins.ambientcontrol.ControlConnectionManager` it manages all connections and interaction requests from apps or other plugins. You will have to instantiate and integrate an instance of this class in your plugin's life cycle.
##### Instantiate it in your plugin's init method:
```java
private ControlConnectionManager controlMgr;
@Override
public void init(PowerScheme powerScheme, ContextPluginSettings settings) throws Exception {
...
   controlMgr = new ControlConnectionManager(this, controlListener, "some loging identifier");
...
    }
```
The controlListener is a listener which you have to implement to handle incoming commands or requests for control. We will describe this in detail in the next section.
##### Starting and stopping Ambient Control
You dont need to do anything in your plugin's start method, but just to be safe you should call the control manager's stop method in your plugins stop method:
```java
@Override
public void stop() {
    controlConnectionManager.stop();
    ...
}
```
##### Handling control request
Last but not least we need to make sure the Ambient Control library gets requests and messages directed to it, by implementing a few hooks in the request and message handling interface methods of your plugin. Implememt the following methods if you haven't done so yet and add the respective lines of code:
```java
@Override
public boolean addContextlistener(ContextListenerInformation listenerInfo) {
    if(!controlMgr.addContextListener(listenerInfo)){
        ...//whatever the plugin should do if this
           //listener was not intended for Ambient Control
    }else
       return true;
}

@Override
public void onMessageReceived(Message message) {
    if(!controlMgr.handleConfigCommand(message)){
        ...//whatever the plugin should do if this
           //message was not intended for Ambient Control
    }
}

@Override
public void handleConfiguredContextRequest(final UUID requestId, String contextInfoType, Bundle config) {
    if (!controlMgr.handleConfiguredContextRequest(requestId, contextInfoType, config)){
        ...//whatever the plugin should do if this
           //request was not intended for Ambient Control
    }
```
### The Control Listener
When you are instantiating the `ControlConnectionManager` you need to supply it with a Listener, which is an implementation of the following interface:
```java
public interface IControllConectionManagerListener {
    public void controllRequest(String command, String name);
    public void stopControlling(String command, String name);
    public void consumeCommand(IControlMessage command, String sourcePluginId, String requestId);
    }
```
These methods will be called by the ControlConnectionManager, dependent on control requests and commands.

`public void controllRequest(String type, String name)`
Is being called when your plugin is supposed to start controlling a client by sending out Controll commands of the specified type and name. In this method, set up all the infrastructure you need to supply the requested command and start sending them out.
To send an Ambient Control command, use the ControlConnectionManager's sendControllCommand method like in this example of sending a Toggle Command:
```java
controlMgr.sendControllCommand(
    new ToggleCommand(Commands.SWITCH,false,"SWITCH"),"[source device ID]");
```

`public void stopControlling(String command, String name)` Is the counterpart to controlRequest, as it is called when the commands of the specified type and name are not needed anymore. In this method, stop the sending of the specified command and release all related resources if possible. This method will only be called if no other subscriptions of the specific command are active any more. You can therefor savely assume that the cammands are not needed anymore and release all infrastructure related to producing them.

`public void consumeCommand(IControlMessage command, String sourcePluginId, String requestId)` This method will only be called if your plugin has subscribed to another controllers commands, to be controlled. If this is the case, every time your plugin receives a control command from a controller, the ControlConnectionManager will call you back by calling this method and give you the command along with it's source for your plugin to process.

### Available Command Types
At the moment we suppport the following control commands:
```java
     public static final String MOVEMENT_LEFT = "MOVEMENT_LEFT";
     public static final String MOVEMENT_RIGHT = "MOVEMENT_RIGHT";
     public static final String MOVEMENT_BACKWARD = "MOVEMENT_BACKWARD";
     public static final String MOVEMENT_FORWARD = "MOVEMENT_FORWARD";
     public static final String MOVEMENT_FORWARD_RIGHT = "MOVEMENT_FORWARD_RIGHT";
     public static final String MOVEMENT_FORWARD_LEFT = "MOVEMENT_FORWARD_LEFT";
     public static final String MOVEMENT_BACKWARD_RIGHT = "MOVEMENT_BACKWARD_RIGHT";
     public static final String MOVEMENT_BACKWARD_LEFT = "MOVEMENT_BACKWARD_LEFT";
     public static final String MOVEMENT_UP = "MOVEMENT_UP";
     public static final String MOVEMENT_DOWN = "MOVEMENT_DOWN";
     public static final String MOVEMENT_START_STOP = "MOVEMENT_START_STOP";
     public static final String MOVEMENT_NEUTRAL = "MOVEMENT_NEUTRAL";
     public static final String MOVEMENT_SPIN_CCW = "MOVEMENT_SPIN_CCW";
     public static final String MOVEMENT_SPIN_CW = "MOVEMENT_SPIN_CW";
     public static final String PLAYBACK_PLAY_PAUSE = "PLAYBACK_PLAY_PAUSE";
     public static final String PLAYBACK_FORWARD_SEEK = "PLAYBACK_FORWARD_SEEK";
     public static final String PLAYBACK_BACKWARD_SEEK = "PLAYBACK_BACKWARD_SEEK";
     public static final String PLAYBACK_NEXT = "PLAYBACK_NEXT";
     public static final String PLAYBACK_PREVIOUS = "PLAYBACK_PREVIOUS";
     public static final String PLAYBACK_STOP = "PLAYBACK_STOP";
     public static final String DISPLAY_COLOR = "DISPLAY_COLOR";
     public static final String DISPLAY_IMAGE = "DISPLAY_IMAGE";
     public static final String DISPLAY_VIDEO = "DISPLAY_VIDEO";
     public static final String DISPLAY_MESSAGE = "DISPLAY_MESSAGE";
     public static final String SENSOR_PYR = "SENSOR_PYR";
     public static final String SENSOR_ACC = "SENSOR_ACC";
     public static final String SENSOR_GYRO = "SENSOR_GYRO";
     public static final String SENSOR_AXIS = "SENSOR_AXIS";
     public static final String SENSOR_TOGGLE = "SENSOR_TOGGLE";
     public static final String SENSOR_KEYPRESS = "SENSOR_KEYPRESS";
     public static final String SENSOR_SINGLE_VALUE = "SENSOR_SINGLE_VALUE";
     public static final String TRIGGER = "TRIGGER";
     public static final String SWITCH = "SWITCH";
```
### Plugin Control Descriptions
To publish the available Commands that can be emitted and/or understood by your plugin, you need to create a PluginControlDescription. This small JSON snippet has the following format (an example from the sphero plugin:

```JSON
{
    "name":"SpheroPlugin",
    "artifact_id":"org.ambientdynamix.contextplugins.spheronative",
    "description":"",
    "pluginVersion":"1.0.0",
    "owner_id":"",
    "platform":"",
    "minPlatformVersion":"",
    "minFrameworkVersion":"",
    "inputList": [ 
        {
            "mandatoryControls":["SENSOR_PYR"],
            "priority":1
        },
        {
            "mandatoryControls":["SENSOR_AXIS"],
            "priority":2
        },
        {
            "mandatoryControls":[
                "MOVEMENT_FORWARD_LEFT",
                "MOVEMENT_BACKWARD_LEFT",
                "MOVEMENT_BACKWARD",
                "MOVEMENT_LEFT",
                "MOVEMENT_RIGHT",
                "MOVEMENT_FORWARD",
                "MOVEMENT_FORWARD_RIGHT",
                "MOVEMENT_BACKWARD_RIGHT"
            ],
            "priority":3
        },
        {
            "mandatoryControls":["DISPLAY_COLOR"],
            "priority":4
        },
        {
            "mandatoryControls":["SWITCH"],
            "priority":5
        }
    ],
    "optionalInputList":["DISPLAY_COLOR","SWITCH"],
    "outputList":{
        "Accelerometer":"SENSOR_ACC",
        "Collision":"SENSOR_TOGGLE",
        "Pitch Yaw Roll":"SENSOR_PYR",
        "Gyroscope":"SENSOR_GYRO"
    }    
}

```
This data needs be transmitted to the Ambient control Webservice to register your plugin in the Ambient control Database.
TODO: create upload infrastructure for control descriptions

## Using your plugin inside Ambient Flow
If your Plugin control description is uploaded to the Ambient Flow server, your plugin can be part of Ambient control graphs, that allow an easy setup and configuration of direct plugin interactions.
To set up such an interaction you need to create a control scenario containing one or more graphs and send it to the Ambient Control Plugin. The easiest way 
to do this is by using the [Ambient Flow Gui](http://flow.ambientdynamix.org/dataflow/). The User interface allows you to create control graphs using an easy visual programming workflow.
After saving the scenario from the UI, you can either deploy it directly to your phone using the provided pairing mechanism, or you can download the JSON or XML representation by calling the associated REST service storing the graph. The URL looks like this:
```http://flow.ambientdynamix.org:8080/ControlProfileServer-1.0.0/ControlGraph/[your graph id here]?format=xml```
```xml

The resulting graph will look something like the following example, which sets up an interaction between a WEMO motion detector and an media renderer, such that the playback is stopped when the wemo detects no motion.

<?xml version="1.0" encoding="UTF-8"?>
<ControlScenario>
    <ControlGraph>
        <receiver>org.ambientdynamix.contextplugins.ambientmedia</receiver>
        <controller>org.ambientdynamix.contextplugins.wemoplugin</controller>
        <controlEdge>
            <commandType>SWITCH</commandType>
            <name>Switch</name>
            <sourcePlugin>org.ambientdynamix.contextplugins.wemoplugin</sourcePlugin>
            <targetPlugin>org.ambientdynamix.contextplugins.ambientmedia</targetPlugin>
            <translator>
                <translateTo>PLAYBACK_PLAY_PAUSE</translateTo>
                <class>org.ambientdynamix.contextplugins.ambientcontrol.SwitchToPlaybackPlayPauseTranslator</class>
            </translator>
        </controlEdge>
    </ControlGraph>
</ControlScenario>
```
The corresponding XML schema for a control Scenario can be found [here](https://bitbucket.org/dynamixdevelopers/ambientcontrolplugin/src/3927a369b63574e262f304e64c3d1dd1243e6c3d/ambientcontrol-Datatypes/src/main/config/ControlConnection.xsd?at=master&fileviewer=file-view-default).
Of course you can also manually or programmatically create the Control Scenario xml and send it to the Ambient Control plugin for instantiation. To do so, you will have to send a configured context request to the plugin, which holds the scenario xml. See the following code example on how to do this

```
#!java

//create a bundle instructing Ambient Control to load your scenario.
Bundle bundle = new Bundle();
bundle.putString(ControlConnectionManager.CONNECTION_CONTROL, ControlConnectionManager.LOAD_SCENARIO);
//either load the scenario from the webservice like this
    bundle.putParcelable(ControlConnectionManager.SCENARIO_ID, scenarioId);
    
//or send the xml directly with the request like this
    bundle.putParcelable(ControlConnectionManager.SCENARIO_XML, scenarioXml);
    
//send the bundle to the plugin with a configured context request
try {
    if (callback != null)
        contextHandler.contextRequest("org.ambientdynamix.contextplugins.ambientcontrol", controlMessage.CONTEXT_TYPE, bundle, callback);
    else
        contextHandler.contextRequest("org.ambientdynamix.contextplugins.ambientcontrol", controlMessage.CONTEXT_TYPE, bundle);
} catch (RemoteException e) {
    e.printStackTrace();
}
```
The same can be done from within a browser using javascript
```javascript
var callback = function() {
        //Do something in the callback
    }
    var params = {
//either load the scenario from the webservice like this
        "SCENARIO_ID": scenarioId
//or send the xml directly with the request like this
        "SCENARIO_XML": scenarioXml
    };
    var header = {
        "Content-type": "application/x-www-form-urlencoded"
    };
    dynamixContextHandler.configuredContextRequest("PUT", "org.ambientdynamix.contextplugins.ambientcontrol", "org.ambientdynamix.contextplugins.ambientcontrol.controlmessage", {
        callback: callback,
        params: params,
        headers: header
    });
```

## Directly Interacting with Ambient Control plugins
There are several ways to send an Ambient Control command to an Ambient Control enabled plugin directly. This is useful, if for example you just want to switch on a lamp, or perform a different one shot action, or if you are interacting with an Ambient Control plugin directly from an app or a web site.   
To achieve this, one way is to send a configured context request holding the command programmatically from within an app or another plugin, after creating a Dynamix session 
with a context handler and adding support for the specific plugin. See the following code example.

```
#!java

//create a bundle holding the Ambient Control command. A "switch on" command in this case
Bundle bundle = new Bundle();
bundle.putString(ControlConnectionManager.CONNECTION_CONTROL, ControlConnectionManager.CONTROL_COMMAND);
bundle.putParcelable(ControlConnectionManager.COMMAND, new ToggleCommand(Commands.SWITCH, true, "switch"));

//send the bundle to the plugin with a configured context request
try {
    if (callback != null)
        contextHandler.contextRequest(pluginId, controlMessage.CONTEXT_TYPE, bundle, callback);
    else
        contextHandler.contextRequest(pluginId, controlMessage.CONTEXT_TYPE, bundle);
} catch (RemoteException e) {
    e.printStackTrace();
}
```

Alternatively Ambient Control plugins can be invoked through Dynamix JS using java script. Please see the [Dynamix JS documentation](https://bitbucket.org/dynamixdevelopers/dynamix-2.x-javascript-apis/wiki/Home) for details on how to set up a session and context support using Java Script from within a browser.
After you have a session set up in the browser, you can send a Command to an Ambient Control plugin by following this code example:

```javascript
var callback = function() {
        //Do something in the callback
    }
    var params = {
        "CONNECTION_CONTROL": "CONTROL_COMMAND", 
        "SWITCH_ON": true
    };
    var header = {
        "Content-type": "application/x-www-form-urlencoded"
    };
    dynamixContextHandler.configuredContextRequest("PUT", "org.ambientdynamix.contextplugins.hueplugin", "org.ambientdynamix.contextplugins.ambientcontrol.controlmessage", {
        callback: callback,
        params: params,
        headers: header
    }); 
```

If you want to subscribe to data provided from an Ambient Control Plugin, you can do so by requesting configured context support from the respective plugin. See the following example to get Orientational Data (Pitch yaw roll) from a sphero robot:
```java
Bundle config = new Bundle();
ArrayList<String> commands = new ArrayList<>();
commands.add("SENSOR_PYR");//You can subscribe for more than one command by adding more commands to the list
config.putStringArrayList("REQUESTED_COMMANDS", commands);
contextHandler.addContextSupport("org.ambientdynamix.contextplugins.spheronative","org.ambientdynamix.contextplugins.ambientcontrol.controlmessage",config);
```
the same works from javascript like this:
```javascript
var addConfiguredContextSupport = function() {
    var callback = function(status, result) {
        console.log("Context support added callback, Status: " + status + ", Result: " + result);
    }
    var listener = function(status, result) {
        console.log("Pitch: " + result.pitch);
        console.log("Yaw: " + result.yaw);
        console.log("Roll: " + result.roll);
    }
    // specify which command Type to subscribe to, In this case pitch yaw and roll info
    var params = {
        REQUESTED_COMMANDS: "SENSOR_PYR" //multiple commands can be supplied as a comma separated list
    };
    var header = {
        "Content-type": "application/x-www-form-urlencoded"
    };
    dynamixContextHandler.addConfiguredContextSupport("PUT", "org.ambientdynamix.contextplugins.spheronative","org.ambientdynamix.contextplugins.ambientcontrol.controlmessage", {
        callback: callback,
        listener: listener,
        params: params,
        headers: header
    });
}
```